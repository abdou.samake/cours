## ValueChanges

est un observable qui emet un événement chaque fois qu'une valeur change dans l'un de des controles de 
formulaires de FormGroup.<br>
Cet observable est la base de la fonctionnalité d'enregistrement automatique d'un formulaire.

## Opérateur RxJs

<ul>
<li>

**debounceTime**
</li>
Cet opérateur n'autorise pas l'émission d'un événement à partir de l'Observable jusqu'à ce qu'un laps de temps défini se soit écoulé jusqu'à ce que le dernier événement soit émis

<li>


**switchMap**
</li>
C'est un operateur qui permet de basculer l'observable vers un nouveau observable.
Il permet de mettre à jour les données de l'utilisateur.
Cet opérateur annule de manière transparente l'Observable précédent et s'abonne au nouveau.
</ul>