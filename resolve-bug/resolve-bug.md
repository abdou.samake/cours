## No such module 'Capacitor'

Pour résoudre le Bug No such module 'Capacitor' sur Xcode avec un projet Ionic.
Il faut supprimer le dossier **iOS** dans le projet Ionic et ajouter à nouveau le plate-forme **iOS**.
Et dans un terminal normal en dehors du repertoire Project excécuter la commande


``sudo arch -x86_64 gem install ffi``

Puis dans le dossier iOS/App on excécute la commande


``arch -x86_64 pod install`` 
