## Round
```Math.round``` permet d'arrondir un nombre decimal.

````javascript
console.log(Math.round(10.9)); // affiche 11
````

## Floor
````Math.Floor```` permet de prendre la partie entière d'un nombre decimal.

````javascript
console.log(Math.floor(10.9)); // affiche 10
````

## tofixed
````tofixed(nombre)```` permet de mettre le nombre de chiffre après la virgule dans un nombre decimale
```toFixed(2)``` prend 2 chiffres après la virgule.

````javascript
console.log(10.666678.toFixed(3)); // 10.667 
````
**toFixed** transforme le nombre en string.

````javascript
console.log(10.666687.toFixed(3) + 10); 
// 10.66710, il considére 10comme un string et fait la concaténation.
````

## Parse
``parse`` permet de tranformer les string en nombre.<br>
``parseInt`` permet de parser un nombre en entier.<br>
``parseFloat`` permet de parser un nombre en virgule

````javascript
console.log(parseFloat('10.67') + 10) // 20.67

console.log(parseFloat('10.67') + parseInt('10')); // 20.67

console.log(parseFloat(10.66678.toFixed(2)) + 10); // 20.67
````
