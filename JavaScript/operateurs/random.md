## Math random
``Math.random()`` permet de créer un nombre aléatoire compris dans l'intervalle [0, 1[
````javascript
console.log(Math.random()); //affiche un nomnbre aléatoire entre 0 et 1 exclus.

````

````javascript
console.log(Math.random()*1000); // affiche un nombre compris entre [0, 1000[

console.log(Math.random()*1000 + 1); //affiche un nombre compris entre [1, 1000[
````

````javascript
console.log(Math.floor(Math.random()*1000 + 1)); // affiche un entier aléatoire compris entre [1, 1000[
````

## random entre deux nombres

Pour avoir un nombre aléatoire situé entre un maximum et un minimum on effectue la fonction suivante
````javascript
function createRandomNumber(min, max) {

return Math.floor(min + Math.random()*(max - min) + 1);
}
console.log(createRandomNumber(10, 15));
// et on obtient un nombre aléatoire situé entre 10 et 15 inclus.
````
## La Récursivité
C'est une fonction qui s'appele elle même elle à l'intérieur de la fonction.

````javascript
const alreadyUsedNumbers = [];
function generateId(min, max) {
  const temp = createRandomNumber(min, max);
  if (alreadyUsedNumbers.length === max) {
    return;
  }
  if (alreadyUsedNumbers.find((value) => value === temp)) {
    return generateId(min, max);
  } else {
    alreadyUsedNumbers.push(temp);
    return temp;
  }
}
````