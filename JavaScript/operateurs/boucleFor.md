## Boucle For
C'est une boucle qui permet de faire de façon répéter une action.
### Syntax :
````javascript
for ('initiateur'; 'condition de continuation'; 'incrémentation') {
    action
}
````

````javascript
for ( let i = 0; i <= 20; i = i+1) {
    console.log('toto' + i);
}
// affiche
// toto1
// toto2
// toto3

// ...
// toto20
````
On a la même écritue qui donne le même resultat.
````javascript
let i =0;
for (;i <=20;) {
    console.log('toto' + 1);
    i = i + 1;
}
````
````javascript
let i = 0;
for (; true;) {
    console.log('tot' + 1);
    i = i + 1;
}
ce boucle s'exécute à l'infini.
````