## PUT
**PUT** permet de remplacer un objet indiqué par un autre objet donné.

Dnas notre tableau contenant d'objets habituel on remplace l'objet d'id 2 par un objet cré sur **postman** sur la requête.

````javascript
{
	"firstName": "baba",
	"lastName": "samake",
	"age": 38,
	"car": false
}
````
````javascript
import express from 'express';
import bodyParser from 'body-parser';
const app = express();
app.use(bodyParser({}));
app.get('/api', (eq, res) => {
    return res.send('coucou promo 4')
});
let users = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 28,
        id: 1
    },
    {
        firstName: 'mamad',
        lastName: 'samak',
        age: 28,
        id: 2
    },
    {
        firstName: 'bouba',
        lastName: 'samake',
        age: 28,
        id:3
    }
];
app.put('/api/users/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const newUser = req.body;
    const index = users.findIndex((user) => user.id === id);
    users[index] = {...newUser, id};
    return res.send(users);
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````
On selectionne **PUT** puis renseigne l'url http://localhost:3015/api/2 et indique l'id 2 de l'objet a remplacer.
on obtient le resultat.

````javascript
[
    {
        "firstName": "abdou",
        "lastName": "samake",
        "age": 28,
        "id": 1
    },
    {
        "firstName": "baba",
        "lastName": "samake",
        "age": 38,
        "car": false,
        "id": 2
    },
    {
        "firstName": "bouba",
        "lastName": "samake",
        "age": 28,
        "id": 3
    }
]
````