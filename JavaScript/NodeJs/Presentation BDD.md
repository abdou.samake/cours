# Présentation de la Base de donnée (BDD)
On crée une base de donnée sur **firebase** en suivant les etapes suivantes : <br>
<ol>
<li>Aller sur son compte firebase, créer un nouveau projet</li>
<li>Sur le projet aller dans  Database => créer une base de donnée</li>
<li>choisir l'operation (Démarrer en mode de production)</li>
<li>mettre eur3 (europe - west) pour que heberger la BDD en europe</li>
<li>Sur webstrn dans terminal on fait firebase init</li>
<li>On selectionne use an existing project</li>
<li>Puis on apuie sur entré jusqu'a obtenir " Firebase initialization complete</li>
<li>On returne sur firebase pour commencer une collection
<ul>
<li>On nomme notre collecte hotels</li>
<li>Puis on rempli un champ en donnant son nom, sa valeur et son type</li>
<li>On peut ajouter à nouveau un ou des champs</li>
L'Id de la collecte est généré automatiquement</li>
</ul>
<li>On peut aussi ajouter un document nommé rooms par exemple, l'Id sera généré automatiquement</li>
<li>Ensuite on définit et on ajoute des champs.</li>
</ol>
