## Post
**POST** permet d'ajouter un nouvel élément à une liste de tableau.
On installe sur l'ordinateur bodyparser grace à la ligne de commande
``npm install -g bodyparser``; <br>
On crée dabord l'objet qu'on va ajouter dans notre tableau habituel sur postman
en cochant sur raw et selectionner le text **JSON**.
````javascript
{
	"firstName": "dramane",
	"lastName": "samake",
	"age": 28,
	"car": true
}
````


````javascript
import express from 'express';
import bodyParser from 'body-parser';
const app = express();
app.use(bodyParser({}));
app.get('/api', (eq, res) => {
    return res.send('coucou promo 4')
});
let users = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 28,
        id: 1
    },
    {
        firstName: 'mamad',
        lastName: 'samak',
        age: 28,
        id: 2
    },
    {
        firstName: 'bouba',
        lastName: 'samake',
        age: 28,
        id:3
    }
];

app.post('/api/users', (req, res) => {
    const newUser = req.body;
    const id = users.length + 1;
    users.push({...newUser, id});
    return res.send(users);
});

app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````

On choisi POST puis on renseigne l'url http://localhost:3015/api/users
et on a le resultat sur postman:
````javascript
[
    {
        "firstName": "abdou",
        "lastName": "samake",
        "age": 28,
        "id": 1
    },
    {
        "firstName": "bouba",
        "lastName": "samake",
        "age": 28,
        "id": 3
    },
    {
        "firstName": "dramane",
        "lastName": "samake",
        "age": 28,
        "car": true,
        "id": 3
    },
    {
        "firstName": "dramane",
        "lastName": "samake",
        "age": 28,
        "car": true,
        "id": 4
    }
]
````
