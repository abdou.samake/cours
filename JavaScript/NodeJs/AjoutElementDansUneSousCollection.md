# Ajouter un élémnent dans une sous collection

On commence par définir notre fonction **createRoom** dans le fichier room.service.ts

````javascript

import {roomModel} from "../models/room.model";
import admin from "firebase-admin";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import DocumentReference = FirebaseFirestore.DocumentReference;
import CollectionReference = FirebaseFirestore.CollectionReference;

const db = admin.firestore();
const refHotels: CollectionReference = db.collection('hotels');
const refRooms: CollectionReference = db.collection('rooms');

export async function createRoom(hotelId: string, newRoom: roomModel): Promise<string> {
    if (!hotelId || !newRoom) {
        throw Error('hotelId and newRoom are missing')
    }
    const refHotel: DocumentReference = refHotels.doc(hotelId);
    const snapShtHotel: DocumentSnapshot = await refHotel.get();
    if (!snapShtHotel.exists) {
        return 'hotel dose not existe';
    }
    const createRoomRef: DocumentReference = await refRooms.add(newRoom);
    const createRoomInHotel: DocumentReference = refHotel
        .collection('rooms')
        .doc(createRoomRef.id);
    await createRoomInHotel.set({ref: createRoomRef, uid: createRoomRef.id});
    return 'ok';
}

````
<ul>
<li>On cré la fonction **createRoom** qui prend en parametre hotelId de type string et newRoom qui est la room à jouter 
de type roomModel.</li>
<li>refHotel est la reference d'un seul hotel qu'on va choisir par son id pour ajouter un room.</li>
<li>On cré la variable createRoomRef pour ajouter dans la reference des rooms notre nouvelle room a ajouté, elle sera la nouvelle room créée</li>
<li>creatRoomInHotel designe la variable pour créer une sous collection rooms puis definir la reference de notre nouvelle room par un id qu'on va créer 
qui sera determiner par 

``createRoomRef.id``.

</li>
<li>Ainsi on peut faire un set sur la variable creatRoomInHotel pour ajouter la reference et l'uid de notre nouvelle chambre</li>

</ul>

Dans notre fichier server.ts on fait appelle a notre fonction 

````javascript
app.post('/api/hotels/:hotelId/rooms/', async (req, res) => {

    try {
        const hotelId = req.params.hotelId;
        const newRoom = req.body;
        const result = await createRoom(hotelId, newRoom);
        return res.send(result);
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message});
    }
});
````
On va dans postman on choisit le verb **Post**, on renseigne sur l'url
le chemin de notre url en choisissant un id d'Hotel où sera ajouter la nouvelle hotel.
Ainsi on obtient une sous collecetion room dans notre hotel. Et cet objet room créé dans l'hotel est aussi créé dans la collection des rooms.