# Promise all

Il ermet d'envoyer toutes les promesses q'on a en même temps et d'avoir le resultat de la demande le plus rapidement possible.

<ul>On effectue d'abord une fonction permettant de faire un GET sur un room par son id s'il existe</ul>  

````javascript
export async function getRoomById(roomId: string): Promise<roomModel> {
    if (!roomId) {
        throw new Error ('roomId is missing');
    }
    const roomRef: DocumentReference = roomsRef.doc(roomId);
    const snapRoomRef: DocumentSnapshot = await roomRef.get();
    if (!snapRoomRef.exists) {
        throw new Error ('room does not exist');
    }
    return snapRoomRef.data() as roomModel;
}
````
<ul>Ensuite on fait la fonction permettant de faire le

**Promiseall**
</ul>

````javascript
export async function getGetAllPromise(roomUids: string []) {
    const getAllPromiseToresolve: Promise<roomModel>[] = roomUids.map((Uid) => getRoomById(Uid));
    return Promise.all(getAllPromiseToresolve);
}
````
<ul>
<li>On fait dabord un map sur les roomsUid qui sont les uid des rooms et on get chaque uid à l'aide de la fonction getRoomById</li>
</ul>

<ul> On fait le test sur les uid des rooms de notre base de données qu'on met dans un tableau</ul>

````javascript
app.get('/api/test', async (req, res) => {
    try {
        const rooms: roomModel[] = await getGetAllPromise([
            'ASGwMPStOB5cH9NntHYR',
            'Mb8jrVaVwm88oHH68Ccw',
            'TTchhMxW79EeUJitgVf3',
            'cAvXOecb14ViGzvwyc0t'
        ]);
        return res.send(rooms);

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
En renseignant l'url sur **postman** on obtient tout les rooms dans un tableau en même temps.