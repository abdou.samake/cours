# Principes simples de firebase
On procéde de la façon suivante:
<ul>
<li>Aller sur le site de firebase</li>
<li>Accéder à la documentation</li>
<li>Get started for Admin</li>
<li>Cloud firebase
<ul><li>Get started

`npm install firebase-admin --save` (--save) donne l'indication au système que vous vouleez installer firebase admin dans le projet et 
le faire apparaitre dans le fichier package.json qu'on a installer firebase-admin.<br>
-S ou --save veut dire enregistre et installe firebase admin.

</li></ul>
<li>Aprés installation on voit apparaitre sur le fichier package.json firebase-admin</li>
<li>Dans serveur.ts on fait 

`import admin from 'firebase-admin`
Puis on insére le code d'initialization de l'application firebase dans firestore.

````javascript
const db = admin.firestore();
const ref = db.collection('hotels'); // c'est la reference por accéder aux hotels
admin.initializeApp({
    credential: admin.credential.applicationDefault()
});
````
<li>Pour faire le GET puisqu'on doit aller chercher les données dans la base de donnée crée sur firebase donc le processus sera "asynchrone" donc on met "async" devant (req, res). On dit qu'on a un code asynchrone et pour l'exécution on met "await" pour dire attend d'avoir le resultat avant d'exécuter.
</li>
<li>Pas la peine d'ecrire du await devant des lignes de code synchrone comme par exemple 

`console.log(2)`<br>
Car dans un code on peut avoir des lignes de code synchrone.
</li>
</ul>
Ainsi on a le code du get 

````javascript
app.get('/api/hotels', async (req, res) => {
    try {
        const hotels = await ref.get();
        return res.send(hotels);
    } catch(e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
Le code donne pas l'objet hotels crée car le serveur n'a pas trouvé le credential dans l'initialisation et il va falloir lui en fournir pour que ça marche.