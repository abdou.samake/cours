# Les enums
enums c'est à dire les enumérations.

Les énumérations c'est comme une interface un peu spéciale qui permet de décrire un état dans lequel est un objet.<br>
On va essayer ici d'enumérer une succession d'etat que peuvent avoir les rooms. Et l'etat d'un objet (rooms) doit être unique on ne peut pas avoir deux etats en même temps.

<ul>
<li>On definit notre enum

````javascript
export enum RoomState {
    isavailable = 'est disponible',
    isoccuped = 'est occupé',
    isclosed = 'est en traveaux',
    iscleaning = 'en nettoyage'
}
````
</li>
<li>On ajoute le model RoomState pour roomState dans l'interface roomModel

````javascript
export interface roomModel {
    roomName: string;
    size: number;
    id: number | string;
    roomState: RoomState;
}
`````

<li>On definit notre objet rooms avec un etat de chambre parmi les etats enumérés dans enum
avec l'attibut roomState.


````javascript
const rooms: roomModel = {
    id: 1,
    roomName: 'suite',
    size: 5,
    roomState: RoomState.isavailable
}
console.log(rooms);
````

Et on a notre chambre d'hotel qui est dans l'etat disponible.

</li>

</li>
</ul>