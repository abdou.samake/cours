# ls classes

Les classes sont conçues pour aider les developpeurs à travailler avec des codes très complexes.

<ul>
<li>On declare une classe avec le mot clé   


**class**   
 Une classe peut être considérée comme un model.
 On cré l'objet HotelClass qui désigne la class d'un hotel.
 
 ````javascript
export class HotelClass {
    id?: number | string;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    roomState?:boolean;
    isavailable?: boolean;
}
Et la class on peut avoir plusieurs attributs on peut le voir dans la fonction.
````
 </li>
 <li>La class a un constructeur 
 
 **constructor** c'est une fonction dont le nom est réservé à une fonction.
 Le constructeur est destiné à créer une instance de class.<br>
 
 une **instance** c'est quelque chose qui est propre à l'objet.
 Pour créer une instance on utilise le mot clé **new** 
 
 ``const myHotel = new HotelClass();`` 
 
 On reprend notre objet avec le constructeur dans le fichier hotelModel.ts: 
 
 ````javascript
export class HotelClass {
    id?: number | string;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    roomState?:boolean;
    isavailable?: boolean;


    constructor() {
        console.log('cread');
    }
}

````

Dans server.ts on defini l'instance de notre object:

````javascript

app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass();
    console.log(myHotel);
    await res.send(myHotel);
});
````
On obtient un objet vide car on a encore rien fait de dans.<br>
``{}``
 
 </li>
 <li>On cré maintenant un constructure qui prend en parametre un id de type number.<br>
 On utilise le mot clé 
 
 **this** qui représente la futur instance de la class
 
 
 ````javascript
constructor(id: number) {
        this.id = id;
    }
````
On vient de stocker l'id dans la class de l'hotel (HotelClass).
Maintenant si on retourne dans le fichier server.ts il nous demande d'indiquer l'id et une fois indiquer on obtient l'objet myHotel avec son id.


````javascript
export class HotelClass {
    id?: number | string;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    roomState?:boolean;
    isavailable?: boolean;


    constructor(id: number) {
        this.id = id;
    }

}
````

````javascript
app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass(12);
    console.log(myHotel);
    await res.send(myHotel);
});
````

Resultat:

````javascript
{"id":12}
````
 </li>
 <li>On peut créer une instance avec les autres attributs de l'objet.<br>
 A noté qu'on peut mettre les points d'interrogations dans les attributs dans le cas où il ya des données non obligatoires c'est à dire qu'on utilise pas
 Les attributs avec points d'interrogations (?) doivent venir à la fin.
 
 ````javascript
    constructor(
        id: number,
        name: string,
        roomNumbers?: number,
        pool?: boolean,
        roomState?:boolean,
        isavailable?: boolean,
        ) {
        this.id = id;
        this.name = name;
        this.roomState = roomState;
        this.roomNumbers = roomNumbers;
        this.isavailable = isavailable
    }
}
````
Il ya pas de point d'interrogation sur l'id et name donc ils sont seulement pris en compte.


````javascript
 app.get('/api/class', async function  (req, res) {
     const myHotel = new HotelClass(12, 'hotel des fleurs');
     console.log(myHotel);
     await res.send(myHotel);
 });
````
 Resultat : 
 
 
 ````javascript
{"id":12,"name":"hotel des fleurs"}
````
 </li>
 <li>On peut aller plus vite en utilisant assign
 
 **assign** permet de prendre les propriétés d'un objet A et les mettre dans l'objet B.
 
 ````javascript
export class HotelClass {
    id?: number | string;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    roomState?:boolean;
    isavailable?: boolean;


    constructor(hotel: HotelModel) {
        Object.assign(this, hotel)
    }
}
````
Dans le server.ts on renseigne dans la fonction HotelClass l'objet hotel qui l'argument avec les attributs demandé qui sont l'id et name.


````javascript
app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs'});
    console.log(myHotel);
    await res.send(myHotel);
});
````
Resultat : 
````javascript
{"id":12,"name":"hotel des fleurs"}
````
 </li>
</ul>

## Les Methodes

Quand on definit une classe on peut les donner des **methodes** ce sont des fonctions specialesqui vont s'appliquer aux instances, elles pourront manupiler l'objet.

<ul>Exemple: calculer les roomNumbers de l'hotel</ul>
On definit la fonction calculateRoomNumbers 

````javascript
calculateRoomNumbers() {
        this.roomNumbers = this.rooms.length;
    }
````
le code complet:

````javascript
import {roomModel} from "./room.model";

export interface HotelModel {
    id?: number | string;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms: roomModel[];
    roomsData?: roomModel[];
}

export class HotelClass {
    id?: number | string;
    name?: string;
    rooms: roomModel[];
    roomNumbers?: number;
    pool?: boolean;
    roomState?:boolean;
    isavailable?: boolean;


    constructor(hotel: HotelModel) {
        this.rooms = hotel.rooms
        Object.assign(this, hotel)
    }
    calculateRoomNumbers() {
        this.roomNumbers = this.rooms.length;
    }
}
````
Maintenat dans le fichier server.ts on ajoute l'attibut room dans la class HotelClass et on applique la methode calculateRoomNumbers a myHotel

````myHotel.calculateRoomNumbers();````

le code: 
````javascript
app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs', rooms: []});
    myHotel.calculateRoomNumbers();
    await res.send(myHotel);
});
````
Et le resultat il nous calcul le nombre de chambre dans l'hotel. 

````javascript
{"rooms":[],"id":12,"name":"hotel des fleurs","roomNumbers":0}
````
On a l'objet myHotel avec le roomNumber qui est égale à 0 puisque les rooms sont vides.

On peut créer d'autre methodes comme mettre la piscine de l'hotel à true ou à false.
On procéde de la même façon:

````javascript

    destroyHotelPool() {
        this.pool = false;
    }
````
````javascript

app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs', rooms: []});
    myHotel.calculateRoomNumbers();
    myHotel.destroyHotelPool();
    await res.send(myHotel);
});
````
Resultat: 
````javascript
{"rooms":[],"id":12,"name":"hotel des fleurs","roomNumbers":0,"pool":false}
// la piscine est à false.
````
CreatePool:

````javascript
createHotelPool() {
        this.pool = true;
    }
````
````javascript
app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs', rooms: []});
    myHotel.calculateRoomNumbers();
    myHotel.createHotelPool();
    await res.send(myHotel);
});
````
Resultat:

````javascript
{"rooms":[],"id":12,"name":"hotel des fleurs","roomNumbers":0,"pool":true}
````
On peut faire autrement pour ne pas appliquer deux fois myHotel.
Pour cela on fait en sorte que nos class renvoient **this**.

````javascript
    calculateRoomNumbers(): HotelClass {
        this.roomNumbers = this.rooms.length;
        return this;
    }
    destroyHotelPool(): HotelClass {
        this.pool = false;
        return this;

    }
    createHotelPool(): HotelClass {
        this.pool = true;
        return this;
    }
}
````

Et la maintenant on peut appliquer les methodes calculateRoomNumbers() et createHotelPool() successivement sur myHotel

````javascript

app.get('/api/class', async function  (req, res) {
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs', rooms: []});
    myHotel
        .calculateRoomNumbers()
        .createHotelPool();
    await res.send(myHotel);
});
````
Le resultat est le même:

````javascript
{"rooms":[],"id":12,"name":"hotel des fleurs","roomNumbers":0,"pool":true}
````
**Anoter**: par convention quand on cré une class la prémière lettre du nom doit être en majuscule et doit se terminer par le mot 'class'.

## Heritage

On cré une autre class enfant qui a les mêmes propriétés que le pere mais qui a des attributs supplémentaires.
On definit la class enfant en mettant **expends** parent pour recupérer toutes les informations du père et aussi les méthodes créés.

``export class LuxHotelModelClass extends HotelClass`` 

On fait ensuite un nouveau constructor pour les nouveaux attributs de l'enfant.<br>
Avant cela on cré une interface pour l'enfant en faisant extends pour récupérer les informations de l'interface du pére et on lui ajoute les modéles des nouveaux attributs.<br>

Quand on construit le constructor enfant on utilise le mot clé **super** qui demande d'appeler le constructor parent.

On a le code :
````javascript

export class LuxHotelModelClass extends HotelClass {
    numberOfStars: number;
    numbersOfRoofTop: number;

    constructor(hotel: LuxHotelModel) {
        super(hotel)
        this.numberOfStars = hotel.numberOfStars || 1;
        this.numbersOfRoofTop = hotel.numbersOfRoofTop || 1; // pareil
    }
}
// le 1 c'est une façon de forcer que un luxHotel doit avoir au minimum 1 numberOfStars et un numbersOfRoofTop
```` 

Dans le fichier server.ts on fait comme on avait fait pour le père :

````javascript
app.get('/api/class', async function  (req, res) {
    const myHotel = new LuxHotelModelClass({
        id: 12,
        name: 'hotel des fleurs',
        rooms: [],
        numberOfStars: 2,
        numbersOfRoofTop: 4,
    });
    await res.send(myHotel);
});
````
Resutat on a l'objet avec les informations supplémentaires :

````
{"rooms":[],"id":12,"name":"hotel des fleurs","numberOfStars":2,"numbersOfRoofTop":4}
````

## les roomClass
On cré une class pour les rooms.<br>
On utilise le mot clé **implements** pour forcer la class nommée **RoomClass** d'avoir le même model que **roomModel**
<ul>Anoter: pour générer rapidement le constructor de RoomClass on :
<ul>
<li>clique droit sur RoomClass</li>
<li> => on clique sur generate</li>
<li>=> on clique sur constructor</li>
<li>=>on selectionne les propriétés</li>
<li>et on fait ok</li>
</ul> 
On obtient automatiquement le constructor de la class.
</ul>

````javascript

export class RoomClass implements roomModel {
    roomName: string;
    size: number;
    id: number;

    constructor(id: number, roomName: string = '', size: number = 0) {
        this.roomName = roomName;
        this.size = size;
        this.id = id;
    }
}
````
Ici on a definit par defaut les parametres du constructor en donnant size la valeur 0 et roomName string vide
Ainsi il nous juste a indique la valeur de id dans server.ts.

````javascript
app.get('/api/roomClass', async function  (req, res) {
    const myRoom = new RoomClass(1);
    await res.send(myRoom);
});
````
Resultat: 
````javascript
{"roomName":"","size":0,"id":1}
````