## Complément sur les fonctions

````javascript
function add(a, b) {
    return a + b;
}
````
Autre façon de definir les fonction, on les appelle **array function**
```const addbis = (a, b) => a + b;```
On peut appeler une fonction en le mettant dans une variable<br>
 ````const result = addbis(2, 5)````
 On a aussi l'ecriture:
 ````javascript
const addbis = (a, b) => {

    return a + b;
}
const result = addbis(2, 5);
console.log(result) // 7
````
la variable intermediaire **result** on le met après avoir défini la fonction.