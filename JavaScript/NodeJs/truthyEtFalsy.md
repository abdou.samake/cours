# truthy et Falsy

## falsy 

**Falsy** c'est toute les valeurs qui sont fausses en javascript. Une valeur est fausse si le predicat renvoi **false**.<br>
Parmi les falsy on a :
Undefined, NAN, NULL, false, zero(0), string vide('') etc.

##Truchy
**truchy** c'est quand quelque chose est vrais en javascript.
Parmi les truchy on : <br>
les number, (' '), true, etc.

le point d'exclamation (!) renvoi l'inverse de quelque chose.
On a !falsy = true et !truchy = false <br>
!1 est un falsy, !true est falsy, !false est un truchy.
Le double point d'exclamation transforme une boolean : <br>
````javascript
console.log(!!null) // false
console.log(!!undefined) // false
console.log(!!0) false
console.log(!!true) // true
console.log(!!'') // false
console.log(!!' ') // true
````

