## GET
Avec Get on demande au protocole http de donner quelque chose

````javascript
import express from 'express';
const app = express();

app.get('/api', (req, res) => {

return res.send('coucou promo 4')
});
let users = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 28,
        id: 1
    },
    {
        firstName: 'mamad',
        lastName: 'samak',
        age: 28,
        id: 2
    },
    {
        firstName: 'bouba',
        lastName: 'samake',
        age: 28,
        id:3
    }
];
app.get('/api/users', (eq, res) => {
    return res.send(users);
});

app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````

Dans ce code on l'url http://localhost:3015/api va faire afficher le message **coucou promo 4**<br>
Et l'url http://localhost:3015/api/users va afficher l'objet users.
