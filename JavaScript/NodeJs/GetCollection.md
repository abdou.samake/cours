# Get collection
Il permet d'affiché toutes les modications apportées sur notre base de données.<br>
<ul>On effectue d'abord notre fonction qui permet de faire le get</ul>

````javascript
export async function getAllHostels(): Promise<HotelModel[]> {
    const hostelsSnapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData> = await refHotels.get();
    const hostel: HotelModel[] = [];
    hostelsSnapshot.forEach((hostelSnap) => hostel.push(hostelSnap.data() as HotelModel)); // on force le type a HotelModel
    return hostel;
}
````
### get

``Promise<QuerySnapshot<T>>``

Le get permet de renvoyer un **QuerySnapshot** <br>
**Query**: c'est l'intérrogation d'une donnée.
**Snapshot** c'est la capture d'une donnée de la base de données à l'instant T.<br>
Un **QuerySnapshot** renvoit un objet qui est comme un array mais qui n'est pas un array, on peut lui appliquer seulement **ForEach**, mais pas les autres comme **map**, **reduce**.

<ul>On appelle notre fonction </ul>

````javascript
app.get('/api/hotels', async (req, res) => {
    try {
        const hostel = await getAllHostels();
        return res.send(hostel);

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
<ul>On choisit GET sur postman et on renseigne l'url, ainsi on voit apparaitre tous les objets de notre base de données avec les modifications effectuées au paravant.</ul>