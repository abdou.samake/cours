# Architecture client serveur

## Serveur
Le serveur represente d'une part la machine ayant pour rôle de générer un réseau d'ordinateur, d'autre part on l'emploi pour désigner des logiciels, des programmes
ayant pour rôle de gerer plusieurs requêtes d'un ensemble de systêmes clients.

## Port
Un port c'est une ligne de connexion que le serveur va établir avec l'exterieur pour repondre avec des 0 et des 1 sur cette connexion.
Les serveurs sont paramétrés sur un port.

Il ya 65535 ports disponibles sur un serveur, certains sont réservés au systeme.
On travail avec les ports qui sont décrits entre 3000 et 6000.
 
On ne peut qu'avoir qu'un seul serveur node js par port.

Pour utiliser un autre serveur sur un port on a deux possiblités :
<ul>
<li>Soit on arrête le prémier serveur</li>
<li>Soit on change le port du serveur sur  webpack </li></ul>  