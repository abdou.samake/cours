## Delete
Avec Delete on demande au protocole http de supprimer quelque chose.
Pour faire fonctionner delete on va utiliser  l'application **postman**.
````javascript
import express from 'express';
import bodyParser from 'body-parser';
const app = express();
app.use(bodyParser({}));
app.get('/api', (eq, res) => {
    return res.send('coucou promo 4')
});
let users = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 28,
        id: 1
    },
    {
        firstName: 'mamad',
        lastName: 'samak',
        age: 28,
        id: 2
    },
    {
        firstName: 'bouba',
        lastName: 'samake',
        age: 28,
        id:3
    }
];
app.delete('/api/users/:id', (req, res) => {
    const id = parseInt(req.params.id);
    users = users.filter((user) => user.id !== id);
    return res.send(users);
});

app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````
Sur **postman** on choisit l'option **DELETE** et on renseigne l'url en choisissant un id exemple
````http://localhost:3015/api/users/2````
Et l'objet de id 2  va être supprimé du tableau users
 On a en resultat
 ````js
[
    {
        "firstName": "abdou",
        "lastName": "samake",
        "age": 28,
        "id": 1
    },
    {
        "firstName": "bouba",
        "lastName": "samake",
        "age": 28,
        "id": 3
    }
]

````