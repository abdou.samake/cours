## Rest Operator
**Rest operator** permet d'avoir des fonctions sans preciser tout les arguments si on en a beaucoup .

````javascript
function add(..arg) {
    console.log(arg);
    return arg;
}
add(2, 3, 4, 5, 8);
````
Ce code affiche les parametres sous forme de tableau<br>
[2, 3, 4, 5, 8] <br>
Et on porra faire un reduce pour calculer la somme.
````javascript
function add(...arg) {
    const result = arg.reduce((acc, value) => acc + value, 0);
    return result;
}
console.log(add(2, 3, 4, 5, 8)) // 22
````
````javascript
function addbis(message, ...arg) {
    console.log(arg);
    const result = arg.reduce((acc, value) => acc + value, 0);
    console.log(message, result);
}
addbis('le resultat est : ', 2, 5, 3, 4, 8);
// les resultat est 22
````
Le **rest operator** doit être le dernier de la liste des parametres de fonction.

