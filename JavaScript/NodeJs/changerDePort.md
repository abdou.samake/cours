# Changer de Port

Webpack est un outil qui permet de faire tourner des codes. Pour avoir deux serveurs qui tourne en même temps, on change le port le l'autre.
Pour le faire : 
<ol>
<li>On ouvre webpack.config.js</li>
<li>On modifie port en lui donnant un numéro différent du premier </li>
<li> Et puis on sauvegarde pour finir</li>
</ol>