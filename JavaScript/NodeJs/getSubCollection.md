# Get Sub collection

On voudrais avoir l'intégralité des contenues des rooms dans les hotels pas seulement les ref et les uids.<br>
Pour cela on fait :
<ol>
<li>On verifie que l'hotel existe à l'aide de la fonction getHotelExistById </li>

````javascript
export async function getHotelExistById(hotelId: string): Promise<HotelModel> {
    if (!hotelId) {
        throw new Error('hotelId is missing');
    }
    const hotelRef: DocumentReference = refHotels.doc(hotelId);
    const snapShotHotelRef: DocumentSnapshot = await hotelRef.get();
    if (!snapShotHotelRef.exists) {
        throw new Error('hotel does not exist');
    }
    return snapShotHotelRef.data() as HotelModel;
}
````
<li>On determine la sous collection des rooms dans l'hotel en faisant un doc sur refHotels puis une collection avec le path rooms</li>

<li>On fait le get de la collection </li>
<li>On definit un tableau vide de type string</li>
<li>On parcourt la collection par forEach et on push chaque uid des rooms dans le tableau vide.</li>
<li>On fait une PromiseAll pour obtenir tous les rooms qui sont dans l'hotel à l'aide de la fonction getGetAllPromise

````javascript

export async function getGetAllPromise(roomUids: string[]){
    const getpromisallRooms: Promise<roomModel>[] = roomUids.map((uid) => getRoomById(uid));
    return await Promise.all(getpromisallRooms);
}
````

</li>
<li>Et pour finir on assigne a l'attribut roomsData le getGetAllPromise des uids contenues dans le tableau précedemment vide et on fait un return qui sera de type HotelModel  </li>
 
</ol> 
On l'intégralité du code


````javascript

export async function getSubCollection(hotelId: string) {
    const hotelToFind: HotelModel = await getHotelExistById(hotelId);
    const roomsCollectionInHotelToFind: CollectionReference  = refHotels
        .doc(hotelId)
        .collection('rooms');
    const snapRoomsCollection: QuerySnapshot = await roomsCollectionInHotelToFind.get();
    const roomsRef: string[] = [];
    snapRoomsCollection.forEach((roomRef) => roomsRef.push(roomRef.data().uid));
    const rooms = await getGetAllPromise(roomsRef);
    hotelToFind.roomsData = rooms;
    return hotelToFind;
}
````

A noter que notre code d'initialisation on le met maintenant uniquement dans le fichier principale server.ts

````javascript
import {cert} from "./cred/cert";
import admin from "firebase-admin";
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo4-node.firebaseio.com'
});
````
Et tout a fait au dessus des autres codes.

On appelle notre fonction

````javascript
app.get('/api/hotels/:hotelId', async (req, res) => {
    try {
        const hotelId: string = req.params.hotelId;
        const result: HotelModel = await getSubCollection(hotelId);
        return res.send(result);

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````

Sur **Postman** on choisit GET on renseigne l'url en choisichant un id d'hotel avec des rooms
et on obtient bien l'intégralité du contenu de l'hotel avec egalement les contenues des rooms

````javascript
{
    "pool": false,
    "name": "hotel ocean",
    "roomNumbers": 15,
    "roomsData": [
        {
            "size": 10,
            "roomName": "suite dakaroise"
        },
        {
            "size": 11,
            "roomName": "suite barcelonnaise"
        }
    ]
}
````