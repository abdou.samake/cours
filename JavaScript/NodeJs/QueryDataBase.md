# Query Database

On fait un query sur la base de données si on veut avoir tous les hotels ayant un roomNumbers égale à 5.
 La valeur du roomNumbers sera en string on va donc faire un parseInt pour le transformer en number.
 
 <ol>
 <li>On cré la fonction getHotelByRoomNumbers qui va prendre comme paramétre roomNumbers de type number et la fonction va retournée un table d'hotel donc de type HotelModel[]</li>
 <li> Sur refHotels on applique la fonction 
 
 ``where`` qui renvoit un query et qui prend trois arguments : 
 <ul>
 <li>le nom de ce que l'on cherche (ici roomNumber) qu'on met entre simple quote</li>
 <li>le signe '==' ou '<' ou '>' qui est mis aussi entre simple quote</li>
 <li>Et la valeur de ce l'on cherche qui est le parametre de la fonction</li></ul>
 Il seront séparés par des virgules.
 </li>
 <li>Ensuite on fait un get() pour obtenir la collection de ces hotels</li>
 <li>Ensuite on fait un forEach sur la collection d'hotels en ajoutant chacun de ces hotels dans un tableau vide préalablement défini</li>
 <li>Et pour finir on return le tableau</li>
 
 
 ````javascript
export async function getHotelByRoomNumbers(roomNumbers: number): Promise<HotelModel[]> {
    if (!roomNumbers) {
        throw new Error('roomNumbers is missing');
    }
    const data: QuerySnapshot = await refHotels
        .where('roomNumbers', '==', roomNumbers)
        .get();
    const result: HotelModel[] =  [];
    data.forEach((doc) => result.push(doc.data() as HotelModel));
    return result;
}
````
 <li>On appele notre fonction dans le dossier principale 
 <ul>
 <li>on defint la constante roomNumbers qui est le query en faisant un parseInt pour qu'il soit un number puisqu'il est definit en tant que number dans la fonction</li>
 <li>On appele la fonction qui va prendre le parametre roomNumbers</li>
 <li>Et on fait le return</li>
 </ul></li>
 
 
 ````javascript
app.get('/api/hotels', async (req, res) => {
    try {
        const roomNumbers: number = parseInt(req.query.roomNumbers);
        const result = await getHotelByRoomNumbers(roomNumbers);
        return res.send({result});
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
<li> On renseigne l'url

``http://localhost:3015/api/testQuery?roomNumbers=15``


On obtient le resultat

````javascript
[
{"pool":false,"name":"hotel ocean","roomNumbers":15},
{"pool":false,"name":"hotel ocean","roomNumbers":15},
{"pool":false,"name":"hotel ocean","roomNumbers":15}
]
````
on a bien un tableau contenant que de hotels avec roomNumber = 15
</li>
 </ol>