# Set and Get enum State

## Set enum State
On va definir et changer l'etat d'un room par son roomId et son state de type RoomState.
<ol>
<li>On verifie d'abord que le state est valable c'est à dire qu'il existe sur enum
on definit la foction checkRoomState qui prend comme parametre state de type any tout accepter tout type


````javascript
export function checkRoomState(state: any) {
    return Object.keys(RoomState).some((roomState) => roomState === state)
}
//state doit être au moins un des clés de l'objet roomState.
````
</li>
<li>On cré la fonction setRoomByState qui prend en parametre roomId et state de type RoomState

````javascript
export async function setRoomByState(roomId: string, state: RoomState): Promise<string> {
    if (!roomId) {
        throw new Error ('roomId is missing');
    }
    if (!checkRoomState(state)) {
        throw new Error ('state is invalid');
    }
    const roomRef: DocumentReference = roomsRef.doc(roomId);
    const snapRoomRef: DocumentSnapshot = await roomRef.get();
    if (!snapRoomRef.exists) {
        throw new Error (' snapRoomRef does not exists');
    }
    await roomRef.update({roomState: state});
    return 'ok';
}
````
Òn fait un ``update`` sur roomRef pour donner a roomState la valeur state
</li>
<li>Dans le fichiser server.ts choisit PATCH <br>
On definit le roomId et le state qui sont les parametres de la fonction setRoomByState

````javascript
app.patch('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const state = req.body.roomState;
        const opeartionResult = await setRoomByState(roomId, state);
        return res.send(opeartionResult);

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
````

</li>
<li>On renseigne un id de room et dans body on definit notre state

````javascript
{
	"roomState": "isavailable"
}
````
On choisit bien le roomState dans les clés de l'objet roomState non pas les valeurs.

</li>
On obtient le resultat ok sur postman comme demander <br>
Puis on retourne sur notre base de données on voit dans le room choisit par son id le roomState isavailable


	roomState: "isavailable"

</ol>

## Get state room

On va geter le state d'un room par son id
<ol>
<li>On cré la fonction getRoomState qui prend en parametre roomId de type string qui renvoit le state de room.

````javascript
export async function getRoomState(roomId: string): Promise<RoomState> {
    const room: roomModel = await getRoomById(roomId);
    return room.roomState;
}
````

</li>
<li>Dans le fichier server.ts on definit le roomId et on appelle la fonction

````javascript
app.get('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const roomState = await getRoomState(roomId);
        return res.send({roomState});

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
</li>
<li> Sur postman son choisit get et on choisit un roomId on a ainsi l'etat de la chambre choisit

````javascript
{
    "roomState": "isavailable"
}
````

</li>
</ol>