# Put Element
On effectue le **PUT** en faisant un set
<ul>La fonction permettant de determiner si l'hotel existe</ul>

``````javascript
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo4-node.firebaseio.com'
});

const db = admin.firestore();
const refHotels = db.collection('hotels');
export async function testHotelExisteById(hotelId: string): Promise<DocumentReference> {
    const hotelRef: DocumentReference = refHotels.doc(hotelId);
    const snapHostelToDelete: DocumentSnapshot = await hotelRef.get();
    const hotelToDelete: HotelModel | undefined = snapHostelToDelete.data() as HotelModel | undefined;
    if (!hotelToDelete) {
        throw new Error(`new hostel must be filled`);
    }
    return hotelRef;
}
``````
### Doc
``doc(documentPath: string): DocumentReference<T>;``
<ul>
<li>Il ajoute un nouveau document à cette collection(base de données) avec les données spécifiées, 
en lui attribuantun automatiquement un ID de document.</li>
<li>param data Un objet contenant les données du nouveau document.</li>
<li>Il retourne une promesse résolue avec une DocumentReference pointant vers le
document nouvellement créé après avoir été écrit dans le backend.</li>
</ul> 

### GET

``get(): Promise<DocumentSnapshot<T>>;``

Retourne une promesse de capture de document.

### Data
``data(): T | undefined; <br>``

T est le type de notre variable.
Il retourne les données à l'emplacement de champs spécifiés 
ou Undefined si aucun champ de ce type n'existe dans le document.

### Delete

``delete(precondition?:Precondition): Promise<WriteResult>``
Lit le document référencé par ce «DocumentReference».<br>
Il retourne une promesse résolue avec un DocumentSnapshot contenant le
contenu actuel du document. (Une promesse WriteResult)

### Set

``set(data: T, options?: SetOptions): Promise<WriteResult>;``

Il fait une mise à jour des données reférés par ce Document de réference.<br>
La mise à jour échouera si elle est appliquée à un document qui n'existe pas.<br>

Il retourne une promesse résolue avec l'heure d'écriture de cette mise à jour.


<ul>On définit la fonction qui permet de faire le put </ul>

````javascript
export async function hotelToPut(hotelId: string, newHotel: HotelModel): Promise<HotelModel> {
    if(!hotelId || !newHotel) {
        throw new Error(`hostel data and hostel data id must be failled`);
    }
    const hotelToPutRef: DocumentReference = await testHotelExisteById(hotelId);
    await hotelToPutRef.set(newHotel);
    return newHotel;
}
````
<ul>On appelle la fonction sur notre fichier principale (server.ts)</ul>

````javascript
app.put('/api/rooms/:id', (req, res) => {
    try {
        const id: number = parseInt(req.params.id);
        const roomAdd: roomModel = req.body;
        const rooms = getRoomToPut(id, roomAdd);
        return res.send(rooms);
    }catch(e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
On definit notre hotel sur **postman** pour le remplacer avec un hotel de notre base de donnée en renseignant son id sur l'url dans postman.
````javascript
 {
        "name": "hotel Ramadan",
        "roomNumbers": 100,
        "pool": false
 }
````
l'hotel avec l'Id : IWPMFMCLHsnLlBxqEvdF <br>
On peut changer les attibuts de l'objet et ils seront changés dans le base de données
.
