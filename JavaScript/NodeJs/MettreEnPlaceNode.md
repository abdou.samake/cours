# Mettre en place Node

Pour mêttre en place NodeJs : 
<ul>
<li>On récupère le repot</li>
<li>Sur terminal on se position d'abord sur server en faisant cd server. </li>
<li> On installe avec npm i</li>
<li> on installe concurrently avce la ligne de commande 

``
npm install -g concurrently
``
</li>
<li> On lance pour la prémière fois Watch puis on le ferme en faisant clique droit sur package.json puis show npm script et double clique sur Watch</li>
<li> on lance serve</li>
<li>On ouvre le fichier server.ts</li>
On prend connaissance de la fonction

``
express() 
``
c'est le serveur node js, c'est un outil donné par node qui permet de lancer un serveur.
</ul>

````javascript
import express from 'express';
const app = express();
app.get('/api', (req, res) => {
return res.send('Hello world');
});

app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````
