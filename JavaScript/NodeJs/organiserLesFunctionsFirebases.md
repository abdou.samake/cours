# Organiser les fonctions firebase

Pour créer une fonction sur firebase:
<ul>
<li>

on cré un dossier qu'on nomme par exemple **backend**
</li>
<li>

On cré un sous dossier dans **backend** nommé **onUserCreated**</li>
<li>Sur terminal on se positionne d'abord sur le dossier backend par 


```cd backend```

ensuite sur le dossier onUserCreated


``cd onUserCreated``

</li>
<li> Si on avait déja installer firebase dans la racine de notre projet, il va falloir le neutraliser en ajoutant sur firestorsc et firestore.json bis ou tot ou n'imoprte quoi.</li>
<li>Sur terminal on tape la commande

```firebase init functions```

</li>
<li>On choisit un projet existant sur firebase</li>
<li>On choisit typescript</li>
<li>

Et on répond à toutes les questions suivantes par **yes**</li>
<li>

Dans le dossier onUserCreated on a un dossier scr dedans on ouvre le fichier **index.js** <br>
on a une fonction en commenetaire, on enléve les commentaires sur la fonction et on la renomme **onUserCreates**


````javascript
import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const onUserCreated = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

````

</li>
<li>On ouvre package.json, sur deploy on ajoute : onUserCreated comme ceci:


````javascript

    "deploy: onUserCreated": "firebase deploy --only functions: onUserCreated",
    "logs": "firebase functions:log"
  },
```` 

</li>
<li> On fait clique droit sur package.json puis show npm et on voit apparaitre la fonction onUserCreated.</li>
</ul>