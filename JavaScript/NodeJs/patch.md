## Patch
**Patch** permet de modifier potentiellement un des attributs ou plusieurs des attributs.
Il permet de mettre à jour une seule information de l'ojet.
On definit l'attribut a modifier de notre objet sur **postman**
````javascript
{
	"lastName": "Le boss"
}
````

````javascript
import express from 'express';
import bodyParser from 'body-parser';
const app = express();
app.use(bodyParser({}));
app.get('/api', (eq, res) => {
    return res.send('coucou promo 4')
});
let users = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 28,
        id: 1
    },
    {
        firstName: 'mamad',
        lastName: 'samak',
        age: 28,
        id: 2
    },
    {
        firstName: 'bouba',
        lastName: 'samake',
        age: 28,
        id:3
    }
];
app.get('/api/users', (eq, res) => {
    return res.send(users);
});
app.patch('/api/users/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const newUser = req.body;
    const index = users.findIndex((user) => user.id === id);
    users[index] = {...users[index], ...newUser}; // on récupére aussi les autres information de l'objet.
    return res.send(users);
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
````

On selectionne **Patch** puis renseigne l'url http://localhost:3015/api/1 et indique l'id 1 de l'objet a remplacer.
on obtient le resultat.
````javascript
[
    {
        "firstName": "abdou",
        "lastName": "Le boss",
        "age": 28,
        "id": 1
    },
    {
        "firstName": "mamad",
        "lastName": "samak",
        "age": 28,
        "id": 2
    },
    {
        "firstName": "bouba",
        "lastName": "samake",
        "age": 28,
        "id": 3
    }
]
````
