# Lié la fonction déployée avec Firebase

<ul><li>

Pour commencer on met la clé de notre projet Firebase dans le dossier **src** prés du fichier **index.ts** en créant le dossier cred. </li>
<li>

Dans le fichier index.ts on met le **admin.InitializeApp** pour activer la clé</li>
<li>

On doit créer le watch pour pouvoir changer le code et être initialisé dans le dossier Lib. Pour cela
dans **package.json** on ajoute `"watch": "tsc -w",` </li>
<li>On utilise emulator en ajoutant firestore, puis on ajoute serve qu'on va utiliser pour relancer notre fonction:


````javascript

    "watch": "tsc -w",
    "emulate": "npm run build && firebase emulators:start --only functions,firestore",
    "serve": "npm run build && firebase serve",
````

</li>
<li>En faisant clique droit sur package.json puis show npm script on a sur la liste watch, serve et emulate. On peut ainsi lancer le watch et le serve.</li>
<li>On peut ainsi modifier notre fonction et relancer le serve puis avoir le resultat.</li>
</ul>

## Express
Si on veut faire des get, put, patch, post il faudrait qu'on utilise app express</li>
<ul><li>

On telecharge express sur terminal en se positionnant sur functions grace à ``cd functions``<br>
En suite on tape

``npm i -S express`` <br>
En suite on l'importe dans index.ts

``import * as express from 'express'`` <br>

on definit la constante app

``const app = express();``

</li>
<li>On fait par exemple un get sur notre collection hotel de notre base de données


````javascript
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import * as express from 'express'

admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: 'https://wr-promo4-node.firebaseio.com'
});

const db = admin.firestore();
const refHotels = db.collection('hotels');

const app = express();

app.get('/hotels', async (request, response) => {
    const snapShotHoteleRef = await refHotels.get();
    const hotels: any[] = [];
    snapShotHoteleRef.forEach((snapHotel) => hotels.push(snapHotel.data()));
    response.send(hotels);
});
 export const myTest = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https
     .onRequest(app);

````

</li>
<li>

A noté qu'on return app sur **onRequest**</li>

</ul>