## Le typage

Le typage consiste à préciser de quels types sont les objets.
On procéde comme suit :
<ul>
<li>On cré un dossier modesls</li>
<li>Dans ce dossier on cré autant de fichiers typescript interface qu'on a d'objets </li>
On effectue le typage en profondeur c'est à dire on commence toujours par l'objet qui est à l'intérieur d'un autre.
</ul>

````javascript
const companie: companyModel = {
    name: '',
    adresse: '',
    size: 100,
    users: []
};
let users: UserModel[] = [
    {
        firstName: 'abdou',
        lastName: 'samake',
        age: 29,
        car: false,
        id: 1
    },
    {
        firstName: 'major',
        lastName: 'coly',
        age: 29,
        car: true,
        id: 2
    },
    {
        firstName: 'demba',
        lastName: 'ba',
        age: 29,
        car: false,
        id: 3
    }
];
````
On cré des fichiers typescript users.models.ts et company.models.ts
et respectivement sur chaque fichier on effectue:
````javascript
export interface UserModel {
    firstName: string;
    lastName: string;
    age: number;
    car: boolean;
    id: number
}
````
````javascript
import {UserModel} from "./user.model";

export interface companyModel {
    name: string;
    adresse: string;
    size: number;
    users: UserModel[]
}
````
L'objet users est dans company donc on commence le typage par users ensuite company, c'est le principe de typage en profondeur.<br>
Et pour finir devant chaque objet on indique son type.
````
companie: companyModel

users: UserModel[]
````

````javascript
function getUserById(id: number): UserModel | undefined {
    return users.find((user) => user.id === id);
}
console.log(getUserById(1));
//  { firstName: 'abdou', lastName: 'samake', age: 29, car: false, id: 1 }

````
En **typescript** le ou s'ecrit avec **|**.