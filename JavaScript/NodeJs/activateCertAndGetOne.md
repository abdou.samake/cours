# Activate cert and get one
On procéde à l'activation de la clé pour accéder à la base de donnée.
<ul>
<li>Dans Firebase => paramétre => paramétre du projet => compte de service

**SDK Admin Firebase**
On choisit le langage Node.js
Puis on copie le bout de code et on le remplace avec le code d'initialisation d'avant dans service.ts

````javascript
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wr-promo4-node.firebaseio.com"
});
````


</li>
<li>On fait (ctrl + clique sur cert) pour ouvrir la fonction de cert</li>
<li>

On type l'objet cert à **serviceAccount** <br>
On import d'abord l'interface en faisant (Alt + entré) <br>
On modifie l'objet selon qu'il est définit dans serviceAccount en changeant les noms des attributs et enlever ceux qui n'existent pas dans serviceAccount.
</li>
<li>On remplace dans le code d'initialisation (serviceAccount) par l'objet cert typé serviceAccount, il apparait en rouge, il faut juste importer l'interface en faisant (Alt + entré et choisir on modifie la réference)


````javascript
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo4-node.firebaseio.com'
});
````

</li>
<li>On définit la variable ref

``const ref = db.collection('hotels').doc(clé)``<br>
Pour avoir accé à la base de donnée on va chercher le document qui a l'unique Id généré par firebase.
</li>
<li>Et pour finir , pour avoir le resultat des data qui sont dans hotel il faut rajouter

``.data()``
ON ainsi le code du get

````javascript
app.get('/api/hotels', async (req, res) => {
    try {
        const hotel = await ref.get();
        return res.send(hotel.data());
    } catch(e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
On obtient bien l'hotel qu'on cré dans firebase.
</li></ul> 

````
{"name":"hotel rose","roomNumbers":10,"pool":true}
````
