# Delete Element

La premiére chose à faire est de repèrer quel élément on veut supprimer. Pour cela on va dans la base de données crée on copie l'id de l'élément (hotel).<br>

<ul>ON cré la fonction qui permet de suprimer notre élément.</ul>

````javascript
export async function deleteHotelById(hotelId: string) {

if (!hotelId) {
    throw Error ('hotel id is missing');
    return await refHotels.doc(hostelId).delete;
}
````
<ul>Dans le fichier principal</ul>

````javascript
app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: writeResult = await deleteHotelById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
``.delete`` renvoit un **WriteResult** qui est un objet qui nous dit a quel heure le write à été fait. <br>
On va ensuite sur postman on choisit  l'option **DELETE** et on colle sur l'url l'id de l'élément à supprimer.
Et on a l'hotel qui a été bien supprimé. <br>
<ul>Pour s'assurer que l'hotel qu'on delete existe bien on apporte ces quelques modification sur la fonction en faisant un get sur la reference de l'hotel (hotelRef):
</ul>

````javascript
async function testIfHostelExistsById(hostelId: string): Promise<DocumentReference> {
    const hotelRef: DocumentReference = refHotels.doc(hostelId);
    const snapHostelToDelete: DocumentSnapshot = await hotelRef.get();
    const hostelToDelete: HotelModel | undefined = snapHostelToDelete.data() as HotelModel | undefined;
// (as) on a forcer le type de hostelToDelete à HotelModel | undefined
    if (!hostelToDelete) {
        throw new Error(`${hostelId} does not exists`);
    }
    await hotelRef.doc(hotelId).delete();
    return `${hostelId} -> delete ok`;
}
````
Ainsi on vérifie bien que l'hotel existe avant de le supprimer de la base de données.