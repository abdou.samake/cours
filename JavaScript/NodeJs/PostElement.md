# Post  element 
On ajoute un element dans la base de données en effectuant du code avec **post**.
D'une maniére générale on cré des petites fonctions dans un autre fichier typescript (ts) et depuis la on fait nos modifications en fonction de ce qu'on veut faire.<br>
Pour faire le poste on cré la fonction **postNewHostel**


````javascript
const db = admin.firestore();

const refHotels = db.collection('hotels');

export async function postNewHostel(newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel) {
        throw new Error(`new hostel must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refHotels.add(newHostel);
    return {...newHostel, id: addResult.id} // nous renvoi le nouveau et son id;
}
// Promise<HotelModel> on fait une promess de type HotelModel
//puisqu'on veut poster un hotel, on ajoute dans la reference de la base de données newHotel qui sera assigné 
// par l'hotel cré sur postman.
```` 

Dans le fichier principal on effectue le code du **post** en appelant la fonction **postNewHostel** crée

````javascript
app.post('/api/hostels', async (req, res) => {
    try {
        const newHostel = req.body;
        const addResult = await postNewHostel(newHostel);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
On peut maintenant aller sur postman choisir l'option **post** et cré notre objet hotel à ajouter à la base de données puis envoyer.<br>
On constate que l'objet a été bien ajouté et si on send plusieurs fois , il nous ajoute plusieurs objets a notre base de données avec des Id differents.<br>

```Pormise<string>``` : c'est une promesse de string <br>
Puisqu'on a une fonction **asynchrone** donc on est dans une promesse donc la fonction return un Promise<HotelModel> puisque la fonction renvoi l'hotel ajouté et son id.






