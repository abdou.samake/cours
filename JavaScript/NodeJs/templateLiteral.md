# Template litéral

C'est une façon d'ecrire des **strings** d'une façon différente. <br>
On fait des quotes à l'envers (alt gr + 7) et on peut écrire de façon libres à l'intérieure des quotes à l'envers.

`````javascript
const hostelId;
`${hostelId} => delete ok`;
`````
le ${hostelId} permet d'utiliser la variable hostelId déja déclaré.