## Try Catch

**try catch** permet d'attraper des erreurs pour eviter que les serveurs qui hebergent les applications se planquent.<br>
On fait un `try` de ce qu'on veut faire sur notre code puis on fait un `catch(e)` et envoyer le message d'erreur de `e.message`

````javascript
app.get('/api', (req, res) => {
    try {
        return res.send('hello promo 4');
    } catch(e) {
        console.error(e.message);
        return res
            .status(500) // c'est à dire le serveur à planquer
            .send({erreur: 'erreur serveur: ' + e.message}) // envoie le message d'erreur.
    }
});
````
