# Déployer une fonction
Déployer une fonction c'est mettre une fonction en ligne. Pour le faire on procéde comme suit:
<ul>
<li>
Dans notre projet on s'assure qu'il ya pas de firebase sur la racine, il ne doit y avoir que le fichier .idea
</li>
<li>On cré ensuite un dossier functions, puis un autre dossier test qui va être notre fonction.</li>
<li>Sur terminal on se positionne sur le dossier test</li>
<li>on fait 

``firebase init``

</li>
<li>On selectionne
<ul>
<li>Firestore</li>
<li>Function</li>
<li>Hosting</li>
<li>Emulators</li></ul></li>
<li>

On choisit ensuite **typeScript**</li>

<li>Pour le dossier d'utilisation on tape

``functions/lib`` <br>
functions on la dans le dossier test
</li>
<li>Ensuite on répond à la question suivante par No</li>
<li>Il nous demande de paramétrer l'Emulateur, on selectionne
<ul>
<li>Function</li>
<li>Firestore</li>
<li>Hosting</li></ul></li>
<li>On tape sur entré ensuite</li>
<li>On répond par Yes pour telecharger l'emulators</li>
<li>

Dans le fichier index.ts on renomme la fonction donné par test, on héberge notre fonction en europe par `.region()`
On clique + entré pour voir le Doc et on choisit.</li>
<li>

On choisit la taille du fichier à déployer par `.runwith`</li>


````javascript
import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
 export const tests = functions
     .region("europe-west2")
     .runWith({memory: "128MB"})
     .https
     .onRequest((request, response) => {
  response.send("Hello from white Rabbit Promo4!");
 });

````
<li>

On sécurise notre fonction en allant sur package.json, sur deploy on colle le nom de la fonction **test** et sur only -- function on colle aussi le nom **test**
On change Node de 8 à 10.


````javascript
"deploy: tests": "firebase deploy --only functions:tests",
    "logs": "firebase functions:log"
  },
  "engines": {
    "node": "10"
  },
``````


</li>
<li>On clique droit sur package.json puis run npm script et on double clique sur deploy: test</li>
</ul>
On obtient ainsi l'url de la fonction qui est mise en ligne.