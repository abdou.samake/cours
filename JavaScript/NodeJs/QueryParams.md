# Query Params

Les query c'est quand on demande quelque chose avec des conditions comme par exemple la limite de quelque.
<ul>On choisit dabord un GET quelque
<li>


````javascript
app.get('/api/testQuery', async (req, res) => {
    try {
        const limit = req.query.limit; 
        return res.send('coucou' + limit);
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````

</li>

<li>Pour notre query on peut specifier notre paramètre en mettant un point d'interrogation (?) avant la demande sur localhost.

``http://localhost:3015/api/testQuery?limit=3``

Et nous affiche coucou3

</li>

<li>Si on definit notre return en un objet on verra que les query renvoient des strings

````javascript
app.get('/api/testQuery', async (req, res) => {
    try {
        const limit = req.query.limit;
        return res.send({limit});
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````

On obtient le resultat suivant:


``{"limit":"3"}``  qui indique trois comme un string.
</li>

<li>Si on fait un query en iindiquant un boolean on aura un string aussi

``http://localhost:3015/api/testQuery?limit=true``

On obtient


````javascript
{"limit":"true"}
````

</li>

<li>Si on definit une seconde variable designant un query, on met un et commerciale (&) pour indiquer qu'on met un second argument sur l'url

````javascript
app.get('/api/testQuery', async (req, res) => {
    try {
        const limit = req.query.limit;
        const toto = req.query.toto;
        return res.send({limit, toto});
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````

``http://localhost:3015/api/testQuery?limit=true&toto=coucou``

On obtient le resultat 

``{"limit":"true","toto":"coucou"}``


</li>
<li>On peut faire autant de query qu'on veut

````javascript
app.get('/api/testQuery', async (req, res) => {
    try {
        const limit = req.query.limit;
        const toto = req.query.toto;
        const tata = req.query.tata;
        return res.send({limit, toto, tata});
    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````


``http://localhost:3015/api/testQuery?limit=true&toto=coucou&tata=5``

on a le resultat

``{"limit":"true","toto":"coucou","tata":"5"}``

</li>

<li>On note qu'on utilise les query uinquement pour les GET.</li>
</ul>