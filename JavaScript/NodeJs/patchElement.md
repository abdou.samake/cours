# Patch Element
<ul>Commen toujours on reprend la fonction qui permet de determiner si l'hotel existe</ul>

````javascript
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo4-node.firebaseio.com'
});

const db = admin.firestore();
const refHotels = db.collection('hotels');
export async function testHotelExisteById(hotelId: string): Promise<DocumentReference> {
    const hotelRef: DocumentReference = refHotels.doc(hotelId);
    const snapHostelToDelete: DocumentSnapshot = await hotelRef.get();
    const hotelToDelete: HotelModel | undefined = snapHostelToDelete.data() as HotelModel | undefined;
    if (!hotelToDelete) {
        throw new Error(`new hostel must be filled`);
    }
    return hotelRef;
}
````
<ul> On cré ensuite la fonction permettant de faire le patch, on l'appelle 

**UpdateHotel** car le **patch** se fait avec **Update**
</ul>

````javascript
export async function updateHotels(hotelId: string, newHotel: HotelModel): Promise<HotelModel> {
    if (!hotelId || !newHotel) {
        throw new Error('hotel and newHotel are not existe')
    }
    const hotelToUpdateRef: DocumentReference = await testHotelExisteById(hotelId);
    await hotelToUpdateRef.update(newHotel);
    return newHotel;
}
````

### Update 
``update(data: UpdateData, precondition?: Precondition): Promise<WriteResult>;``

Il fait la mise à jour d'une donnée de réference dans ce Document de Réference. <br>
La mise à jour échoue si le document de réference n'existe pas.
Il renvoit une promesse résolue avec le temps exacte de la mise à jpur.

<ul>On appelle notre fonction dans le fichier server.ts</ul>

````javascript
app.patch('/api/hotels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newHotel = req.body;
        const hotel = await updateHotels(id, newHotel);
        return res.send(hotel);

    } catch (e) {
        console.error(e.message);
        return res
            .status(500)
            .send({erreur: 'erreur serveur: ' + e.message})
    }
});
````
<ul>On va sur postman on renseigne sur l'url l'identifiant de l'objet qu'on veut mise à jour et on cré l'objet apportant les modifications. </ul>
Et on aperçoit les modifications apportées sur notre base de données dans firebase.
 Par contre si on définit notre nouvelle objet avec des attibuts qui n'existait pas avant sur l'objet qu'on veut updaté, cette nouvelle objet sera ajouté dans l'objet.
 
````javascript
{
    "roomNumbers": 30;
    "pool": true;
}
````
Aprés mise à jour on a dans notre base de données: 
````javascript
{
    name: hotel Ramadan;
    roomNumbers: 30;
    pool: true;
}
// l'attribut pool qui n'existait pas avant a été ajouté.
````
<ul>La difference avec PATCH qaund on renseigne un nouveau attibut ça ne l'ajoute pas mais remplacer tout par ce nouveau attribut avec sa valeur.</ul>
