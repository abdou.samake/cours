# Présentation du Playground

Playground c'est l'endroit qu'on met en place pour travailler sur javaqcript.<br>
On procéde comme suite:
<ol>
<li>On récupére le repo du TryHard</li>
<li>Une fois le repo reçu on ouvre le fichier package.json et on tape la commande npm i pour l'installation</li>
<li>Une fois installer on clique droit sur package.json on sélectionne show npm scrip</li>
<li>On obtient le fichier qui apparait puis on clique droit sur serve</li>
<li>Ensuite on clique sur localhost qui nous dirige sur la page du fichier.</li></ol>