# Organiser son code en Javasript

La présentation du code est très important dans le travail du développeur. Il faut bien organiser son code avec clareté .<br>
Pour organiser son code:
<ul>
<li>On crée un fichier1 où on met le code</li>
<li>Un autre fichier2 où on met les exécutions console.log par exemple.</li>
</ul>
Sur le fichier1 on met export devant l'élèment qu'on veut exécuter.<br>
Sur le fichier2 on met import{le nom} from 'le chemin.

`````javascript
export const tab=[me, user2, user3, user4];

import{tab} from './user.data;