# Every et Some
Ils ont des opérateurs qui renvoi un booléen.
## Every

````javascript
const allhostelsHavePools = hostels.every((hotel) =>hotel.pool === true);
console.log(allhostelsHavePools);

``````
ça revoit **true** si tout les hotels ont un pool = true, sionon on aura le resultat false.

## Some
````javascript
const = allhostelsHavePools = hostels.some((hotel) =>hotel.pool === true);
console.log(allhostelsHavePools);
````
Pour **some** il suffit au que un hotel ait au moins pool = true pour que ça renvoit la valeur **true**, s'ils sont tous false, on aura comme resultat **false**.
