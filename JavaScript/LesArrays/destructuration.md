# Destructuration des tableaux

La destructuration est une façon de créer une variable en destructurant un tableau en petites variables.

````javascript
const tab = ['tata', 'titi', 'toto', 'tutu'];
`````
**la Destructuration:**
````javascript
const [var1, var2, var3, var4] = tab;

console.log(var1) // tata
console.log(var2) // titi
console.log(var3) // toto
console.log(var4) // tutu
`````

````javascript
const [, , var3, ] = tab;

console.log(var3) //toto
````

