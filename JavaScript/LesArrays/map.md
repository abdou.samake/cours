# Opérateur map et passage par réference

L'opératuer **map** fonctionne à peu prés de la même façon que foreach, la différence est que **map** renvoi une valeur.

````javascript
const tab2 = tab.map(user, index) =>{
    return 12
};
console.log(user);
````

````javascript
const tab2 = tab.map(user,index) =>{
    const temp ={...user};
    temp.index= index;
    return user;
};
````

``{...user} permet de copier les user et les mettre dans un autre tableau``