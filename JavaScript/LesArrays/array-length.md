# Array length and index

Dans un tableau on a les index et les éléments. Les index désignent le numéro de la position des éléments.
Les index commencent toujours par zero dans un tableau.

l'index 0 désigne le prémier élément du tableau<br>
l'index 1 désigne le deuxiéme élément du tableau<br>
l'index 2 désigne le troisiéme élément du tableau<br>
Ainsi de suite.<br>
Pour afficher le prémier élément on fait
````javascript
console.log(tab[0]);
````
Si l'index n'est pas défini dans le tableau on obtient UNDEFINED sur le console.<br>

Pour connaitre la longueur d'un tableau :

````javascript
console.log(tab.lenght);
````
lenght est la fonction qui dermine la longueur.<br>
Pour afficher le dernier élément du tableau: 
````javascript
console.log(tab.length-1);
````
tab.length - 1 est l'index du dernier élément du tableau.