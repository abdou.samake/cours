# Functions
On peut définir une fonction de la façon suivante :
````javascript
function testFunction(a, b){
    return a + b;
}
````
Il existe une autre autre façpon de définir une fonction, on remplace le nom **function** par une flêche **=>** on l'appelle **fonction arrow**.
````javascript
const testFunction = (a,b) => {
    return a + b;
}
````
On note:
````javascript
tab.filter(() => a + b)
`````
C'est pareil que 
````javascript
tab.filter(() => {
    return a + b;
});
````