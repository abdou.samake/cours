# Array Opérateur foreach

Un opérateur est un outil qui permet de manupiler un objet et de lui fait faire des choses.
On a l'opérateur foreach.

`````javascript
const tab = [me, user1, user2,user3, user4];

tab.foreach(user =>console.log('user',user));
``````
Ici foreach va parcourrir tout le tout le table et pour chaque élément de la table il l'appele user et affiche l'élément.

'template de string' qui se fait par (altgr + 7) permet d'écrir des chaines de caractéres à l'intérieur libremenet sans se prendre la tête.

````javascript
tab.foreach(user, index) => {
    console.log('user', index, user);
};
````
Ici la fonction de callback va parcoucour le tableau nomme chaque élément du tableau par user et son index, elle affiche
l'element et son index comme ça : user: 0 => me,

````javascript
tab.foreach(user, index) => {
    console.log(`l'utilisateur dont l'index est ${index} s'appelle ${user.firstname}`);
};
````
Affiche ici parexemple l'utilisateur dont l'index est 0 s'appelle Antoine 
 ````javascript
tab.foreach((user, index) => console.log(user.age +10));
````
Ici on ajoute 10 à chaque age d'un user et l'afficher.

````javascript
tab.foreach(user => console.log(user.lastname));
````
Affiche le nom de chaque utilisateur.