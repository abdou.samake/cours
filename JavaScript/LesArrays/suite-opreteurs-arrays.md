# Suite des opérateurs arrays
## Opérateur filter
**Filter** est un opérateur qui sert à filtrer un tableau.

### Syntax:

````javascript
const tab=[me, user, user1, usre2, user3, user4]
const filterArray = tab.filter(user => user.age >30);
console.table(filterArray);

//On va obtenir dans un tableau que des gens d'âge supérieur à 30 ans.

````
#### Programmation Fonctionnelle:

````javascript
const filteredArray = tab
    .filter(isOver30)
    .map(addsAtend);
function isOver(user){
    return user.age>30;
}
function addsAtend(user){
    user.lastname = user.lastname +'s';
}
// On effectue des opérations de filter et map sur des fonction qui vont étre définies par la suite.
````

## Sort
**sort** permet de mettre de l'ordre dans un tableau
### Syntax:
````javascript
const sortedArray = tab.sort((user1 - user2) =>
    user1.age - user2.age)
console.table(sortedArray)
````
Ici on compare l'age des utilisateurs deux à deux et la machine s'en charge de faire le tri.
<ul><li>Faire par ordre alphabétique</li></ul>

````javascript
const sortedArray = tab.sort((user1 - usre2) =>{
    if(user.firstname < user2.firstname){
        return -1;
}
else{
    return +1;
}
});
console.table(sortedArray):
//les prénoms seront rangés par ordre alphabétique dans le tableau.
````
<ul><li>Autre méthode</li></ul>

````javascript
const sortedArray = tab
    .sort((user1, user2) =>user1.firstname < user2.firstname ? -1 : +1);

````
? est le prédicat, <ul><li>Si c'est vrais on renvoit -1</li></ul>
<ul><li>Si c'est faux on renvoit +1</li></ul>

On peur ajouter les pérateurs

````javascript
.filter(user => user.firstname.includes('a'))
.map(user=>{
    user.age = user.age +10;
});
````

## Concaténation

### l'opérateur concat

````javascript
const tab2 =[1, 2, 3];
const tab3 =[3, 4, 5];
const tab4 = tab2.concat(tab3);
console.log(tab4);
````
**concat** construit un nouveau tableau dan lequel il ajoute tab3 dans tab2.

## Autre méthode
````javascript
const tab4 = [...tab2, ...tab3];
````
...tab2, ...tab3 signifie qu'on copie tab2 et tab3 dans tab4.

## splice
**splice** permet d'insérer, de supprimer un élément dans un tableau.
### Syntax:

````javascript
const tab2 = [1, 2, 3, 4, 5, 7];
 tab3 = tab2.splice(start:5, deletecount:0, items:6);
console.log(tab3);
// à partir de l'index 5, on ajoute 6 et on supprime 0 élément
// tab3 = [1, 2, 3, 4, 5,6, 7];
tab4 = tab2.splice(5,1);
console.log(tab4);
// à partir de l'index 5 on supprime un élément.
// tab4 =[1, 2, 3, 4, 5,]
````

## slice

**slice** crée un nouveau tableau sans modifier le tableau de départ. On extrait les éléments définis.
### Syntax:
````javascript
const tab2 =[1, 2, 3, 4, 5, 6];
const tab3 = tab2.slice(1, 4);
// On commence par l'index 1 inclus jusqu'a l'index 4 exclus et on garde ces éléments
console.log(tab3);
// tab3 = [2, 3, 4]
````