# Récupérer les valeurs du formulaire

On récupére les valeurs du formulaire en faisant un console.log de l'etat du formulaire puis on valide

````html
 function handleSubmit(event) {
        event.preventDefault();
        console.log(state);
    }
````
On obtient sur console l'objet avec les valeurs de nom et age .<br>
Si on change la valeur de age on obtient le resultat affiché en string . Pour changer ça il faut préciser que si le type de id est un number on fait un parseInt de value pour obtenir le resultat en number.

````html
    function handleInput(event) {
        const {id, value, type} = event.target;
        console.log(id, value);
        setState({
            ...state,
            [id]: type === 'number' ? parseInt(value) : value
        })
    }
````
Et on obtient le resultat du formulaire sous forme d'objet.

````html
<script type="text/babel">
function MyForm() {
    const [state, setState] = React.useState({name:'coucou', age:12});
    function handleSubmit(event) {
        event.preventDefault();
        console.log(state);
    }
    function handleInput(event) {
        const {id, value, type} = event.target;
        console.log(id, value);
        setState({
            ...state,
            [id]: type === 'number' ? parseInt(value) : value
        })
    }
    return <form onSubmit={handleSubmit}>
        <label htmlFor="name">Nom</label>
        <input
            onChange={handleInput}
            value={state.name}
            id="name"
            type="text"/>
        <label htmlFor="age">Age</label>
        <input
            onChange={handleInput}
            value={state.age}
            id="age"
            type="number"/>
        <button type="submit">Valider</button>
        <button type="button">Annuler</button>
    </form>

}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>
````
 Resultat:
 
 ````html
{name: "coucou", age: 13}
````