# Récupérer les valeurs d'un Input
<ul>
<li>

````html
console.dir(event)
````
permet de recupérer plus d'information sur les inputs comme l'id, le type...
</li>
<li>

````html
console.dir(event.target)
````
target: c'est le le cible de l'input lui même qui est passé en réference<br>
Quand on clique sur age on obtient **input#age**<br>
Quand on ecrit sur l'input name on obtient sur le console **input#name**
</li>
<li>

````html
console.dir(event.target.id);// nous permet l'id des inputs
````
<ul><li>en cliquant sur age on a sur console l'id: age</li>
<li>en tapant un nom sur l'input nom on sur console l'id name</li></ul>


````html
console.dir(event.target.type)// nous permet d'avoir le type, on a number avec age et text avec nom
console.dir(event.target.value) // nous donne les valeurs des inputs quand on clique sur age on a 
// la valeur de age sur input
// quand on ecrit sur l'input nom on a sur console le nom ecrit.
````
</li>
<li>On se rencontre que quand on écrit sur l'input name ou bien qu'on essaye de changer la valeur de age rien ne change au niveau du formulaire, on doit faire un setState pour pouvoir changer les valeurs passer en input nom et age.</li>
<li>On change l'id des inputs nom et age et leur apporter une nouvelle valeur par value

````html
function handleInput(event) {
        console.dir(event.target.id) 
        console.dir(event.target.value)
    }
````
<ul>
<li>l'id c'est l'id des input age et nom qu'on veut changer</li>
<li>Et value c'est la nouvelle valeur qu"il va prendre</li>
</ul>
On obtient ainsi surt console les id et valeurs des ages et nom qu'on renseigne pas de changement au niveau du formualire des valeurs par défaut
</li>
<li>Pour changer les valeurs par defaut on fait un setSate
<ul><li>On peut faire la destructuration de id et value </li>
<li>ensuite on prend tout les props de state viz (...state)</li>
<li>On change les id en leur donnant la valeur value ([id]: value)</li></ul>
</li>

````html
<script type="text/babel">
    function handleInput(event) {
        const {id, value} = event.target;
        console.log(id, value)
        setState({
            ...state,
            [id]: value
        })
    }
````
</ul>

On a le code complet:

````html
function MyForm() {
    const [state, setState] = React.useState({name:'coucou', age:12});
    function handleform(event) {
        event.preventDefault();
    }
    function handleInput(event) {
        const {id, value} = event.target;
        console.log(id, value)
        setState({
            ...state,
            [id]: value
        })
    }
    return <form onSubmit={handleform}>
        <label htmlFor="name">Nom</label>
        <input
            onChange={handleInput}
            value={state.name}
            id="name"
            type="text"/>
        <label htmlFor="age">Age</label>
        <input
            onChange={handleInput}
            value={state.age}
            id="age"
            type="number"/>
        <button type="submit">Valider</button>
        <button type="button">Annuler</button>
    </form>

}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>
````
On peut ainsi changer l'age et le nom et on le resultat qui s'affiche sur console.