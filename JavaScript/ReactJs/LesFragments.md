# Les Fragments

On cré deux div avec deux objects props pour faire la destructuration.

````html
 const className1 = 'main';
  const children1 = 'ma div1';

  const className2 = 'secondary';
  const children2 = 'ma div2';

  const props1 = {
    className: className1,
    children: children1,
  };

  const props2 = {
    className: className2,
    children: children2,
  };
````
On appelle le premier Div: 
`````html
const exempleDiv = <div {...props1} />;
`````
resultat: ma div1 
Si on appelle le deuxiéme Div2: 

`````html
const exempleDiv = <div {...props2} />;
`````
resultat: ma div2

Si on veut ajouter nos deux divs en les mettant cote à cote , on a une erreur car en Js 
on a forcément un div principal qui ferme les deux divs

```const exempleDiv = <div {...props1} />  <div {...props1} />;``` 
Ca nous enoi Error.<br>

Et si on met ces deux divs dans un div principal

````html
const exampleDiv = <div>
      <div {...props1} />
      <div {...props2} />
  </div>;
````
On obtient bien le resultat<br>
ma div1<br>
ma div2<br>
Mais en inspectant on voit que JS les a créé chacun dans un div apart, et ça peut être compliqué quand on cré un site où il va y avoir plusieurs divs.

On utilise ainsi le système des **fragment** <> </>

````html
const exampleDiv = <>
    <div {...props1} />
    <div {...props2} />
  </>;
````
Et on le resultat: <br>
ma div1<br>
ma div2<br>
Et nos div sont a plat et pas de div supplémentaire.
````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Les fragments</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');

  const className1 = 'main';
  const children1 = 'ma div1';

  const className2 = 'secondary';
  const children2 = 'ma div2';

  const props1 = {
    className: className1,
    children: children1,
  };

  const props2 = {
    className: className2,
    children: children2,
  };

  const exampleDiv = <>
    <div {...props1} />
    <div {...props2} />
  </>;

  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>

````
````html
<div id="root">
<div class="main">ma div1</div>
<div class="secondary">ma div2</div>
</div>
````