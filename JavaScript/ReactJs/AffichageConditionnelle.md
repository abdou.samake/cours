# Affichage Conditionnelle 

C'est le fait d'affcher en fonction de quelques choses.

<ul>
<li> 


**ManagerDisplay** on la créé pour gérer le display. On return à l'intérieur 

````<pre>{JSON.stringify(props)}</pre>````
 Parceque si on fait l'interpolation de propsdirectement ça ne marcherait pas car React ne sait pas faire l'interpolation des objects, il fait l'inter^polation des strings et des numbers. C'est pourquoi on transforme props en string grace à **JSON.stringify**
 
 
 ````html
<script type="text/babel">
 function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay(props) {
      console.log(props);
      return <pre>{JSON.stringify(props)}</pre>
  }

  function App() {
      return <>
          <ManageDisplay value={1}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````
Affiche:  {"value":1}

</li>
<li>Dans la fonction ManageDisplay on mettre les conditions qui permettent d'afficher les components Un, Deux, Trois

````html
function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay(props) {
      const value = props.value;
      if(value === 1) {
          return <Un/>
      }
      if(value === 2) {
          return <Deux/>
      }
      if(value === 3) {
          return <Trois/>
      }
      return <pre>{value}</pre>
  }

  function App() {
      return <>
          <ManageDisplay value={1}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````
Dand App si on met value = {1} on a le resultat: Je suis Un <br>
Si on met value = {2} on a le resultat: Je suis Deux <br>
Si on met value = {3} on a le resultat: Je suis Tois
</li>
<li>Dans ManageDisplay on pouvait remplacer


``const value = props.value;`` 

par   

``const {value} = props``

et aussi faire tout simplement 

``function ManageDisplay({value})``
et notre code se résume en :


````html

  function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay({value}) {
      if(value === 1) {
          return <Un/>
      }
      if(value === 2) {
          return <Deux/>
      }
      if(value === 3) {
          return <Trois/>
      }
      return <pre>{value}</pre>
  }

  function App() {
      return <>
          <ManageDisplay value={3}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````

</li>
<li>Si on veut rien on fait tout simplement return null au niveau de la condition

````html

  function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay({value}) {
      if(value === 1) {
          return <Un/>
      }
      if(value === 2) {
          return <Deux/>
      }
      if(value === 3) {
          return null
      }
      return <pre>{value}</pre>
  }

  function App() {
      return <>
          <ManageDisplay value={3}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````

</li>
<li>Ca marche toujours si on utilise 

**switch** **case** : c'est quand la condition dans if ne change pas et on change seulement les cas.

````html
  function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay({value}) {
     switch (value) {
         case 1:
             return <Un/>;
             case 2:
                 return <Deux/>;
                 case 3:
                     return <Trois/>;
                     default:
                         return null;


     }
  }

  function App() {
      return <>
          <ManageDisplay value={12}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````

</li>
<li>Si on a des cas simples à gérer comme seulement 1 cas on peut faire un ternaire:

````html
  function Un() {
      return <h1>Je suis Un</h1>
  }
  function Deux() {
      return <h1>Je suis Deux</h1>
  }
  function Trois() {
      return <h1>Je suis Tois</h1>
  }

  function ManageDisplay({value}) {
     return value === 1 ? <Un/> : <h1>erreur</h1>
  }

  function App() {
      return <>
          <ManageDisplay value={12}/>
      </>

  }

  ReactDOM.render(<App/>, document.getElementById('root'));

</script>
````
Et on ici le resultat : **erreur** puisque value !== 1
</li>
</ul>