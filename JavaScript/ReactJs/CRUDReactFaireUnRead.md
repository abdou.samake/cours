# Correction CRUD : Faire un Read
On veut afficher un utilisation dans un api en renséignant son uid dans un formulaire.
les données de l'utilisateur sont sur :

````html
const api = 'https://jsonplaceholder.typicode.com/users/';
````

<ul><li>On cré un component MyForm composé de deux parties.<br>
Puisqu'on change des utilisateurs par leur uid alors on fait setState des uid

````html
const [uid, setUid] = React.useState('');
````
<ol>
<li>Dans la prémiere partie on intégre  React.useEffect car on veut changer des données d'utilisation qui nous vient de l'extérieur(user)<br>
<ul>
<li>On definit une fonction asynchrone pour faire un await des données extérieures, on nomme la fonction getUserByUid

````html

     React.useEffect(() => {
         async function getUserByUid(uid) {
             if (!uid) {
                 handleUser({user: null});
                 return;
             }
             handleUser('...loading');
             const newUser = await axios.get(api + uid);
             handleUser(newUser);
         }

         getUserByUid(uid);
     }, [uid]);
````
<li>la condition if consiste à vérifier si l'utilisateur ne donne aucun uid dans le formulaire alors on obtient {"user": "null"} </li>
<li>Sinon on change l'utilisateur par newUser qui nous vient du get sur axios de l'api avec l'uid donné. Le changement d'utilisateur se fait par handleUser qui est le setState de user</li>
<li>Avant on fait un handleUser('...loading') pour attendre le temps l'user s'affiche.</li>
<li>Ensuite on fait l'appel de la fonction getUserByUid</li>
</li>
</ul>
</li>
<li><ul><li>Dans la deuxiéme partie on definit une fonction qui permet de gérer les input, handleInput<br>
Dans laquelle on change la valeur du input par setUid

````html
function handleInput(event) {
         const {value} = event.target; // const value = event.target.value
         setUid(value);
     }
````
</li>
On definit le formulaire

````html
     return <form>
         <label htmlFor="uid">Uid</label>
         <input
             value={uid}
             onChange={handleInput}
             type="text" id="uid"
         />
     </form>;
````
<ul>

</li>

</ol></li>
<li>On définit le component DisplayUser qui permet d'afficher user.

````html
  function DisplayUser({user}) {
      return <div>{JSON.stringify(user)}</div>;
  }
````
On fait juste un return du user transformer en string par JSON.stringify.
</li>
<li>On definit notre component principal App qui va comporter nos deux components MyForm et DisplayUser
<ul><li>On definit dabord le setState de user qui vont changer dans les components MyForm et DisplayUser

````html
  function App() {
    const [user, setUser] = React.useState({user: null});
    return <>
      <MyForm handleUser={setUser}/>
      <DisplayUser user={user}/>
    </>;
  }
````

</li></ul>
</li>
</ul>
On a ainsi le code complet

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Faire un READ</h1>
<p>Faire un formulaire qui prend en paramètre un Uid et qui affiche le résultat obtenu</p>
<p></p>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">

  const api = 'https://jsonplaceholder.typicode.com/users/';
  function MyForm({handleUser}) {
     const [uid, setUid] = React.useState('');

     React.useEffect(() => {
         async function getUserByUid(uid) {
             if (!uid) {
                 handleUser({user: null});
                 return;
             }
             handleUser('...loading');
             const newUser = await axios.get(api + uid);
             handleUser(newUser);
         }

         getUserByUid(uid);
     }, [uid]);

     function handleInput(event) {
         const {value} = event.target;
         setUid(value);
     }

     return <form>
         <label htmlFor="uid">Uid</label>
         <input
             value={uid}
             onChange={handleInput}
             type="text" id="uid"
         />
     </form>;
 }
  function DisplayUser({user}) {
      return <div>{JSON.stringify(user)}</div>;
  }
  function App() {
    const [user, setUser] = React.useState({user: null});
    return <>
      <MyForm handleUser={setUser}/>
      <DisplayUser user={user}/>
    </>;
  }

        ReactDOM.render(<App/>, document.getElementById('root'));
</script>

</body>
</html>
````
En donnant l'uid 1 on a le resultat:

````html
{"data":{"id":1,
"name":"Leanne Graham",
"username":"Bret",
"email":"Sincere@april.biz",
"address":{"street":"Kulas Light",
"suite":"Apt. 556",
"city":"Gwenborough",
"zipcode":"92998-3874",
"geo":{"lat":"-37.3159","lng":"81.1496"}},
"phone":"1-770-736-8031 x56442","website":"hildegard.org",
"company":{"name":"Romaguera-Crona",
"catchPhrase":"Multi-layered client-server neural-net",
"bs":"harness real-time e-markets"}},"status":200,
"statusText":"","headers":{"cache-control":"max-age=43200",
"content-type":"application/json; charset=utf-8",
"expires":"-1",
"pragma":"no-cache"},
"config":{"url":"https://jsonplaceholder.typicode.com/users/1","method":"get","headers":{"Accept":"application/json, text/plain, */*"},
"transformRequest":[null],"transformResponse":[null],"timeout":0,
"xsrfCookieName":"XSRF-TOKEN","xsrfHeaderName":"X-XSRF-TOKEN","maxContentLength":-1},"request":{}}
````