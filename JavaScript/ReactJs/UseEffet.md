# Use effet
````html
React.useEffect() permet l'aafichage des données qui vont venir de l'extérieur.

````
<ul>
<li>On cré d'abord l'api contenant l'url qui nous donne les données provenant de JSONplaceholder</li>
<li>On cré un user et setUser

````html
const [user, setUser] = React.useState(null);
````
<li>On cré le component getUserByUid(uid) qui prend en paramêtre uid</li>
<li>On verifie s'il ya pas d'uid on fait rien </li>
<li>On verifie si user existe, si c'est le cas on return rien</li>

````html
async function getUserByUid(uid) {
              if (!uid) {
                  return;
              }
              if (user) {
                  return;
              }
````
<li>Sinon on va stocker user dans une variable temp que va nous envoyer l'api par un get de axios et on fait un setUser de la variable temp

````html
              const temp = await axios.get(api + uid);
              setUser(temp)
````

</li>
<li>Aprés on appelle la fonction getUserByUid(uid) avec le paramêtre uid.</li>
<li>On return un div de user transformer en string par JSON.stringify()</li>
<li>Comme on traite des données qui viennent de l'extérieur on doit utiliser React.useEffect()
React.useEffet prend deux argument
<ul><li>le return d'une fonction qui va s'autoexécuter</li>
<li>l'autre argument est un tableau contenant user.</li></ul></li>
</ul>
On a le code

````html
<script type="text/babel">

  const api = 'https://jsonplaceholder.typicode.com/users/';
  function DisplayUserByUid({uid}) {
      const [user, setUser] = React.useState(null);

      React.useEffect(() => {
          async function getUserByUid(uid) {
              if (!uid) {
                  return;
              }
              if (user) {
                  return;
              }
              const temp = await axios.get(api + uid);
              setUser(temp)
          }

          getUserByUid(uid);

      }, [user]);


      return <div>{JSON.stringify(user)}</div>;
  }

      function App() {

          return <>
              <DisplayUserByUid uid={1}/>
          </>
      }
  ReactDOM.render(<App/>, document.getElementById('root'));
</script>
````
On obtient l'user de uid 1 dans la page.