# React Create React App présentation

Pour se connecter à Create React App:
<ul>
<li>On se connecte sur l'url:
github.com/facebook/create-react-app => User Guide => Get Started
</li>
<li>Pour installer Bootstrap:
=> Building Your App => Adding Bootstrap
=> sur terminal 


``npm install --save bootstrap``
</li>
<li>Sur le projet on ne touche pas le fichier 

**index.js** 

</li>
<li>On modifie le fichier en supprimant les parties qui nous intéresse pas, on se trouve donc avec ce code:

````html
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div class="container-fluid">
        <button className="btn btn-danger">coucou boutton</button>
      <header>
        <h1>coucou</h1>
      </header>
    </div>
  );
}


export default App;
````
On import React from rest <br>

On peut importer une feuille de style par: 

``import './App.css``
L'importation de bootstrap nous vient de building your App.<br>
</li>
<li>En React tous les noms de fichier commence par une majuscule sauf les fichiers js classique comme index.css. </li>
</ul>
Le resultat de ce code nous affiche le message coucou et le boutton bootstrap sur notre le localhost.