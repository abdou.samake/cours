# React à quoi sert JSX

On utilise des fichiers avec l'extension JSX pour faire du React pour expliquer au systéme de complilation qu'on fait du React JS avec du **JSX**.<br>
Le **systéme de compliation** est un systéme assez compaleexe qui va prendre des fichiers qu'on peut inventer et qui les compile.

**Babel** est un  systéme de compilation React
<ul>
<li>On se rend sur le site babeljs.io</li>
<li>Dans le site Babel on va dans Setup</li>
<li>on entre dans In the browser</li>
<li>On copie le lien 

````
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script> 
````
</li>
<li>On le colle dans notre fichier HTML sur webstorm</li>
<li>On clique sur babel et on clique sur Download babel et webstorm le télécharge.</li>
</ul>
Quand on insére notre code on voit que ça fonctionne bien, le texte s'affiche sur notre page

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Commencer à utiliser JSX</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const value = 'React';
  const version = React.version;

  const getString = (a, b) => a + b;

  const root = document.getElementById('root');

  const exampleDiv = <div className="main">
    Bienvenue dans l'initiation à {getString('Re', 'act')} (version {version})
  </div>;
  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>

````
On a texte attendu : Bienvenue dans l'initiation à React (version 16.13.1)

On note que Babel transforme le code en **createElement** 
Et on se rendcompte que c'est plus facile de faire du **JSX** plutot que de faire la méthode du **creatElement**
