# Passer des functions en props

On va Manipuler des données qui sont entre plusieurs components.

<ul><li>On voudrais dans App avoir un h1 qui nous affiche  la valeur du boutton cliqué </li>
<li>On va faire une action dans Calc puisque Calc a besoin de Touche alors dans Touche on va faire un onClick</li>
<li>

````html
 function Touche(props) {
      return <button onClick= {props.action}>{props.display}</button>
  }
````
On a ensuite besoin d'atteindre props.display donc on passe en paramétre props.display à props.action. Et pour faire cela correctement  on est obligé de faire une fonction anonyme'()' qui va s'autoexcécuter.
On obtient alors dans Touche 

````html
 function Touche(props) {
      return <button onClick= {() => props.action(props.display)}>{props.display}</button>
  }
````

</li>
<li>On passe ensuite l'action dans le component Calc

````html

  function Calc(props) {
      return liste.map((value, index) => <Touche action= {props.action} display ={value} key= {index}/>)
  }
````
<li>On va passer l'action dans App mais pour cela on doit gérer les etats et créer une fonction handleState

````html

  function App() {
      const [state, setState] = React.useState({display:'null'});

      const handleState = (display) => setState({display});// destructuration de setState({display: display})
````
La fonction handleState prend en paramètre display et qui return le setState de display
</li>
<li>On mettre la fonction handleState dans l'action de App</li>
</li>
<li>
return <>
          <h1>le dernier clique est: {state.display} </h1>
          <Calc action={handleState}/>
      </>
</li>
<li>Et dans h1 on affiche le display de state.

````htm
<h1>le dernier clique est: {state.display} </h1>
`````

</li>
</ul>

On a le code complet:

````html
<script type="text/babel">
  const liste = ['un', 'deux', 2, 'toto', 'tata'];
  function Touche(props) {
      return <button onClick= {() => props.action(props.display)}>{props.display}</button>
  }
  function Calc(props) {
      return liste.map((value, index) => <Touche action= {props.action} display ={value} key= {index}/>)
  }

  function App() {
      const [state, setState] = React.useState({display:'null'});

      const handleState = (display) => setState({display});

      return <>
          <h1>le dernier clique est: {state.display} </h1>
          <Calc action={handleState}/>
      </>
  }

  ReactDOM.render(<App/>, document.getElementById('root'));
</script>
````
Et on le resultat qui affiche la valeur quand on clique sur un élément de la liste des bouttons.