# React passer des classes Css dans un Component

Pour avoir le contenu de notre component en deux background de couleur différent on met le className bg dans la variable **exampleDiv**

````html
<script type="text/babel">
  function Box({...props}) {
      return <div
          className = "container"
          {...props}
          />
  }
  const exampleDiv =
      <>
      <Box className="bg-red big">Mon container</Box>
      <Box className="bg-blue">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
</script>
```` 
On obtient les deux background mais on pert la bordure de la className container car il exécute le className bg qui vient en dernére position.
la meilleur façon de faire c'est de definir un className dans les paramètres de Box et de faire une interpolation de string container.

````html
<script type="text/babel">
  function Box({className, ...props}) {
      return( <div
          className = {`container ${className}`}
          {...props}
          />);
  }
  const exampleDiv =
      <>
      <Box className="bg-red">Mon container</Box>
      <Box className="bg-blue">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root)
</script>
````

 