# Choisir une class grace à une props
<ul>
<li>On deefinit des classe pour changer la taille des box


````html
<style>
    .container {
        border: 2px solid black;
    }

    .bg-blue {
        background-color: #1edcff;
    }

    .bg-red {
        background-color: #ff5454;
    }

    .small {
        width: 50px;
        height: 50px;
    }

    .medium {
        width: 100px;
        height: 100px;
    }

    .large {
        width: 150px;
        height: 150px;
    }

</style>
````
</li>
<li>On peut mettre n'importe lequel dans notre className pour changer sa taille

````html

  function Box({className, ...props}) {
      return( <div
          className = {`container ${className}`}
          {...props}
          />);
  }
  const exampleDiv =
      <>
      <Box className="bg-red large">Mon container</Box>
      <Box className="bg-blue medium">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
</script>
````
On obtient bien le changement de la taille des box.

</li>

<li>On peut maintenant remplacé ces class(large, medium...) par un props <br>
<ul>
<li>On definit le props size dans les paramétres du Box</li>
<li>on indique sa class dans le component</li></ul>


````html
  function Box({size, className, ...props}) {
      return( <div
          className = {`container ${className} ${size}`}
          {...props}
          />);
  }
  const exampleDiv =
      <>
      <Box className="bg-red large" size="large">Mon container</Box>
      <Box className="bg-blue medium" size="medium">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
````

</li>

<li>Si on enleve un size des box on a un UNDEFINED dans le div


````html
  const exampleDiv =
      <>
      <Box className="bg-red">Mon container</Box>
      <Box className="bg-blue medium" size="medium">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
````

````html
<div id="root">
<div class="container bg-red undefined">Mon container</div>
<div class="container bg-blue medium medium">Mon container</div>
</div>
````

</li>
<li>Pour eviter cela on déclare le props size la valeur string vide par defaut dans les parametres du Box

````html

  function Box({size='', className, ...props}) {
      return( <div
          className = {`container ${className} ${size}`}
          {...props}
          />);
  }
  const exampleDiv =
      <>
      <Box className="bg-red">Mon container</Box>
      <Box className="bg-blue medium" size="medium">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
````

````html
<div id="root">
<div class="container bg-red ">Mon container</div>
<div class="container bg-blue medium medium">Mon container</div>
</div>
````
Ainis on a plus de UNDEFINED.

</li>
<li>On peut changer le nom des class sans les definir et les appelé


````html

  function Box({size='', className, ...props}) {
      let sizeClass;
      if(size==='big') {
          sizeClass = 'large';
      }
      if(size==='middle') {
          sizeClass = 'medium';
      }
      if(size==='little') {
          sizeClass = 'small';
      }

      return( <div
          className = {`container ${className} ${sizeClass}`}
          {...props}
          />);
  }
  const exampleDiv =
      <>
      <Box className="bg-red" size="big">Mon container</Box>
      <Box className="bg-blue medium" size="middle">Mon container</Box>
      </>;

  ReactDOM.render(exampleDiv, root);
````
On obtient le change de la taille des box selon la classe qu'on lui indique.

</li>
</ul>

## Prop Style
La propriété **style** est une propriété géré nativement par React.<br>
On definit en faisant une interpolation d'objet dans un component pour lui apporté des changement:

````html

      <Box className="bg-red" size="big"
      style={
          {color: 'white'}
           }
      >Mon container</Box>
````
``{color: 'white'}`` et on fait une interpolation.
Ainsi la couleur du text Mon container devient blanc.<br>
le prop style est connu par le systéme React . Si on essayé de mettre un autre props sous le nom de toto parexmple ça ne marcherait pas , pour que ça marche il faudrait le definir dans les paramétres du component Box.