# React gérer les cliks

Pour gérer les cliks on cré un boutton par la balise **button* et on va claculer le nombre de fois qu'on a cliqué sur le boutton.

````html
<script type="text/babel">
  function App(){
      return (
          <>
              <button>Bouton</button>
          </>
      )
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````
**Onclik** est une fonction React qui permet d'expliquer au templéte qu'on veut cliquer sur le boutton. Onclick fait appel a une fonction et cette fonction on la crée à l'intérieur de **App**.
<ul>
<li>On cré la fonction 

**handleClick**
</li>
<li>On l'appele sur Onclick en faisant une interpolation de la fonction.

``<button onClick={handleClik}>Ajouter1</button>``

A noter qu'on ne met les parenthéses de le fontion quand on fait l'interpolation.
</li>

````html
<script type="text/babel">
  function App(){
      function handleClik() {
          console.log('clik')
      }
      return (
          <>
              <h1>Coucou</h1>
              <button onClick={handleClik}>Ajouter1</button>
          </>
      )
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````
<li>

**handleClick** permet de recuperer l'événement d'un click.
</li>
<li>On ajoute le paramatre event sur la fonction handleClick et on fait console.log de event et on voit les événements qui s'est produit en cliquant sur le boutton.</li>

````html
  function App(){
      function handleClik(event) {
          console.log('click', event);
      }
      return (
          <>
              <h1>Coucou</h1>
              <button onClick={handleClik}>Ajouter1</button>
          </>
      )
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````

</ul> 

**Pou avoir le nombre de Clik**
<ul>
<li>On definit l'objet state avec son attribut account et on fait une incrémentation de state.count


````html
  function App(){
      const state = {count: 0};
      function handleClik() {
          state.count++;
          console.log(state);

      }
      return (
          <>
              <h1>Coucou: {state.count}</h1>
              <button onClick={handleClik}>Ajouter1</button>
          </>
      )
  }
````
En apuyant sur le bouton il nous affiche le nombre de fois qu'on a appuyer sur le boutton

````html
{count: 1} // appuyer 1 fois
{count: 2} // appuyer 2 fois
{count: 3} // appuyer 3 fois
````

</li>
</ul>

En faisant   
``state.count++;`` on modifie l'objet alors qu'en React on a âs le droit de modifier l'objet mais on a le droit de le remplacer en faisant un **assign**

````html

  function SetState(newState) {
      Object.assign(state, newState)
  }
````
Srur la fonction handleState on appelle la fonctios SetState on definissant count par la dreniére valeur de count de state et l'on ajoute 1.

````html
function handleClik() {
          SetState({count: state.count + 1});
          console.log(state);
      }
````
Et on trouve le resultat comme tout à l'heur , on a le nombre a chaque appui.


````html
{count: 1} // appuyer 1 fois
{count: 2} // appuyer 2 fois
{count: 3} // appuyer 3 fois
{count: 3} // appuyer 4 fois
{count: 3} // appuyer 5 fois
{count: 3} // appuyer 6 fois
````

Pour mettre à jour la valeur sur le nombre de cliks indiqué dans le h1, on indique au Dom qu'on a fait une modification en mettant le 
renderApplication() sur la fonction SetState

````html
<script type="text/babel">
  const state = {count: 0};
  function App() {
      function handleClik() {
          SetState({count: state.count + 1});
          console.log(state);
      }
      return (
          <>
              <h1>Nombre de cliks:{state.count}</h1>
              <button onClick={handleClik}>Ajouter1</button>
          </>
      )
  }
  function SetState(newState) {
      Object.assign(state, newState)
      renderApplication();
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````
On obtient les nombres de cliques au niveau du titre.