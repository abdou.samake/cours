# Interpollation

Une **interpollation** c'est quand on fait du calcul à l'intérieur d'un templète.

Un **templète** c'est une Div html écrit en javascript.
``<div className="main">
      Bienvenue dans l'initiation à {getString('Re', 'act')} (version {version})
    </div>;``
    
le ``<div className="main">`` est notre templete.

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Commencer à utiliser JSX</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const value = 'React';
  const version = React.version;

  const getString = (a, b) => a + b;

  const root = document.getElementById('root');

  const exampleDiv = <div className="main">
    Bienvenue dans l'initiation à {getString('Re', 'act')} (version {version})
  </div>;
  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>

````

L'interpollation ici c'est la fontion getString qui est la concaténation de Re et act.
``{getString('Re', 'act')}``
Les interpollations en React c'est juste des crochets {}.