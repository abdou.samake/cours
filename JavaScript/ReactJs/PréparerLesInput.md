# Préparer les Inputs

Les inputs peuvent prendre des valeurs par defaut comme **name** et **age** par exemple.
<ul>
<li>On commence par définir les state par React

````
const [state, setState] = React.useState({name:'', age:null});
````
On initialise name par un string vide et age par la valeur nulle.
</li>
<li> On definit les input name et age</li>

````html
function MyForm() {
    const [state, setState] = React.useState({name:'nom', age:null});
        function handleForm(event) {
        event.preventDefault();
        console.log('coucou')
    }
    return <form onSubmit={handleForm}>
        <label htmlFor="name">Nom</label>
        <input id="name" type="text"/>
        <label htmlFor="age">Age</label>
        <input id="age" type="number"/>
        <button type="button">Annuler</button>
        <button type="submit">Valider</button>
    </form>
}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>

````
<li>On definit dans les inputs name et age les valeurs par defauts respectivements nom et 12</li>

````html

function MyForm() {
    const [state, setState] = React.useState({name:'nom', age: 12});
        function handleForm(event) {
        event.preventDefault();
        console.log('coucou')
    }
    return <form onSubmit={handleForm}>
        <label htmlFor="name">Nom</label>
        <input value={state.name} id="name" type="text"/>
        <label htmlFor="age">Age</label>
        <input value={state.age} id="age" type="number"/>
        <button type="button">Annuler</button>
        <button type="submit">Valider</button>
    </form>
}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>
````
<li>On obtient bien sur le formulaire nom et 12 mais il ya une erreur qui dit qu'on doit ajouter le props onChange pour le changement des states 
</li>
<li>Et pour ne pas mettre onChange sur tout les inputs qu'on fait, on cré une fonction handleInput qui prend en parametre event puis on fait une interpolation de handleInput sur nos input.


````html

function MyForm() {
    const [state, setState] = React.useState({name:'nom', age: 12});
        function handleForm(event) {
        event.preventDefault();
        console.log('coucou')
    }
    function handleInput(event) {

    }
    return <form onSubmit={handleForm}>
        <label htmlFor="name">Nom</label>
        <input
            onChange= {handleInput}
            value={state.name}
            id="name" type="text"/>
        <label htmlFor="age">Age</label>
        <input
            onChange={handleInput}
            value={state.age}
            id="age" type="number"/>
        <button type="button">Annuler</button>
        <button type="submit">Valider</button>
    </form>
}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>
````

</li>
Et la on obtient notre formulaire avec les valeurs par defaut nom et 12 respectivement sur les input name et age sans erreurs.

</ul>