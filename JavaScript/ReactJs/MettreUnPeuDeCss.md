# React mettre un peu de CSS
 Pour gérer les styles avec React on insére sur le même fichier HTML le script 
 ```<style></style>``` et on fait les changements des classes crées entre les balise.
 <ul>
 <li>On cré d'abord notre compinent Box et on cré la className container
 
 ````html

  function Box({...props}) {
      return <div
          className = "container"
          {...props}
          />
  }

  const exampleDiv =
      <Box>Mon container</Box>

  ReactDOM.render(exampleDiv, root);
````
 </li>
 <li> On lla balise 
 
 ```<style></style>``` avant le script 
 
 ```<script type="text/babel"></script>```
 
 ````html

<style>
    .container {
        border: 2px solid black;
    }
</style>
````
On a stylisé notre component Box avec un bordure de 2px visible et de couleur black.

<li>On ajoute un autre className backgroun parexemple (bkg) 

````html
  function Box({...props}) {
      return <div
          className = "container bkg"
          {...props}
          />
  }
  const exampleDiv =
      <Box>Mon container</Box>;
````
Sur le style 

````html
<style>
    .container {
        border: 2px solid black;
    }
    .bkg{
        background: blue;
    }
</style>
````
</li>
 ET on a le contenu de notre composant Box Mon container en background bleu et de bordure 2 px lisible et de couleur noir.
 </li></ul>