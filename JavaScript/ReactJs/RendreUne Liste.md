#  Rendre Une liste

<ul>
<li>On cré le component Touche qui return un boutton qui affiche une information qu'on appelle display qui peut être un nombre.

````html
  function Touche(props) {
      return <button>{props.display}</button>
  }
  function App() {
      return <>
          <Touche display={1}/>
          <Touche display={2}/>
          <Touche display={3}/>
          <Touche display={4}/>
          <Touche display={5}/>
          <Touche display={6}/>
          <Touche display={7}/>
          <Touche display={8}/>
          <Touche display={9}/>
          <Touche display={10}/>
          </>
  }

  ReactDOM.render(<App/>, document.getElementById('root'));
</script>
````
Resultat: on a une liste de boutton de 1 à 10 réparti en ligne.

</li>
<li>Cette methode n'est aimé pas le système du coup on va afficher la liste à l'aide d'un array.

````html
  function Touche(props) {
      return <button>{props.display}</button>
  }
  const liste = [1, "tot", 2, "tata", 5, 7];
  function Calc() {
      return liste.map((value, index) => <Touche display={value} key={index}/>)

  }
  function App() {
      return <>
          <Calc/>
          </>
  }

  ReactDOM.render(<App/>, document.getElementById('root'));
</script>
````
Resultat: on a bien la liste qui s'affiche. 
</li></ul>