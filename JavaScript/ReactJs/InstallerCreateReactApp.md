# React installer Create React App

**Create React** est un outil mis en place par Facebook qui permet d'installer la dernière version de React.<br>
Pour l'installation :
<ul>
<li>

````html
npx create-react-app react-create-app-promo4
````
promo4 est le nom qu'on lui donne.
</li>
<li>Une fois le projet créé sur webstorm
<ul>
<li>Clique droit sur le projet react-create-app-promo4 => git => Add</li>
<li>Dans VCS => Git => push => definir remote</li>
<li>On va sur gitlab on cré un nouveau projet, on copie l'url du projet</li>
<li>On colle sur l'url dans webstorm et fait OK</li>
<li>On trouve tous les fichiers de notre projet qui sont ajouter à git puis on push et le projet et l'ensemble des fichiers sont envoyés sur git.</li>
</ul></li>
<li>Package.json => start</li>
</ul>