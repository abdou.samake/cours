# CRA import component
On ne code plus les components en faisant des classes, on utilise maintenant des fonctions donc on fait des return au lieu des render.

````javascript
import './Header.css'
import React from 'react';

export default function Header() {
     {
        return (<header className = "main">
            <h1>Je suis un Header</h1>
        </header>)
    }
}
````
On reprend notre exercice sur l'affichage des user par leur uid de base de données sur firebase.
<ul><li>On cré des repertoires pour chaque component</li>

## Dans App.js

````javascript
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import Header from './components/Header/Header.js'
import MyForm from "./components/MyForm/MyForm";
import DisplayUser from "./components/DisplayUser/DisplayUser";
function App() {
    const [user, setUser] = React.useState({user: null});
  return (
    <div className="container-fluid main ">
      <Header/>
        <MyForm handleUser={setUser}/>
        <DisplayUser user={user}/>
    </div>
  );
}


export default App;

````

### Dans le component Header.js

````javascript
import './Header.css'
import React from 'react';

export default function Header() {
     {
        return (<header>
            <h1>Je suis un Header</h1>
        </header>)
    }
}
````

### dans le fichier Header.css

````javascript
.main {
    background: blue;
}
````

### Dans le component MyForm.js

````javascript
import React from 'react'
import axios from 'axios'


const api = 'http://localhost:3015/api/hotels/';


export default function MyForm({handleUser}) {
    const [uid, setUid] = React.useState('');

    React.useEffect(() => {
        async function getUserByUid(uid) {
            if (!uid) {
                handleUser({user: null});
                return;
            }
            handleUser('...loading');
            const newUser = await axios.get(api + uid);
            handleUser(newUser);
        }

        getUserByUid(uid);
    }, [uid]);

    function handleInput(event) {
        const {value} = event.target;
        setUid(value);
    }

    return <form>
        <label htmlFor="uid">Uid</label>
        <input
            value={uid}
            onChange={handleInput}
            type="text" id="uid"
        />
    </form>;
}
````

### Dans le component DisplayUser

````javascript

import React from 'react'

export default function DisplayUser({user}) {
    return <div>{JSON.stringify(user)}</div>;
}
````
</ul>
En relançant notre serveur on obtient le formulaire dans un background de couleur bleu qui demande un uid pour afficher un hotel.