# Créer application racine

la variable root qu'on sést créer : 
```` const root = document.getElementById('root');````
on peut ne plus l'utilisé et appliqué directement son contenu sur ReactDOM.render

````html
ReactDOM.render(exampleDiv, document.getElementById('root'));
````
et on a plus de variable root.

Soit **App** est l'application qui va nous servir de racine de toute notre application, c'est notre component de base. On la definit:

````html
function App({className, size='', ...props}) {
    return (
    <>
    <h1>coucou</h1>
    </>
    )
}
````
On peut enléver **ReactDom.render** et on met une fonction à la place qui va prendre en paramétre ``<App/>`` Qui dit à React de prendre App comme racine de toute l'application.

````html

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
    
  }

renderApplication();
````