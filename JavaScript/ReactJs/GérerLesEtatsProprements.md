# Gérer les etats proprements

la fontion SetState 

````html
  function SetState(newState) {
      Object.assign(state, newState);
      renderApplication();
  }
````
est une mauvaise pratique car on ne peut gérer le mise a jour du virtuel DOM 

**state** et *setState** du viebt de React par 
``React.useState``
``React.useState`` nous renvoit dans un tableau 
<ul><li>state qui est l'etat de l'application, les etats des données</li>
<li>et la valeur setState</li>

````html
const [state, SetState] = React.useState({count: 0, lastEvent: '', name:''});
````

</ul>
Pour que ça fonctionne normalement il faudra qu'on fasse au niiveau de la fonction handleInputChange et handleEvent la destructuration des etats de l'application 
c'est à dire la destructuration de state(...state) et on remplace seulement les propriétés count et lastEventType.

````html
<script type="text/babel">
function App() {
    const [state, SetState] = React.useState({count: 0, lastEvent: '', name:''});
    function handleEvent(event) {
        SetState({
            ...state,
            count: state.count + 1,
            lastEvent: event.type});
        console.log(state);
    }
    function handleInputChange(event) {
        SetState({...state, name:event.target.value})
    }

    return (
        <>
            <h1>Bonjour {state.name}</h1>
            <h1>Nombres d'event: {state.count}</h1>
            <h1>Le dernier événement: {state.lastEvent}</h1>
            <div onClick= {handleEvent}
                    onMouseEnter={handleEvent}
                    onMouseLeave={handleEvent}
            >Ajouter 1</div>
            <input type="text" onChange={handleInputChange}/>
        </>
    )
}


  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````

