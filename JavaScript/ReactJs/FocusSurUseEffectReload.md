# React Focus sur UseEffect: reload

Dans 

``React.useEffect(() => {}, [])``
Quand on rempli le tableau de la liste des dépendances ça relance en boucle et pour changer on le fait avec une variable **reload** on a créé  non pas  avec **state**

<ul>
<li>On construit une npouvelle variable reload avec React.useState dans notre App

````html
const [reload, setReload] = React.useState(false);
````

</li>
<li>On cré un boutton qui permet de relancer quand on clique
<ul>
<li>On definit la fonction handleClick qui gére le clique du boutton.<br>
Dans laquelle on relance l'inverse de reload créé dans App

````html
        function handleClick() {
            setReload(!reload)
        }
````

</li>

</ul></li>
<li>On passe notre component List la variable reload en paramétre et dans App 

````html
<ul>
                <List setState={setState} state={state} reload={reload}/>
            </ul>
````

</li>
<li>On ajoute reload dans le tableau qui est la liste des dependances.

``[reload]);``
</li>
</ul> 

On a le code suivant:

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Faire un liste</h1>
<p>Afficher une collection</p>
<p></p>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">

    const api = 'http://localhost:3015/api/hotels/';

    function State({state}) {
        return <pre>{state.message}</pre>;
    }

    function List({setState, reload}) {
        const [hotels, setHotels] = React.useState([]);
        React.useEffect(() => {
            async function getHotels() {
                console.log('USE EFFECT');
                try {
                    const hotelsToGet = await axios.get(api);
                    setHotels(hotelsToGet.data);
                    setState({message: 'ok'});
                } catch (e) {
                    setState({message: 'error'});
                }
            }
            getHotels();
        }, [reload]);

        return hotels.map((hotel, index) => <li key={index}>{hotel.name} {hotel.roomNumbers}</li>);
    }

    function App() {
        const [state, setState] = React.useState({message: 'initializing'});
        const [reload, setReload] = React.useState(false);
        function handleClick() {
            setReload(!reload)
        }
        return <>
            <ul>
                <List setState={setState} state={state} reload={reload}/>
            </ul>
            <State state={state}/>
            <button onClick={handleClick}>reload</button>
        </>;
    }
    ReactDOM.render(<App/>, document.getElementById('root'));
</script>

</body>
</html>
````
Résultat: On peut relancer une seule fois en cliquant sur le boutton sans que cela se relance en boucle.