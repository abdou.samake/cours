# Faire Une liste

On va afficher la liste de la collection d'hotel de notre base de données.<br>
<ul>
<li>On cré le componnent State a afficher l'etat dans lequel on fait l'opération. Et c'est grace à State qu'on a le message "OK".

````html
function State({state, setState}) {
    return <pre>{state.message}</pre>
}
````

</li>
<li>On cré le component List qui sert à afficher la liste de la collection.<br>
Sur liste on fait d'abord un setState des hotels

````html
const [hostels, setHostels] = React.useState([]);
````
Et initialise l'hotel à tableau vide.

````html

<script type="text/babel">

    const api = 'http://localhost:3015/api/hotels';
function Liste({setState}) {
    const [hostels, setHostels] = React.useState([]);
    React.useEffect(() => {
        async function getHostel() {
            try {
                const hotelToGet = await axios.get(api);
                setHostels(hotelToGet.data);
                setState({message: 'ok'})
            } catch (e) {
                setState({message: 'erreur'})
            }
        }
        getHostel()
    }, []);
    return hostels.map((hotel, index) => <li key={index}>{hotel.name} {hotel.roomNumbers}</li>)
}
````
<ul>
<li>Quand on fait un map sur React il faudra qu'on met un key d'où

````html
key={index}
````
dans le return du component List
</li>
<li>On a fait un .data sur hotelToGet pour obtenir l'hotel das la collection.</li>
</ul>
</li>
<li>On fait notre App en faisant d'abord un setStat des states et faire un retour de nos deux components.</li>
</ul>

````html
function State({state, setState}) {
    return <pre>{state.message}</pre>
}
function Liste({setState}) {
    const [hostels, setHostels] = React.useState([]);
    React.useEffect(() => {
        async function getHostel() {
            try {
                const hotelToGet = await axios.get(api);
                setHostels(hotelToGet.data);
                setState({message: 'ok'})
            } catch (e) {
                setState({message: 'erreur'})
            }
        }
        getHostel()
    }, []);
    return hostels.map((hotel, index) => <li key={index}>{hotel.name} {hotel.roomNumbers}</li>)
}
function App() {
    const [state, setState] = React.useState({message:'initializing'});
    return <>
        <ul>
            <Liste state={state} setState={setState}/>
        </ul>
        <State state={state}/>
    </>
}
  ReactDOM.render(<App/>, document.getElementById('root'));
</script>
````
Résultat:
Avec notre api on obtient bien la liste de nos hotels et message ok pour montrer tout c'est bien passé.