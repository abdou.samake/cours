# React router

Le router est une librairie supplémentaire qu'on ajoute à React.
Le router permet de créer des liens qui nous ménent dans des pages.
<ul><li>
On va sur internet on recherche 

**React Router** => Reactraining =>Github => View the docs=> on clique sur web et on a toute les informations nécessaires.
</li>
<li>Pour l'installer

````javascript
npm i --save react-router-dom
````
</li>
<li>On va faire un router de trois pages pour un appli
<ul><li>On cré les trois components chacun dans un fichier a part pour nos pages.</li>
<li>Pour la page1 dans le fichier Page1.js

````javascript
import React from 'react'

export default function Page1() {
    return (
        <h1>Page1</h1>
    )
}
````

</li>
<li>Pour la page2 dans le fichier Page2.js

````javascript
import React from 'react'

export default function Page2() {
    return (
        <h1>Page2</h1>
    )
}
````

</li>
<li>Pour la page3 dans le fichier Page3.js

````javascript
import React from 'react'

export default function Page3() {
    return (
        <h1>Page3</h1>
    )
}
````


</li>
<li>On cré un fichier App.js dans lequel on cré notre application et le router

````javascript
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import {
    BrowserRouter as BrowserRouter,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Page1 from "./components/Page1";
import Page2 from "./components/Page2";
import Page3 from "./components/Page3";
function App() {
    return (
        <BrowserRouter>
            <div>
                <ul>
                    <li>
                        <Link to="/">Page1</Link>
                    </li>
                    <li>
                        <Link to="/Page2">Page2</Link>
                    </li>
                    <li>
                        <Link to="/Page3">Page3</Link>
                    </li>
                </ul>
                <Switch>
                    <Route exact path="/">
                        <Page1/>
                    </Route>
                    <Route path="/Page2">
                        <Page2/>
                    </Route>
                    <Route path="/Page3">
                        <Page3/>
                    </Route>
                </Switch>
            </div>

        </BrowserRouter>
    )
}


export default App;
````

</li>
</ul>
</li></ul>


``<BrowserRouter>`` C'est le main c'est lui qui gére les routes.

Resultat on a un liste de trois routes qui nous chacun dans une page

<ul>
<li>Page1</li>
<li>Page2</li>
<li>Page3</li>
</ul>
La page1 est la racine c'est la page principale. C'est pour ça que sur son link on met 

```"/"```