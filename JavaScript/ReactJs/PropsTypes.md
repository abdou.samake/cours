# Specifier les types des Props

**prop-types** est une librairie qui permet de specifier les types des props d'un component.

<ul>
<li>On prend notre component Hello on lui applique l'attibut
 
**propTypes**
 à qui dedans on va remettre les propriétés du component et on utilise l'outil proposé par propTypes et on déclare ce qu'on attend des propriétés.
 
 On a les propTypes sur github.com/facebook/prop-type
 
 ````html
<script src="https://unpkg.com/prop-types@15.6/prop-types.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');
  function Hello({firstName, lastName, age, car}) {
    return (
        <div>
          <div>age : {typeof age}</div>
          <div>car : {typeof car}</div>
          Bonjour je m'appelle {firstName} {lastName} et j'ai {age} an(s)!
        </div>
    );
  }

  Hello.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    car: PropTypes.bool,
  };

  const exampleDiv = <Hello
      lastName='Bianchi'
      age={17}
      car={true}
  />;


  ReactDOM.render(exampleDiv, root);
</script>
````


````html
  Hello.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    car: PropTypes.bool,
  };
````
 firstName est type: string<br>
 lastName est de type string <br>
 age est de type number <b
 car est de type boolean <br>
 
 **isRequired** c'est pour dire que la propriété est indispensable pour le bon fonctionnement du composant.
 
</li></ul>