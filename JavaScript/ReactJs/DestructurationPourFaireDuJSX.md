# React les props + astuce destructuration

Comment on fait la destructuration pour faire du JSX.

<ul>
<li>On cré un objet props d'attributs children et className 


````html
  const className = 'main';
  const children = 'Coucou de White Rabbit';
const props = {className, children} 

````
</li>
<li>Si dans exampleDiv on ajoute dans le div la destructuration de props

````html
const exampleDiv = <div {...props} />;
````
On va obtenir le resultat **coucou de White Rabbit** qui s'affiche.
</li>
</ul>
On a ainsi une façon plus faire de faire du JSX.

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Les props et astuce de destructuration</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');

  const className = 'main';
  const toto = 'Coucou de White Rabbit';

  const props = {className, children};
  const exampleDiv = <div {...props} />;

  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>
````
Parcontre si on change le nom **children** par **toto** parexemple le message ne s'affiche plus car JavaScript connait le nom children specifiquement.

Ainsi on peut mettre plusieurs propriétés dans l'objet **props** et en faisant la destructuration  seule la propriété de nom children va s'afficher, les autres proprités Javascript les met dans un div avec les valeurs qu'on les a associé.

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Les props et astuce de destructuration</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');

  const className = 'main';
  const toto = 'Coucou de White Rabbit';

  const props = {toto, className, children: 'salut', type: 'button'};
  const exampleDiv = <div {...props} />;

  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>

````
Resultat: on a le message : **salut** <br>

Et quand on inspecte :

````html
<div id="root">
<div toto="Coucou de White Rabbit" class="main" type="button">salut</div>
</div>
````