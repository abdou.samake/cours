# React Router Params

On souhaite récupérer les paramétres qui sont dans les routes.
<ul><li>On fait l'exemple d'un id</li>
<li>On importe d'abord useParams dans le component de la page2 qu'on souhaite récupérer l'id et un autre paramétre nommé p2.

````javascript
import React from 'react'
import {useParams} from 'react-router-dom'
export default function Page2() {
    let {id, p2} = useParams();
    return (
        <h1>Page2: {id} {p2}</h1>
    );
}
````
On definit les parametres id et p2 puis faire la destructuration pour récupérer la valeur dans useParams.
</li>
<li>Dans App on renseigne sur le path de la page2 les params id et p2.

````javascript
 <li>
                    <Link to="/Page2/:id/:p2">Page2</Link>
     </li>
````
On le fait aussi dans le swith

````javascript
<Route path="/Page2/:id/:p2">
                        <Page2/>
````
</li>

</ul>
On a le code dans App:

````javascript
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import {
    BrowserRouter as BrowserRouter,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Page1 from "./components/Page1";
import Page2 from "./components/Page2";
import Page3 from "./components/Page3";
function App() {
    return (
        <BrowserRouter>
            <div>
                <ul>
                    <li>
                        <Link to="/">Page1</Link>
                    </li>
                    <li>
                        <Link to="/Page2/:id/:p2">Page2</Link>
                    </li>
                    <li>
                        <Link to="/Page3">Page3</Link>
                    </li>
                </ul>
                <Switch>
                    <Route exact path="/">
                        <Page1/>
                    </Route>
                    <Route path="/Page2/:id/:p2">
                        <Page2/>
                    </Route>
                    <Route path="/Page3">
                        <Page3/>
                    </Route>
                </Switch>
            </div>

        </BrowserRouter>
    )
}


export default App;
````
Et en renseignant les valeurs de id par coucou et p2 par tata dans l'url 

````javascript
http://localhost:3000/Page2/coucou/tata
````

on a le resultat sur la page2<br>
# Page2: coucou tata 