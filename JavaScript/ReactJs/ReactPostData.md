# React correction post

On va envoyer un hotel dans notre base de données sur firbase en remplissant le nom et le nombre de chambre dans un formulaire React.

<ul>
<li>On le component MyForm on definit les variables name, roomNumbers et la variable sending pour l'envoie de l'objet, cette variable est un boolean

````html
    const [name, setName] = React.useState('');
    const [roomNumbers, setRoomNumbers] = React.useState('');
    const [sedding, setSedding] = React.useState(false);
````

</li>
<li>Dans le React.useEffect on definit la fonction async postHostel({name, roomNumbers}) qui prend en paramétre les props name et roomNumbers destructurés

````html
        async function postHostel({name, roomNumbers}) {
            if(!sedding) {
                return;
            }
            setMessage('post in progress');
            try {
                const result = await axios.post(api, {name, roomNumbers});
                setMessage(`la chambre créée a l'id: ${result.data.id}`)
            } catch (e) {
                setMessage(e.data.message);
            }
            setSedding(false)
        }
        postHostel({name, roomNumbers})
    }, [sedding]);
````
on fait un axios.post sur l'api de l'objet en donnant son name et roomNumbers.<br>
<br>
On fait un setMessage qui le affiche l'id de l'hotel envoyer une fois qu'appuie sur le boutton.<br>
<br>
On fait un setSedding(false) ensuite.<br>
Puis on retourne la fonction async postHostel<br>
<br>
Dans le tableau de la liste des dependances on le sedding puisque c'est ça qui change(false or true) 
</li>
<li>Dans notre formulaire on crée:
<ul>
<li>La fonction handleSubmit pour gérer la soumission

````html
    function handleSubmit(event) {
        event.preventDefault();
        if(!name || !roomNumbers) {
            setMessage('ERROR missing data');
            setSedding(false);
            return;
        }
        setSedding(true)
    }
On envoir d'abord le message d'erreur si appuie sur valider alors les imputs name et roomNumbers sont vides et setSedding(false) qui n'envoie rien.<br>
Si c'est pas le cas on fait setSedding(true) qui envoie le data.
`````



</li>
<li>La fonction handleName qui gére la valeur indiqué dans l'input name.

````html
    function handleName(event) {
        const {value} = event.target;
        setName(value)
    }
````
On fait un setName qui change la nouvelle valeur.
</li>
<li>
La fonction handleRoomNumbers qui gére le roomNumbers

````html
    function handleRoomNumbers(event) {
        const {value} = event.target;
        setRoomNumbers(value)

    }
````
On fait aussi un setRoomNumbers(value) qui change la nouvelle valeur renseignée.
</li>
</ul></li>
<li>On retourne notre notre formulaire

````html
    return <form onSubmit={handleSubmit}>
        <label htmlFor="name">Name</label>
        <input
                value={name}
                onChange={handleName}
                type="text" id="name"
        />
        <label htmlFor="roomNumber">RoomNumbers</label>
        <input
            value={roomNumbers}
            onChange={handleRoomNumbers}
            type="number" id="roomNumber"
        />
        <button type="submit" >Valider</button>

    </form>
````

</li>
<li> On definit le component MyMessage

````html

function MyMessage({message}) {
    return <pre>{message}</pre>
}
````
Pour message d'envoi.
</li>
<li>A la fin on definit le component principal App

````html

function App() {
    const [message, setMessage] = React.useState('');
    return <>
        <MyForm setMessage={setMessage}/>
        <MyMessage message={message}/>
        </>

}
````

</li>
</ul>
On a le code complét

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Faire un POST</h1>
<p>Faire un formulaire qui prend en paramètre un objet et qui le post en BDD</p>
<p></p>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">

    const api = 'http://localhost:3015/api/hotels/';
function MyForm({setMessage}) {
    const [name, setName] = React.useState('');
    const [roomNumbers, setRoomNumbers] = React.useState('');
    const [sedding, setSedding] = React.useState(false);

    React.useEffect(() => {

        async function postHostel({name, roomNumbers}) {
            if(!sedding) {
                return;
            }
            setMessage('post in progress');
            try {
                const result = await axios.post(api, {name, roomNumbers});
                setMessage(`la chambre créée a l'id: ${result.data.id}`)
            } catch (e) {
                setMessage(e.data.message);
            }
            setSedding(false)
        }
        postHostel({name, roomNumbers})
    }, [sedding]);

    function handleSubmit(event) {
        event.preventDefault();
        if(!name || !roomNumbers) {
            setMessage('ERROR missing data');
            setSedding(false);
            return;
        }
        setSedding(true)
    }

    function handleName(event) {
        const {value} = event.target;
        setName(value)
    }

    function handleRoomNumbers(event) {
        const {value} = event.target;
        setRoomNumbers(value)

    }

    return <form onSubmit={handleSubmit}>
        <label htmlFor="name">Name</label>
        <input
                value={name}
                onChange={handleName}
                type="text" id="name"
        />
        <label htmlFor="roomNumber">RoomNumbers</label>
        <input
            value={roomNumbers}
            onChange={handleRoomNumbers}
            type="number" id="roomNumber"
        />
        <button type="submit" >Valider</button>

    </form>

}

function MyMessage({message}) {
    return <pre>{message}</pre>
}

function App() {
    const [message, setMessage] = React.useState('');
    return <>
        <MyForm setMessage={setMessage}/>
        <MyMessage message={message}/>
        </>

}
  ReactDOM.render(<App/>, document.getElementById('root'));
</script>

</body>
</html>
````
### Résultat : <br>
Si on dans le formulaire un nom d'hotel et un nombre de roomNumbers puis on valide, on obtient l'id de notre hotel qu'on va retrouver dans la base de données sur firebase.