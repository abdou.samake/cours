# Créer ses composants

Le but est de créer un composant qui soit utilisable:<br>
On choisit d'abord un composant **Div** parexemple  à qui on met du texte dans le parametre title. Pour cela on fait une interpolation.
<ul>
<li>On cré une fonction 

**getTitle** qui renvoit du JSX c'est à dire un div qui prend en paramétre className, title et number

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Créer ses composants</h1>
<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');

function getTitle(className, title, number) {
    return <div className={className}>{`Titre n°${number}: ${title}`}</div>
// on a fait l'interpolation d'une interpolation de string.
  }

  const exampleDiv =
      <div>
          {getTitle('main', 'coucou', 1)}
          {getTitle('main', 'coucou2', 2)}
      </div>;
````
</li>

On a le resultat: 
Titre n°1: coucou<br>
Titre n°2: coucou2

A noter en JSX à l'interieur du Div le className est égale l'interpolation du className non pas un string.
</ul>
Pour créer son propre composant au cas où on vaudrais l'utiliser plusieurs fois dans son appli, on cré un élément qui représente une fonction.<br>

A noter que en React un élément qui represente une fonction commence toujours par une majuscule.<br>
<ul>
<li>On cré le fonction MyTitle qui représente notre composant et qui prend en parametre des 

**props**, on lui passera les paramétres 
<ul><li>children</li>
<li>className</li>
</ul>
Imposés par le systéme.
</li>

````html
 function MyTitle({className, children, number}) {
    const props = {
      children: `Titre n°${number}: ${children}`,
      className
    };
    return <div {...props} />

  }
````
````html
  const exampleDiv =
      <div>
        {getTitle('main', 'coucou', 1)}
        <MyTitle className="seconder" number="2">coucou</MyTitle>
      </div>;
````
<ul>
<li>className: donne la classe du div seconder</li>
<li>children: affiche le text et le titre coucou</li>
<li>number: donne le numéro 1</li>
</ul>
</ul>
On le resultat: <br>
Titre n°1: coucou / correspond au resultat par la fonction getTitle<br>
Titre n°2: coucou / correspond au resultat par le composant <MyTitle></MyTitle> créé.