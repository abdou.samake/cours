# Gérer les evenements des cliks

<ul>
<li>On modifie notre fonction handleClik en handleEvent pour gérer les événements des cliques.</li>
<li>On ajoute l'attribut lastEventType qui est le dernier type événement de clique on l'initialise à un string vide. 
 
 ````const state = {count: 0, lastEventType:''};````
 
 </li>
<li>Dans la fonction handleEvents on ajoute à l'attribut lastEventType le type de event qui est l'événement du clique 

````html

      function handleEvent(event) {
          SetState({count: state.count + 1, lastEventType: event.type});

          console.log(state);
      }
````
</li>

<li>On ajoute dans le Templète(notre Div) un h1 en faisant l'interpolation de la valeur de l'attribut lastEventType de l'objet state </li>
<li>Dans le component boutton on choisit un événement de React (onMouseEnter parexemple)

````html

      return (
          <>
              <h1>Nombre de cliks:{state.count}</h1>
              <h1>l'évenement du clik:{state.lastEventType}</h1>
              <button
                  onClick={handleEvent}
                  onMouseEnter={handleEvent}
                  onMouseLeave={handleEvent}

              >Ajouter1</button>
          </>
      )
````
</li>
</ul>
Soit le code complét :

````html
  const state = {count: 0, lastEventType:''};
  function App() {
      function handleEvent(event) {
          SetState({count: state.count + 1, lastEventType: event.type});

          console.log(state);
      }
      return (
          <>
              <h1>Nombre de cliks:{state.count}</h1>
              <h1>l'évenement du clik:{state.lastEventType}</h1>
              <button
                  onClick={handleEvent}
                  onMouseEnter={handleEvent}
                  onMouseLeave={handleEvent}

              >Ajouter1</button>
          </>
      )
  }
  function SetState(newState) {
      Object.assign(state, newState);
      renderApplication();
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }

  renderApplication();

</script>
````
On obtient un type d'événement 

Gérer les clics <br>
Nombre de cliks:15<br>
l'évenement du clik:mouseleave

On clique plusieurs fois il ne se passe rien mais si on arrête et on pose la souris le type s'affiche.
<li>Gérer les événements pour les Div d'une manière générale c'est la même chose que por les bouttons on remplace juste button par div


````html
<div
                  onClick={handleEvent}
                  onMouseEnter={handleEvent}
                  onMouseLeave={handleEvent}

              >Ajoute 1
              </div>
````

</li>