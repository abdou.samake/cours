# Node activer Cors Pour React
Pour pouvoir utiliser son propre api avec React il faudrait activer cors son projet webstorn.<br>
Pour installer cors:
<ul>
<li>Dans  terminal on se positionne sur server, on met

```npm i -s cors```
On obtient cors dans package.json avec sa derniére version

</li>
<li>Ensuite on met

```npm i -D @types/cors```

qui va installer la compatibilité entre cors codé en node et cors codé en typescript.

## Pour activer
Dans notre fichier server de typescipt on met 

``app.use(cors())``
Puis on import cors 
``ìmport cors from cors``

</li>
<li>Et dans notre fichier React remplace notre api par l'url qui donne notre base de données.

```html
const api = 'http://localhost:3015/api/hotels/';
```

</li>
</ul>