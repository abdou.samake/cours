# React input value event

Gerer les inputs
<ul>
<li>On definit un input dans le templéte

````html
     return (
          <>
              <h1>Bonjour:{state.name}</h1>
              <h1>Nombre de cliks:{state.count}</h1>
              <h1>l'évenement du click:{state.lastEventType}</h1>
              <div
                  onClick={handleEvent}
                  onMouseEnter={handleEvent}
                  onMouseLeave={handleEvent}

              >Ajoute 1
              </div>
              <input type="text" onChange={inputChangeEvent} />
          </>
      )
````

</li>
<li>On definit la fonction inputChangeEvent

````html
  function inputChangeEvent(event) {
      console.log(event.target.value);
      SetState({name: event.target.value})
  }
````
On récupére la valeur du input en faisant event.target.value
On a jouté l'objet SetState pour obtenir le input(ce qu'on écrit) sous l'attribut name
</li>
<li>Maintenant dans notre templéte on peut ajouter un h1 qui récupére directement la valeur du input.

````html
<h1>Bonjour:{state.name}</h1>
````
</li>
</ul>

Le code complét:

````html
<script type="text/babel">
  const state = {count: 0, lastEventType:''};
  function App() {
      function handleEvent(event) {
          SetState({count: state.count + 1, lastEventType: event.type});

          console.log(state);
      }
      return (
          <>
              <h1>Bonjour:{state.name}</h1>
              <h1>Nombre de cliks:{state.count}</h1>
              <h1>l'évenement du click:{state.lastEventType}</h1>
              <div
                  onClick={handleEvent}
                  onMouseEnter={handleEvent}
                  onMouseLeave={handleEvent}

              >Ajoute 1
              </div>
              <input type="text" onChange={inputChangeEvent} />
          </>
      )
  }
  function inputChangeEvent(event) {
      console.log(event.target.value);
      SetState({name: event.target.value})
  }

  function SetState(newState) {
      Object.assign(state, newState);
      renderApplication();
  }

  function renderApplication() {
    ReactDOM.render(<App/>, document.getElementById('root'));
  }
  renderApplication();


</script>
````
Resultat:
Quand on renseigne un nom sur input on le récupére directement sur le Bonjour,<br>

Gérer les clics<br>
Bonjour:coucocu<br>
Nombre de cliks:10<br>
l'évenement du click :mouseleave