# Les formlaires

Les formulaires sont des demandesqui permettent de déclancher une action.
<ul>
<li>On fait des input</li>
<li>Pour une bonne pratique pour chaque input on fait une label</li>
<li>Pour les label on utilise htmlFor et on identifie par id dans le input</li>
<li>On cré deux bouttons:
<ul>
<li>le boutton annuler de type button</li>
<li>le boutton valider de type submit</li></ul>
</li>

````html
<script type="text/babel">

function MyForm() {
    function handleForm() {
        console.log('coucou')
    }
    return <form onSubmit={handleForm}>
        <label htmlFor="name">Nom</label>
        <input id="name" type="text"/>
        <button type="button">Annuler</button>
        <button type="submit">Valider</button>
    </form>
}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));

function MyForm() {
    function handleForm() {
        console.log('coucou')
    }
    return <form onSubmit={handleForm}>
        <label htmlFor="name">Nom</label>
        <input id="name" type="text"/>
        <button type="button">Annuler</button>
        <button type="submit">Valider</button>
    </form>
}
  ReactDOM.render(<MyForm/>, document.getElementById('root'));
</script>
````
<li>On obtient le formulaire mais quand on clique sur le boutton de soumission valider, il recharge la page, c'est un comportement normal de HTML et pour empêcher ça on prend le paramêtre event de la fonction handlefor et on lui applique 

**preventDefault** qui va arrêter le comportement par defaut et ainsi la page ne va plus se recharger quand on appuie sur valider.</li>


````html

````


</ul>