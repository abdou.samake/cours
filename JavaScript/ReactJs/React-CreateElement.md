# React CreateElement

React est une librairie qui sert à gérer le virtuel Doom.
Dans un fichier html on met le script qui fait l'import du React.
````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cours React</title>
</head>
<body>
<a href="./">Retour</a>
<h1>Créer un élément avec React</h1>

<div id="root"></div>

<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>

<script>
  const value = 'React';
  const version = React.version;

  const root = document.getElementById('root');

  const exampleDiv = React.createElement('div', {
    className: 'main',
    children: `Bienvenue dans l'initiation à ${value} (version ${version})`,
  });

  ReactDOM.render(exampleDiv, root);
</script>

</body>
</html>

````
<ul>
<li>script pour pour React:</li>

    ``<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>``
    
<li>

``const value = 'React';``
<li>

``const version = React.version;`` : permet de determiner la version de React. Actuellement on est à la verrsion 16.

</li>

<li>

``<div id="root"></div>`` est l'element du Doom.
</li>
<li>
Maintenant pour chercher l'élément du Doom on effectue la ligne de code: 

````html
 const root = document.getElementById('root');
````
<ul>
<li>document: pour aller dans la page actuelle </li>
<li>getElementById('root'): pour aller chercher un élément du Doom par son Id et l'élémnet du Doom qui a un Id qui s'appelle root</li>
</ul>
Ce processus consiste à prendre le Doom et le stocker dans la constante  

**root** 
</li>

<li>
On crée maintenant une nouvelle constante pour créer un élément du Doom par 

**createElement**

````html
  const exampleDiv = React.createElement('div', {
    className: 'main',
    children: `Bienvenue dans l'initiation à ${value} (version ${version})`,
  });

  ReactDOM.render(exampleDiv, root);
````

<ul>
<li>'div': on cré un element du type Div, ça pouvait être un autre élément à la place(h1, p ...)</li>
<li>children: ça veut dire ce qui est à l'intérieur de cette div pon va le mettre dans children, on va chercher value qui React et la version qui est la version de React.</li>
</ul>
<li>

``ReactDOM.render(exampleDiv, root);`` c'est la deuxiéme partie du React qu'on utilise à partir du script: 
````html
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd
````
On le fait en deux partie car il ya des gens qui n'ont pas besoin de Doom.
ReactDOM: on va prendre le Doom , on fait le **render** c'est à dire l'affichage et on prend le **DoomVirtuel** on y ajoute **exampleDiv** et **root**
on fait un rendering pour que ça s'affiche. Et on a le resultat de l'élément qu'on a créer.
</li>
</ul>
A l'intérieur du root on a créer un enfant de class main de contenu

**Bienvenu dans l'initiation à React (version 16.3.1)**

<ul>
<li>
Si on desactive React en mettant le code en commentaire:

````html
/*const root = document.getElementById('root');

  const exampleDiv = React.createElement('div', {
    className: 'main',
    children: ` ${version})`,
  });*/
````
On a le message **Bienvenu dans l'initiation à React (version 16.3.1)** qui ne s'aafiche plus.
</li>

<li>Si on reactive le Doom en enlevant les commentaires 

Si on inspecte notre page on se rend compte que dans le div root un autre div de class main et de contenu Bienvenue dans l'initiation à React (version 16.13.1)
avec la version du React qui nous vient de React.version

````html
<div id="root">
<div class="main">Bienvenue dans l'initiation à React (version 16.13.1)
</div>
</div>
````


</li>




