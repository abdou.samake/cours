# React Passer les bons Types

On a un composant Hello qui prend en paramétre les props firstName, lastName, age, car et qui affiche une phrase simple.

````html
  function Hello({firstName, lastName, age}) {
      return <div>
          hello je m'appele {firstName} {lastName} et j'ai {age} an(s)!
      </div>
  }
  const exampleDiv = <Hello
          firstName="abdou"
          lastName="samake"
          age="{28}">
  </Hello>;



  ReactDOM.render(exampleDiv, root);
````
Resultat:<br>

hello je m'appele abdou samake et j'ai {28} an(s)!

A noter que quand on veut afficher des nmbres en React on fait une  interpolation de même que pour les boolean.
C'est pourquoi ici on a <br>

``age="{28}"``

Il faut préciser les types de propriétés qu'on souhaite avoir.
<ul>
<li>Si on fait une interpolation de age et de car puis retouner leur type:

````html
<script type="text/babel">
  const root = document.getElementById('root');
  function Hello({firstName, lastName, age, car}) {
      return <div>
          <div>{typeof age}</div>
          <div>{typeof car}</div>
          hello je m'appele {firstName} {lastName} et j'ai {age} an(s)!
      </div>
  }
  const exampleDiv = <Hello
          firstName="abdou"
          lastName="samake"
          age={28}
          car={true}
          />;



  ReactDOM.render(exampleDiv, root);
</script>
````
resultat: <br>
number<br>
boolean<br>
hello je m'appele abdou samake et j'ai 28 an(s)!<br>
On a bien age de type number et car de type boolean.
</li>
<li>Parcontre si on ne fait d'interpolation :

````html
<script type="text/babel">
  const root = document.getElementById('root');
  function Hello({firstName, lastName, age, car}) {
      return <div>
          <div>{typeof age}</div>
          <div>{typeof car}</div>
          hello je m'appele {firstName} {lastName} et j'ai {age} an(s)!
      </div>
  }
  const exampleDiv = <Hello
          firstName="abdou"
          lastName="samake"
          age="28"
          car="true"
          />;



  ReactDOM.render(exampleDiv, root);
</script>
````
Resultat:<br>
string<br>
string<br>
hello je m'appele abdou samake et j'ai 28 an(s)!<br>

On voit que number et car sont de type string.
</li></ul>