# Behavior Subject

On fait le cas ou le component app n'est pas en relation direct avec un autre component.
<ul>
<li>On cré un nouveau service

```ng g s service/mood```

</li>
<li>Dans mood.service.ts on cré un Behavior subject c'est à dire un sujet de comportement.<br>
Quand on le cré on met toujours un dollar a la fin du nom. On le met de type BehaviorSubject<string> et on le declare, on l'initialise avec null c'est à dire vide.


````angularjs
handleMood$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
````
</li>
<li>On cré une fonction  SetMood() permettant de changer le mood de la personne en cliquant sur le boutton, elle prend en paramétre message


````angularjs
 setMood(message) {
    this.handleMood$.next(message);
  }
````
**next** c'est pour prendre la prochaine valeur de la personne.<br>
Cette fonction permet de récupérer la nouvelle valeur .



````angularjs
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoodService {
  handleMood$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor() { }
  setMood(message) {
    this.handleMood$.next(message);
  }
}

````
</li>
<li>Dans mood.component.ts on injecte dans constructor le moodService
 
 ````angularjs
  constructor(
    private moodService: MoodService
  ) { }
````
 </li>
 <li>Et dans nos methodes on fait un setMood des messages.
 
 
 ````angularjs

  Iok() {
    this.moodService.setMood('je suis ok');
  }
  NotOk() {
    this.moodService.setMood('je ne suis pas ok');
  }
  Neutre() {
    this.moodService.setMood('je suis neutre');
  }
````


````angularjs
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MoodService} from "../../service/mood.service";

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {
  constructor(
    private moodService: MoodService
  ) { }

  ngOnInit(): void {
  }
  Iok() {
    this.moodService.setMood('je suis ok');
  }
  NotOk() {
    this.moodService.setMood('je ne suis pas ok');
  }
  Neutre() {
    this.moodService.setMood('je suis neutre');
  }

}

````
Ainsi à chaque fois qu'on clique sur un boutton on aura l'évenement correspondant qui est un message ici.
 </li>
 <li>On injecte le moodService dans app.component.ts
 
 
 ````angularjs
private moodService: MoodService
````
On ajoute la fonction **ngOnInit** pour cela on fait un implements OnInit sur la class AppComponent.<br>
<br>
On cherche dans le ngOnInit moodService, handleMood et on s'abonne à BehaviorSubject grace à Subscribe() en récupérant le message de type string qui va s'afficher.



````angularjs
import {Component, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from "./service/mood.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'promo4Angular';
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService
  ) {
  }
  ngOnInit(): void {
    this.moodService.handleMood$
      .subscribe(message: string) => this.message = message);
  }
}

````

C'est pas trop propre de récupérer le message dans le subscibe . On peut faire un pipe à l'intérieur du quel on utilise un tap.<br>
Un tap est une opération dans le pipe qui permet de faire des petits actions.



````angularjs
import {Component, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from "./service/mood.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'promo4Angular';
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService
  ) {
  }
  ngOnInit(): void {
    this.moodService.handleMood$
      .pipe(
        tap((message: string) => this.message = message)
      )
      .subscribe();
  }
}

````
 </li>
</ul>
On a ainsi récupéré l'événement dans le component principal app.component.ts et on a les messages correspondants à chaque clique qui s'affichent sur la page.