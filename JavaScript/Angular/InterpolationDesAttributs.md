# Interpolation des attributs

Pour faire une interpolation on doit mettre des crochets au nom de l'attribut et lui donné la valeur de la variable qu'on a créé dans la class Menu1Component.
<ul><li>On cré deux variable link1 et link2 qui vont prendre la valeur des chemin home et à-propos

````angularjs
export class Menu1Component implements OnInit {
  name = '';
  link1 = 'home';
  link2 = 'à-propos';

````

</li>
<li>Dans menu1.components.html on fait l'interpolation des attributs routerLink en mettant des crochets

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}
  <li>
    <a [routerLink]= "link1">home</a>
  </li>
  <li>
    <a [routerLink]= "link2">about</a>
  </li>
</ul>
<p>menu1 works!</p>

````
On explique a Angular en mettant des crochets que l'attribut routerLink ne va prendre de string mais on va interpoler la valeur des variables link1 et link2.
</li></ul>
On a ainsi nos deux router home et à-propos dans la page menu1component.html. 