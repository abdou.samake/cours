# NG IF ET NG CONTAINER

## ngIf
le **ngIf** est une directive qui permet de faire du if.
<ul>
<li>On fait un exemple qui consiste à afficher un message pour des questions dont l'age est supérieur à 32 ans par exemple.
 <ul>
 <li>On definit la variable age dans la class dans menu1.component.ts
 
 ````angularjs
export class Menu1Component implements OnInit {
  name = '';
  age = 40;
````
 
 </li>
 <li>On fait le ngIf sue la balise li
 
 ````angularjs
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}

  <h2 *ngIf = "age > 32">Info pour les vieux</h2>

  <li *ngFor="let current of allRoutes">
    <a  [routerLink] = "current.url">{{current.name}}</a>
  </li>
</ul>
````
On a bien le message Info pour les vieux car l'age qu'on a definit est supérieur à 32.
 
 
 </li></ul></li>
 <li>On souhaite affiché un lien sur menu1 que lorsque age est supérieur à un nombre minimum qu'on va définir.
 <ul>définit une variable supplémentaire nommé minumAge pour les deux objets dans le tableau allRoutes.
 
 ````angularjs
import { Component, OnInit } from '@angular/core';
import {MenuModel} from '../../../model/menu.model';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';
  age = 40;
  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 30}
    ];

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }

}


````
<li>On ajoute le type de minumAge dans notre interface

````angularjs
export interface MenuModel {
  url: string;
  name: string;
  minumAge: number;
}

````

</li>
<li>Ensuite on fait le *ngIf sur la balise a 


````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}


  <li *ngFor="let current of allRoutes">
    <a  *ngIf = "age > current.minumAge" [routerLink] = "current.url">{{current.name}}</a>
  </li>
</ul>
<p>menu1 works!</p>
````

Les deux liens s'affichent car age est supérieur à chacun de leur minumAge.
</li>

 </ul></li>
 <li>
 
 Maintenant si on met nos deux directives *gIf et *ngFor cote à cote on a une erreur car on ne peut pas mettre deux directives structurelles sur la même ligne en Angular. l'étoile * montre que le ng est une directive structurelle.
 </li>
 <li>Pour que ça marche on utilise la balise ng-container</li>
 
</ul>

## NG Container
<ul>
<li>On met la directive ngFor dans ng-container et le ngIf on le met dans la balise li. Puis on met tout le contenu de la balise li dans la balise ng-container pour current puisse exister.

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}

  <ng-container *ngFor = "let current of allRoutes">
      <li *ngIf="age>current.minumAge">
      <a [routerLink]= "current.url">{{current.name}}</a>
      </li>
  </ng-container>

</ul>
<p>menu1 works!</p>


```` 
On a seulement le lien accueil qui s'affiche car on a mis à age la valeur 10.
</li>
<li>On peut aussi faire des ng-container dans des ng-container dans lequel on pourra mettre le ngIf.


````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}

  <ng-container *ngFor = "let current of allRoutes">
    <ng-container *ngIf="age>current.minumAge">

      <li>
        <a [routerLink]= "current.url">{{current.name}}</a>
      </li>
    </ng-container>
  </ng-container>

</ul>
<p>menu1 works!</p>

````

On a le même resultat. L'avantage c'est qu'on peut mettre chaque directive structurelle sur une balise ng-container

</li>
</ul>