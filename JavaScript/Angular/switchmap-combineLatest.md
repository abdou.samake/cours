# Swithmap

<ul>
<li>

On cré deux bouttons **get hotel** et **get room** qui permettent de geter un hotel ou un room en appuyant sur le boutton.

Dans hotel.service.ts



````angularjs
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>);



  getRoomById$(roomId: string): Observable<RoomModel> {
    return (this.httpClient.get(this.root + 'rooms/' + roomId) as Observable<RoomModel>);
  }
````

Dans App.component.ts


````angularjs
  getHotel() {
    this.hotelService.getHotelById$('IWPMFMCLHsnLlBxqEvdF')
      .pipe(
        tap(x => console.log(x))
      )
      .subscribe();
  }
getRoom() {
    this.hotelService.getRoomById$('TTchhMxW79EeUJitgVf3')
      .pipe(
        tap(x => console.log(x))
      )
      .subscribe();
}
````
On a fait des pipe avec tap pour afficher l'hotel et le room dans le console.<br>
<br>
Dans app.component.html


````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>

<button (click)="getHotel()">get hotel</button>
<button (click)="getRoom()">get room</button>
<router-outlet></router-outlet>


````
</li>
<li>

On voudrais geter les rooms conteanu dans un hotel grace à son id. Avec angular on a pas accés à **axios** comme React, on utilise **switchmap**
<ul>
<li>On va afficher un room grace à son id conteu dans un hotel.<br>
Dans la fonction getHotelById$ on utilise switchmap

````angularjs
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x)),
        switchMap(hotel =>  => ths.getRoomById$'TTchhMxW79EeUJitgVf3'),
        tap(x => console.log(x)),
      );
  }
````
Avec tap on va d'abord affiché l'hotel sur le console et le room contenant dans l'hotel qui a l'id 'TTchhMxW79EeUJitgVf3'
</li>
<li>On va affiché le room autrement d'une maniére plus propore.



````angularjs
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x)),
        switchMap(hotel => this.getRoomById$(hotel.rooms[0])),
        tap((rooms: RoomModel[]) => this.hotel.roomData = rooms),
      );
  }
````
On fait le switchmap en récupérant l'hotel et comme dans l'hotel on a array de string des rooms donc on fait un hotel.room[0] qui donne le prémier string du tableau et qu'on va renséigner dans la fonction getRoomById$ qui va détreminer notre room. Puis un console.log dans tap pour l'afficher.

</li>
<li>

Si on veut afficher les rooms qui dans l'hotel en même temps on utilise un **combineLatest** en récupérant d'abord le tableau de string(hotel.rooms) puis faire un map et detreminer le room avec getRoomById$(roomId)


````angularjs
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x)),
        switchMap(hotel => combineLatest((hotel.rooms).map((roomId) => this.getRoomById$(roomId)))),
        tap(x => console.log(x)),
      );
  }
````
En appuyant sur le boutton get hotel on a d'abord l'hotel puis tous les rooms contenus dans l'hotel.
</li>

</ul>
</li>
<li>On voudrais afficher l'hotel et ses rooms dans notre page html de l'application.
<ul>
<li>On definit la variable hotel dans hotel.service.ts</li>
<li>Au lieu de faire un console.log dans  tap on definit un nouveau hotel qu'on remplace par cet hotel
 
 
 ````angularjs
tap((hotel: HotelModel) => this.hotel = hotel),
````
 </li>
 <li>On definit aussi un nouveau rooms qu'on remplace par le tableau des rooms de roomsData
 
 
 ````angularjs
tap((rooms: RoomModel[]) => this.hotel.roomData = rooms),
````
Ensuite on fait un map qu'on importe qui return this.hotel pour afficher entiérement l'hotel pas que les chambres.


````angularjs
map(() => this.hotel)

````

On a le code complet dans hotel.service.ts


````angularjs
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {combineLatest, Observable, pipe} from 'rxjs';
import {HotelModel} from '../model/hotel.model';
import {map, switchMap, tap} from 'rxjs/operators';
import {RoomModel} from '../model/room.model';


@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;
  hotel: HotelModel;
  constructor(
    private httpClient: HttpClient
  ){ }

  getAllHotel(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.post(this.root + 'hotels', hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
        );
  }
  deleteHotelById$(hotelId: string): Observable<string> {
    return (this.httpClient.delete(this.root + 'hotels/' + hotelId) as Observable<string>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  putHotelById(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.put(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  patchHotelById$(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.patch(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>)
      .pipe(
        tap((hotel: HotelModel) => this.hotel = hotel),
        switchMap(hotel => combineLatest((hotel.rooms).map((roomId) => this.getRoomById$(roomId)))),
        tap((rooms: RoomModel[]) => this.hotel.roomData = rooms),
        map(() => this.hotel)
      );
  }
  getRoomById$(roomId: string): Observable<RoomModel> {
    return (this.httpClient.get(this.root + 'rooms/' + roomId) as Observable<RoomModel>);
  }
}





````
 </li>
 <li>Dans app.component.ts On definit le variable hotel.<br>
 Dans la fonction getHotel dans le tap on definit un hotel qu'on remplace par this.hotel au lieu de faire un console.log
 
 
 ````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {combineLatest, Subject, Subscription} from 'rxjs';
import {HotelService} from './service/hotel.service';
import {HotelModel} from "./model/hotel.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  hotel: HotelModel;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private hotelService: HotelService
  ) {
  }
  ngOnInit(): void {
    combineLatest([this.hotelService.getAllHotel()
      .pipe(
        tap(x => console.log(x))
      ),
      this.moodService.handleMood$
        .pipe(
          tap((message: string) => this.message = message)
        )])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
  createHotel(): void {
    this.hotelService.createNewHotel$({
      name: 'hotel des roses',
    roomNumbers: 20,
    pool: true
    })
      .subscribe();
  }
  deleteHotel(): void {
    this.hotelService.deleteHotelById$('Q9R3CzyBQOk2VyW6McVE')
      .subscribe();
  }
  putHotel(): void {
    this.hotelService.putHotelById('8jF9JEE9GUFe6FCFwPTw', {
      name: 'Novotel',
    roomNumbers: 30,
    pool: true
    })
      .subscribe();
  }
  patchHotel(): void {
    this.hotelService.patchHotelById$('AonmSa4BlVmUL0XHMHmt', {
      name: 'Rabbuson Blue'
    }).subscribe();
  }
  getHotel() {
    this.hotelService.getHotelById$('IWPMFMCLHsnLlBxqEvdF')
      .pipe(
        tap((hotel: HotelModel) => this.hotel = hotel)
      )
      .subscribe();
  }
getRoom() {
    this.hotelService.getRoomById$('TTchhMxW79EeUJitgVf3')
      .pipe(
        tap(x => console.log(x))
      )
      .subscribe();
}
}

````
 </li>
 <li>Dans app.component.html on fait un h1 pour l'afficher en faisant l'interpolation de hotel
 
 
 ````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>

<button (click)="getHotel()">get hotel</button>
<button (click)="getRoom()">get room</button>
<h1>{{hotel | json}}</h1>
<router-outlet></router-outlet>


````
 </li>
 En appuyant sur le boutton get hotel on a l'hotel avec ses attibuts notamment ses rooms qui sont affiché sur la page.
</ul>


</li>
</ul>