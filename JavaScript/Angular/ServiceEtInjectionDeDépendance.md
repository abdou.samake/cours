# Service et Injection de dépendance

On cré un service pour stocker les données au lieu de les 
mettre dans un component enfant menu1.component.ts 
par exemple. Ainis on pourra accéder directement sur les 
données à travers le component parent app.component.ts
<ul>
<li>Pour créer un service

``ng g s service/menu``<br>
g comme général<br>
s comme service<br>
menu est le nom qu'on lui donne
</li>
<li>On obtient ainsi un dossier créé nommé service contenant menu.service.spec.ts et menu.service.ts</li>
<li>Dans menu.service.ts on stocke nos données, ainsi dans la class MenuService on met notre tableau allRoutes qui était dans menu1.component.ts


````angularjs
import { Injectable } from '@angular/core';
import {MenuModel} from '../model/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 40}
  ];
  constructor() { }
}

````
<li>

Pour injecter un service on va créer un constructor dans app.component.ts qui à l'intérieur on precise que le service doit être utiliser que dans app.component en utilisant **private** et on lui donne un nom et de type MenuService.<br>
<br>

En faisant ``shift + fn + f6`` on a une proposition de nom<br>
<br>

````angularjs
  constructor(
    private menuService: MenuService
  ) {
  }
````


Ensuite definit allRoutes dans la class AppComponent

````angularjs
allRoutes: MenuModel[] = this.menuService.allRoutes;
````
qui permet d'accéder directement à allRoutes dans le component parent sans passer par des props.

````angularjs
import { Component } from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'promo4Angular';
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService
  ) {
  }
}

````

</li>


<li>Dans menu1.component.ts on peut enlever le @Input routes qu'on avait créé </li>

<li>Dans menu1.component.ts on definit routes de MenuModel[] qui n'est pas un input pour l'utiliser dans le parcour du ngFor.<br>
<br> Dans le contructor on fait le private<br>

````angularjs

  constructor(
    private menuService: MenuService
  ) {
  }
````

On routes dans le ngOnInit() par:

````angularjs
this.routes = this.menuService.allRoutes;
````


````angularjs
import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../model/menu.model';
import {MenuService} from '../../../service/menu.service';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';
  routes: MenuModel[];
  constructor(
    private menuService: MenuService
  ) {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
    this.routes = this.menuService.allRoutes;
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }
  @Input() age: number;

}


````
</li>
</ul>
<li>Le props routes est enlever dans app-menu1 dans le app.component.html, on a voulu maintenu le props age.</li>
On obtient ainsi le même resultat dans notre page.