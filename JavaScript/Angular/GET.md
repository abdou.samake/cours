# Get
<ul>
<li>On cré un nouveau service dans le dossier service qu'on nomme hotel

````angularjs
ng g s services/hotel
````

</li>
<li>Dans le fichier hotel.servive.ts on construit HTTPClient fournit par angular qui va nous permettre d'appeller des routes sur internet.


````angularjs
  constructor(
    private httpClient: HttpClient
  ) { }
````

</li>
<li>On cré la variable root est donne l'url de l'api dans environnement et on choisit l'environnement normal pas prop


````angularjs
 root: string = environment.api;
````
</li>
<li>On cré la fonction gatHotelById$ avec le symbole dollar à la fin qui fait un le get de notre hotel et le get va prendre comme argument la racine de notre api(this.root)</li>
<li>on cré une autre fonction getAllHotel qui permet de geter les hotels</li>


````angularjs

  getHotelById$(hotelId: string) {
    return this.httpClient.get(this.root + hotelId);
  }

  getAllHotel$() {
    return this.httpClient.get(this.root);
  }
````

<li>On cré les model de type HotelModel et RoomModel dans le dossier model</li>
<li>On renvoit un Observable de type HotelModel pour la fonction getHotelById et un Observable de type HotelModel[] pour getAllHotel pour pouvoir observer nos hotels.</li>
<li>Dans App.module on  importe HttpClientModule


````angularjs
    imports: [
        BrowserModule,
        AppRoutingModule,
        MainMenuModule,
        MoodModule,
        HttpClientModule
    ],
````

</li>
<li>Dans app.components.ts on appelle le hotelService dans le constructor.<br>
<br>
Dans ngOnInit à partir de hotelService on apelle getAllHotel, on fait un pipe et dedans un tap pour consolelogé les hotels, puis on souscri avec suscribe().



`````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {HotelService} from "./service/hotel.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private hotelService: HotelService
  ) {
  }
  ngOnInit(): void {
    this.hotelService.getAllHotel$()
      .pipe(
        tap(x => console.log(x))
      )
      .subscribe();
    this.moodService.handleMood$
      .pipe(
        takeUntil(this.destroy$),
        tap((message: string) => this.message = message)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}




`````
</li>
<li>Dans hotelService dans la fonction getHotelById on ajoute hotels dans notre url pour geter les hotels


````angularjs
  getAllHotel$(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
````

</li>
</ul>
On a ainsi dans le console.log tous nos hotels.