# Créer des liens

Pour créer des liens 
<ul>
<li>On se met dans le component principal app.component.html</li>
<li>On utilise des ul et li et des a sans le href car le href reload l'application.

````angularjs
<h1>mon app</h1>
<ul>
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>
<router-outlet></router-outlet>
````
 
 
 </li>
</ul>