# Unsubcribe()

On gére le désabonnement du component.
<ul>
<li>

On ajoute **OnDestroy** pour l'autodestruction de l'abonnement du component.

````
export class AppComponent implements OnInit, OnDestroy{
````
on uitlise la fonction OnDestroy

````angularjs
  ngOnDestroy(): void {
  }
}
````

</li>

## Methode simple et Moche
<li>On definit une variable subscription de type subscription.<br>
<br>
Au moment où on s'abonnne dans le component on appelle subscription et l'abonnement va s'enrégistrer dans une variable subscription.
</li>
<li>

Dans le ngOnDestroy() on détruit le subscription par **unsubscribe()**</li>
</ul>
On a le code dans app.components.ts

````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  subscription: Subscription;
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService
  ) {
  }
  ngOnInit(): void {
    this.subscription = this.moodService.handleMood$
      .pipe(
        tap((message: string) => this.message = message)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

````
Et la on s'assure que quand on change de page ou quand le component va être détruit on va se désabonner du component.<br>
Mais cette méthode est moche car ça nous oblige de créer la variable subscription et de rappeller à plusieurs fois this.subscription quand on a plusieurs Subscription.
<ul>
<li>

## Methode takeUntil

<ul>
<li>

On utilise un subject pas un behavior subject
un **Subject** est un behavior qu'on initialise pas et dans lequel on peut mettre une valeur quand on le veut.

````angularjs
destroy$: Subject<boolean> = new Subject();
````

</li>
<li>On fait un this.destroy.next(value) pour mettre une valeur qui va être true pour se désabonner.<br>
 Ensuite le faire le destroy$ par complete()</li>
 <li>
 
 Dans le **pipe** on s'abonne tant que le destroy$ n'a pas emi la valeur true. </li></ul>
</li>


````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService
  ) {
  }
  ngOnInit(): void {
    this.moodService.handleMood$
      .pipe(
        takeUntil(this.destroy$),
        tap((message: string) => this.message = message)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}




````
</ul>

Ainsi on s'abonne au component tant que destroy n'a pas emi la valeur true et au cas ou il emet la valeur true c'est à dire qu'il n'existe plus, on se désabonne. 