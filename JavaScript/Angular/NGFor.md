# NG For
Pour ne pas faire des répétitions des routerLink dans notre fichier menu1.component.html, on fait d'abord un tableau nommé allRoutes où on met les routes

````angularjs
import { Component, OnInit } from '@angular/core';
import {MenuModel} from '../../../model/menu.model';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';
  allRoutes: MenuModel[] = ['home', 'à-propos'];

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }

}

````
<ul><li>

on fait ensuite une boucle for en utilisant **ngFor**.<br>
**ngFor** est une directive de la boucle For.<br>
On definit une variable nommée current qui va être pris dans le tableau allRoutes.

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}

  <li *ngFor="let current of allRoutes">
    <a  [routerLink]= "current">{{current}}</a>
  </li>

</ul>
<p>menu1 works!</p>

````
Ainsi on a un seul routerLink et les deux liens home et à-propos sont affichés.

 
</li></ul>

## Si on veut avoir un nom différent des noms des path sur la liste du menu:
<ul>
<li>On modifie allRoutes en un tableau contenant des objets

````angularjs
export class Menu1Component implements OnInit {
  name = '';
  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil'},
    {url: 'à-propos', name: 'à propos'}
    ];
````

</li>
<li>On change le type
<ul>
<li>On cré un dossier model</li>
<li>Dans model on cré un fichier menu.model.ts</li>
<li>Dans menu.model.ts on cré l'interface MenuModel

````angularjs
export interface MenuModel {
  url: string;
  name: string;
}
````

</li>
</ul></li>
<li>Dans le fichier menu1.component.html on precise l'attribut current.url et current.name

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}

  <li *ngFor="let current of allRoutes">
    <a  [routerLink]= "current.url">{{current.name}}</a>
  </li>

</ul>
<p>menu1 works!</p>

````

</li>
</ul>
On a ainsi sur la liste du menu1 des noms de lien différents des path.