# Interpolation Simple

A noter le ````<router-outler></router-outler>```` nous permet d'avoir l'endroit où on est sur les pages sur lesquelles on navigue.

<ul><li>
En angular les interpolations se font avec des doubles crochets 


``{{}}``

</li>
<li>

On fait l'interpolation d'un string **name** parexemple dans le fichier
menu1.component.html 

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Menu1 Bonjour, {{name}}
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>
<p>menu1 works!</p>

````

</li>
<li>On donne la valeur de name dans le fichier menu1.component.ts dans la class Menu1Component

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name= 'Jean Paul'

  constructor() { }

  ngOnInit(): void {
  }

}

````
Et on le resultat sur la page.
</li>
<li>On peut definir en faisant le typage de name en donnant la valeur de name dans la fonction d'initialisation du component 

**ngOnInit**:

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name: string;

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

}

````

</li>
<li>On peut le faire aussi de façon implicite:

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

}

````

</li></ul>