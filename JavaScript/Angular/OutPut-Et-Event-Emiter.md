# Output et event emiter
Pour envoyer quelque chose au parent il faut emettre une information. Et pour emettre une information il faut avoir un **event emiter**.

<ul>
<li>

On cré le event emiter nommer **handleMoodChange** dans mood.component. C'est un output de type **EventEmitter<string>** de string, on le cré en faisant égale à new Event Emitter<string>()

````angularjs
 @Output() handleMoodChange: EventEmitter<string> = new EventEmitter<string>();
````
**handleleMoodChange** est un EventEmitter qui est une fonction fournit par angular qui permet d'envoyer un signal au component parent pour lui dire qu'il ya un changement chez le component enfant.
 
</li>
<li>Dans nos methode on emit nos string


````angularjs
import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {
  @Output() handleMoodChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
  Iok() {
    this.handleMoodChange.emit('ok');
  }
  NotOk() {
    this.handleMoodChange.emit('not ok');
  }
  Neutre() {
    this.handleMoodChange.emit('neutral');
  }

}

````
</li>
<li>On fait abonner le component parent a handleMoodChange pour qu'il reçoit l'action des cliques
<ul>
<li>

On cré une fonction **manageMoodChange(message:string)** qui va receptionner un message de string dans notre cas.

````angularjs
import { Component } from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'promo4Angular';
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService
  ) {
  }
  manageMoodChange(message) {
    this.message = message;
  }
}


````
on déclare le paramétre message et on rempli la fonction manageMoodChange en récupérant le message.
</li>
<li>

Maintenant dans app.component.html on fait abonner handleMoodChange en appelant la fonction manageMoodChange et lui passer le paramétre **$event** fournit par angular non pas le paramétre message</li>
<li>On fait un h1 pour affiché le message correspondant à chaque fois qu'on clique sur un boutton en interpolant message.


````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood (handleMoodChange)="manageMoodChange($event)"></app-mood>
<router-outlet></router-outlet>

````


</li>
</ul></li>
</ul>
Ainisi le event emiter a bien été fait chez lr parent, à chaque fois qu'on clique sur un boutton on a le message qui s'affiche.