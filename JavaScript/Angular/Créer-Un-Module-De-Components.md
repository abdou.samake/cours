# Créer un modulede de components

Pour créer un module de component on met sur terminal

```ng g m components/main-menu```

**main-menu** est le nom de notre module

On a ainsi créé un seul module nommé **main-menu.module.ts** dans le dossier main-menu.
<ul>
<li>On va créer un sous module dans le module main-menu. On le nomme menu1

````ng g c components/main-menu/menu1```` <br>
c pour component<br>
Ainsi le component menu1 est créé avec les fichiers menu1.components.html, menu1.components.scss, menu1.compnents.spec.ts, menu1.components.ts.

</li>
<li>On cré un autre component de la même maniére

````ng g c components/main-menu/menu2````

On le fichier main-menu.modules.ts est mis en jour. Et ce fichier on a la déclaration des deux components.
</li>
<li>On cré un troisiéme component

````ng g c components/main-menu/menu-logo````

</li>
<li>le fichier main-menu.modules.ts est mis a jour.<br>

On fait un export des deux components menu1 et menu2. On ne fait pas l'export de menu-logo pour ne pas autorisé son utilisation.

````angularjs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Menu1Component } from './menu1/menu1.component';
import { Menu2Component } from './menu2/menu2.component';
import { MenuLogoComponent } from './menu-logo/menu-logo.component';
import {RouterModule} from '@angular/router';




@NgModule({
  declarations: [
    Menu1Component,
    Menu2Component,
    MenuLogoComponent,
  ],
  exports: [
    Menu1Component,
    Menu2Component,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class MainMenuModule { }

````

</li>
<li>Dans les fichiers menu1.components.html et menu2.components.html on peut on utilisé menu-logo même s'il n'est pas exporter car ils sont defini du même components en mettant le prefix app- qui est utilisé par defaut par angular
<ul>
<li>Dans menu1.components.html

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Menu1
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>
<p>menu1 works!</p>

````

</li>
<li>Dans menu1.components.html

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Menu2
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>

<p>menu2 works!</p>

````
On import routerLink dans le fichier main-menu.modules.ts pour que ça marche
</li>
<li>Dans le fichier app.components.html

````angularjs
<h1>mon app</h1>
<app-menu1></app-menu1>
<router-outlet></router-outlet>

````
On appelle le component menu1 ou menu2.
</li>
</ul>
</li>
</ul>
Aprés un start on obient notre page html avec le menu1 qui contient les router home et à-propos.