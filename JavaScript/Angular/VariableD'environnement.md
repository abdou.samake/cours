# Les variables d'environnement

Dans le dossier environnement on a deux fichiers: 
<ul><li>Environnement.ts qu'on utilise quand on est en local. Dedans on cré la variable api et on met le localhost de notre serveur base de données.


````angularjs
export const environment = {
  production: false,
  api: 'http://localhost:3015/api/ '
};
````

</li>
<li>environnement.props.ts est celui qu'on utilise quand on va bulder l'application et le mettre en props.Dedans on fait la même que dans environnement.ts


````angularjs
export const environment = {
  production: true,
  api: 'http://localhost:3015/api/ '
};
````

</li>

</ul>
Ainis on a accés à l'adresse vers notre base de données.