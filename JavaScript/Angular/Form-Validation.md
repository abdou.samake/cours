# Form validation

Dans le formulaire on peut mettre des champs obligatoires avec **required**.<br>
<ul>
<li>

Dans le tableau vide sur les champs de formulaire on fait des **Validators**. On a plusieurs liste de validators comme required, maxlength le nombre maximale etc.</li>


````angularjs
  ngOnInit(): void {
  this.myForm = this.fb.group({
    name: ['hotel des fleurs', [Validators.required, Validators.maxLength(5)]],
    roomNumbers: [null, []],
    pool: [false, [Validators.requiredTrue]]
  });
  }
````
Ici on a fait un required de remplir d'abord le champ name obligatoirement, ensuite le nombre de caratére ne doit pas dépassé 5. Ensuite le pool doit être à true avec requiredTrue.<br>
Ainis sans ces obligations le formulaire est invalid.

<li>Pour gérer les valid et les invalid du formulaire on fait un div dans form.service.html pour montrer si formulaire est valid ou invalid


````angularjs
<div Vali>est il valide ? : {{myForm.valid}}</div>
<div>est il invalide ? : {{myForm.invalid}}</div>

<form [formGroup]="myForm" (ngSubmit)="sendForm()">
  <label>
    name
    <input type="text" formControlName="name"/>
  </label>
  <label>
    nombre de chambre
    <input type="number" formControlName="roomNumbers"/>
  </label>
  <label>
    <input type="checkbox" ng-checked="all" formControlName="pool">Piscine ?
  </label>
  <button type="submit">envoyer</button>
</form>

````
Ainsi on à (est il valid) true si les champs sont bien rempli ou false le cas contraire.
</li>
<li>On peut afficher un message d'erreur sur le champ name si le champ n'est rempli correctement.<br>
On fait un *ngIf de myform où on va géter name et lui passé invalid. Ce qui veut dir que au cas où le champ name est invalid le message sera affiché.


````angularjs
  <label>
    name
    <input type="text" formControlName="name"/>
    <em *ngIf="myForm.get('name').invalid">Le champ est obligatoire et doit fairez moins de 5 lettre</em>
  </label>
````
</li>
<li>Pour ne pas envoyer un formulaire invalid on apporte des modifications au niveau de la fonction sendForm


````angularjs
  sendForm(): void {
    if (this.myForm.valid) {
      console.log(this.myForm.value);
    }else {
      alert('le formulaire est invalid');
    }
  }
````
On ajoute une condition if avec comme condition le formulaire est valide. Si le formulaire est valid on affiche sa valeur sur console.log sinon on fait un alert qui nous dit que le formulaire est invalid.
</li>
<li>En cas d'erreur on peut affiché le type d'erreur qui indique comment doit être le champ.<br>
On fait l'exemple du champ name.
<br>
On fait un div en faisant l'interpolation de myform geter name indiqué errors et tranformer en json


````angularjs
<div>{{myForm.get('name').errors | json}}</div>
````

Ainsi si un rempli le champ name avec un nom de plus de 5 caractére il nous affiche:


````angularjs
{ "maxlength": { "requiredLength": 5, "actualLength": 10 } }

````
qui indique le caractére requiert est 5 et qu'on a mis 10.
</li>
</ul>

Dans from.component.ts:

````angularjs
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
myForm: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  this.myForm = this.fb.group({
    name: ['hotel des fleurs', [Validators.required, Validators.maxLength(5)]],
    roomNumbers: [null, []],
    pool: [false, [Validators.requiredTrue]]
  });
  }
  sendForm(): void {
    if (this.myForm.valid) {
      console.log(this.myForm.value);
    }else {
      alert('le formulaire est invalid');
    }
  }

}

````

Dans form.component.html:


````angularjs
<div Vali>est il valide ? : {{myForm.valid}}</div>
<div>est il invalide ? : {{myForm.invalid}}</div>
<div>{{myForm.get('name').errors | json}}</div>

<form [formGroup]="myForm" (ngSubmit)="sendForm()">
  <label>
    name
    <input type="text" formControlName="name"/>
    <em *ngIf="myForm.get('name').invalid">Le champ est obligatoire et doit fairez moins de 5 lettre</em>
  </label>
  <label>
    nombre de chambre
    <input type="number" formControlName="roomNumbers"/>
  </label>
  <label>
    <input type="checkbox" ng-checked="all" formControlName="pool">Piscine ?
  </label>
  <button type="submit">envoyer</button>
</form>

````
