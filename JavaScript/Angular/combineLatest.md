# CombineLatest
Dans notre code précédent on avait deux fois subscribe et deux fois takeUntil. Pour éradiquer à cela on utilise **combineLatest** qui prend en paramétre un array et qui combine nos abonnements.
Dans cette array on va mettre l'abonnement a getAllHotel et handleMood.<br>
<br> On met un pipe contenant un seul takeUntil<br>
<br>On mettra à la fin un subscribe qui nous souscrit une seule fois.

````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {combineLatest, Subject, Subscription} from 'rxjs';
import {HotelService} from './service/hotel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private hotelService: HotelService
  ) {
  }
  ngOnInit(): void {
    combineLatest([this.hotelService.getAllHotel()
      .pipe(
        tap(x => console.log(x))
      ),
      this.moodService.handleMood$
        .pipe(
          tap((message: string) => this.message = message)
        )])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}




````
On a ainsi nos deux abonnements qui marchent, on se souscrit une seule fois et quand on appelle le destroy par takeUntil nos deux abonnements de désabonnent.