# Les Inputs
Les inputs sont des propos spécial qu'on définit avec **@Input()**

<ul>
<li>On transfert notre tableau des routes allRoutes dans le component principal app.component.ts


````angularjs
import { Component } from '@angular/core';
import {MenuModel} from './model/menu.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'promo4Angular';

  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 30}
  ];
}
````

</li>
<li>On cré un input nommé routes dans menu1.component.ts qui sera de type MenuModel

````angularjs
export class Menu1Component implements OnInit {
  name = '';
  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }
  @Input() routes: MenuModel;
````

</li>
<li>Dans menu1.component.html on parcourir routes non pas allRoutes dans le ngFor.

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Bonjour, {{sayHello().name}} {{sayHello().age}}
  <div>J'ai {{age}} ans </div>

  <ng-container *ngFor = "let current of routes">
    <ng-container *ngIf="age>current.minumAge">

      <li>
        <a [routerLink]= "current.url">{{current.name}}</a>
      </li>
    </ng-container>
  </ng-container>

</ul>
<p>menu1 works!</p>

````


</li>
<li>Dans app.component.html on donne la valeur de allRoutes à routes en faisant une interpolation d'attribut.
dans app-menu1.


``````angularjs
<h1>mon app</h1>
<app-menu1 [routes]="allRoutes"</app-menu1>
<router-outlet></router-outlet>

``````
</li>
<li>On cré un autre input age pour amener age dans le component principal


````angularjs
import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../model/menu.model';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';
  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }
  @Input() routes: MenuModel;
  @Input() age: number;

}


````

</li>
<li>On transfert age dans app.component.ts en lui donnant sa valeur

````angularjs
import { Component } from '@angular/core';
import {MenuModel} from './model/menu.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'promo4Angular';

  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 30}
  ];
  age = 50;
}
````

</li>
<li>Dans app.component.html on fait l'interpolation de l'attribut age qui va prendre la valeur age definit dans le component principal


````angularjs
<h1>mon app</h1>
<app-menu1 [routes]="allRoutes" [age]="age"></app-menu1>
<router-outlet></router-outlet>

````

</li>
</ul>
On obient ainsi nos deux liens qui s'affichent.