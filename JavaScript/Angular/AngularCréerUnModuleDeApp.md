# Angular créer un module de page
Il ya deux types de modules:
<ul>
<li>

**Module de type page** c'est à dire l'intégralité de la page. On a le homeModule par exemple.</li>

<li>

**Module de type component** c'est à dre l'endroit où on va mettre les components. On a le App module par exemple.

</li>
</ul>

## Créer une module

Pour créer une module de page on met sur terminal:

````angularjs
`ng g m home --route=home --module=app-routing
````
ng g(générate): c'est à dire fabriqué quelque chose.
Ainsi on cré le module home de route home c'est a dire de path home qui est dans le module component App-routing.<br>

On a ainsi le module home qui est créé avec les fichiers html, scss, le module de la page, le module de routing.
<ul>
<li>

Dans **app-routing.module** qui est le routing pricipal de notre application on a une variable nommée **routes** qui est un array qui contient les modules crées en objet contenant le **path** et le **loadchildren** qui charge le module créé en l'important.
le .then c'est pour une demande d'attente comme async await.

````angularjs
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModule } from './home/home.module';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  {
    path: 'à-propos',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule)
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes), HomeModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }

````
On a créé aussi un autre module **about** de router **à-propos** c'est dire de path **à-propos**en faisant la même chose que pour home

````angularjs
`ng g m about --route=à-propos --module=app-routing
````
Si on fait dans app-routing-module ``ctrl + clique`` sur `ng g m home --route=home --module=app-routing ou sur AboutModule on tombe directement sur le fichier homeModule.ts ou aboutModule.ts
</li>

<li> 

Dans**app.component.html** on supprime tout le code  et on met la balise
``<router-outlet></router-outlet>```qui est le router principal sur lequel on va mettre les informations des routes.<br>
On peut mettre un <h1> par exemple:

```<h1>Mon App</h1>```
</li>
<li>Dans home.compnent.html on peut modifier en mettant un text avec une class pour gérer le style

````angularjs
<p class="main">home works!</p>
````

</li>
<li>On definit notre class dans home.component.scss

````angularjs
.main {
  background: blue;
}
````

</li>
</ul>
Si on fait start et qu'on met sur l'url le router home on a le resultat.

<img src="Capture1.PNG" alt="image">

<ul>
<li>Si on met la class main dans le fichier app.component.html il ya pas de changement du background sur le text mon app.
Car le CSS il est géré que par chaque component.</li>
</ul>