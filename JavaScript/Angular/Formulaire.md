# formulaire

<ul>
<li>On commence par cré un module form


````angularjs
ng g m form --route=form --module=app-routing
````
</li>
<li>Dans le menu on ajoute le route form


````angularjs
import { Injectable } from '@angular/core';
import {MenuModel} from '../model/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 40},
    {url: 'form', name: 'formulaire', minumAge: 2}
  ];
  constructor() { }
}

````

</li>
<li>Dans form.module.ts on importe ReactiveFormsModule


````angularjs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [FormComponent],
  imports: [
    CommonModule,
    FormRoutingModule,
    ReactiveFormsModule
  ]
})
export class FormModule { }

````

</li>
<li>

Dans form.component.ts on definit la variable **myForm** de type FormGroup donné par angular.<br>
 <br>
 On construit la variable fb de type FormBuilder pour construire le formulaire.<br>
  Ensuite dans la fonction ngOnInit on cré le formulaire avec les éléments à remplir, on peut donné des valeurs par défaut soit null oubien une valeur. Les valeurs des attributs sont mis dans un tableau qui contient aussi un autre tableau vide.
   
   
   ````angularjs
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
myForm: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  this.myForm = this.fb.group({
    name: ['hotel des fleurs', []],
    roomNumbers: [null, []],
    pool: [true, []]
  });
  }
  sendForm(): void {
  console.log(this.myForm.value);
  }

}

````
le .group c'est la façon de definir un formulaire, Angular considére les formulaires par groupe.
</li>
<li>Dans form.component.html on cré le formulaire par la balise form de javascript.
<ul>
<li>Dans form on fait d'abord l'interpollation de l'attribut formGroup de angular à qui on va égaler à myForm</li>
<li>On fait un ngSubmit pour soumission du boutton pour envoyer les données. On fait l'interpolation de ngSubmit qui est égale à une fonction sendForm qu'on va définir dans form.component.ts


````angularjs
 sendForm(): void {
  console.log(this.myForm.value);
  }
````

````angularjs
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
myForm: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  this.myForm = this.fb.group({
    name: ['hotel des fleurs', []],
    roomNumbers: [null, []],
    pool: [true, []]
  });
  }
  sendForm(): void {
  console.log(this.myForm.value);
  }

}

````
</li>
<li>On fait des label qui vont contenir les input. Et pour chaque input on fait un formControlName qui est égale au nom de l'attribut.<br>
On cré aussi le boutton de type submit</li>
</ul>


````angularjs
<form [formGroup]="myForm" (ngSubmit)="sendForm()">
  <label>
    name
    <input type="text" formControlName="name"/>
  </label>
  <label>
    nombre de chambre
    <input type="number" formControlName="roomNumbers"/>
  </label>
  <label>
    <input type="checkbox" ng-checked="all" formControlName="pool">Piscine ?
  </label>
  <button type="submit">envoyer</button>
</form>

````


</li>
</ul>
On a ainsi le formulaire qui permet de donner un objet d'hotel, si on rempli les différents champs du formulaire on obtient notre objet d'hotel avec ses attributs et les valeurs correspondants.