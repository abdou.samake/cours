# Post Delete Patch

## Post 
<ul>
<li>
Pour faire le post on cré une fonction createNewHotel dans hotelService. Cette fonction prend en paramétre hotel de type HotelModel, ensuite  dans httpClien on fait un post qui prend en paramétre root + hotels et l'hotel.<br>
Dans notre backend on voit que le post newHotel renvoit un Promise hotelModel dans notre fonction createHotel sera de type Observable<HotelModel>. Comme je return un httpClient donc le nom de la fonction doit se finir par un $, d'où createNewHotel$.


````angularjs
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HotelModel} from '../model/hotel.model';


@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;
  constructor(
    private httpClient: HttpClient
  ){ }
  getHotelById(hotelId: string): Observable<HotelModel> {
    return this.httpClient.get(this.root + hotelId) as Observable<HotelModel>;
  }
  getAllHotel(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return this.httpClient.post(this.root + 'hotels', hotel) as Observable<HotelModel>;
  }
}





````


</li>
<li>
Pour faire un Post on doit faire un boutton, donc on le cré dans app.component.html
</li>
<li>Dans app.component.ts on cré la méthode createHotel pour créer la nouvelle hotel qu'on va poster, ensuite on ajoute souscribe pour s'abonner à la fonction.
 
 
 
 ````angularjs
 createHotel() {
    this.hotelService.createNewHotel$({
      name: 'hotel des roses',
    roomNumbers: 20,
    pool: true
    })
      .subscribe();
  }
}
````
 </li>
 <li>Dans app.component.html on fait un click et appelle la fonction createHotel
 
 
 ````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>
<button (click)="createHotel()">envoyer</button>
<router-outlet></router-outlet>


````
 </li>
 
</ul>
Resultat: quand on appuie sur le boutton on a un hotel qui est envoyer dans notre base de données sur firebase avec un nouveau id qu'on peut voir sur NetWork.

<ul>
<li>Pour voir l'hotel poster sur console on fait un pipe tap dans le fonction createNewHotel$ qui return un httpClient


````angularjs
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HotelModel} from '../model/hotel.model';
import {tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;
  constructor(
    private httpClient: HttpClient
  ){ }
  getHotelById(hotelId: string): Observable<HotelModel> {
    return this.httpClient.get(this.root + hotelId) as Observable<HotelModel>;
  }
  getAllHotel(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.post(this.root + 'hotels', hotel) as Observable<HotelModel>)
        .pipe(
            tap(x => console.log(x))
        );
  }
}

````
On voit ainsi l'hotel créé dans le console.log et envoyer dans firebase.

</li>
</ul>

## Delete
Pour faire le delete
<ul>
<li>On cré la fonction deleteHotelById qui si on verifie dans notre backend renvoit un observable de string pour dire que l'hotel a été bien supprimé.



````angularjs
  deleteHotelById$(hotelId: string): Observable<string> {
    return (this.httpClient.delete(this.root + 'hotels/' + hotelId) as Observable<string>)
      .pipe(
        tap(x => console.log(x))
      );
  }
}
````
</li>
<li>Dans app.component.ts cré la methode deleteHotel pour l'appéler dans le boutton et souscrire à la fonction deleteHotelById.<br>
On renseigne ici l'id de l'hotel qu'on veut supprimer qu'on a pris dans notre base de données sur firebase.


````angularjs
  deleteHotel(): void {
    this.hotelService.deleteHotelById('5eO8LFtmdE4yuOsuhanq')
      .subscribe();
  }
}
````

</li>
<li>Dans app.component.html on definit notre button pour deleter et appeler la fonction deleteHotel


````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>
<button (click)="createHotel()">envoyer</button>
<button (click)="deleteHotel()">delete</button>
<router-outlet></router-outlet>


````

</li>
</ul>

## Put et patch
On cré la fonction putHotelById qui prend en paramétre l'id et newhotel qui est la nouvelle hotel à remplacer.<br>
<br>
Pour patch on fait la même 

````angularjs
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HotelModel} from '../model/hotel.model';
import {tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;
  constructor(
    private httpClient: HttpClient
  ){ }
  getHotelById(hotelId: string): Observable<HotelModel> {
    return this.httpClient.get(this.root + hotelId) as Observable<HotelModel>;
  }
  getAllHotel(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.post(this.root + 'hotels', hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
        );
  }
  deleteHotelById$(hotelId: string): Observable<string> {
    return (this.httpClient.delete(this.root + 'hotels/' + hotelId) as Observable<string>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  putHotelById(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.put(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  patchHotelById(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.patch(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
}





````
Et dans app.component.ts on souscrit à ces fonctions:

````angularjs
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {combineLatest, Subject, Subscription} from 'rxjs';
import {HotelService} from './service/hotel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private hotelService: HotelService
  ) {
  }
  ngOnInit(): void {
    combineLatest([this.hotelService.getAllHotel()
      .pipe(
        tap(x => console.log(x))
      ),
      this.moodService.handleMood$
        .pipe(
          tap((message: string) => this.message = message)
        )])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
  createHotel(): void {
    this.hotelService.createNewHotel$({
      name: 'hotel des roses',
    roomNumbers: 20,
    pool: true
    })
      .subscribe();
  }
  deleteHotel(): void {
    this.hotelService.deleteHotelById$('7LbiRVRtKEg3CMCImaVm')
      .subscribe();
  }
  putHotel(): void {
    this.hotelService.putHotelById('AonmSa4BlVmUL0XHMHmt', {
      name: 'Novotel',
    roomNumbers: 30,
    pool: true
    })
      .subscribe();
  }
  patchHotel(): void {
    this.hotelService.patchHotelById('AonmSa4BlVmUL0XHMHmt', {
      name: 'Rabbuson Blue'
    }).subscribe();
  }
}




````
Dans app.component.html on cré les bouttons:

````angularjs
<h1>{{message}}</h1>
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>
<button (click)="createHotel()">envoyer</button>
<button (click)="deleteHotel()">delete</button>
<button (click)="putHotel()">put</button>
<button (click)="patchHotel()">patch</button>
<router-outlet></router-outlet>


````


