# Interpolation en detail

<ul>
<li>On peut faire une fonction et faire son interpolation </li>
<li>Mais on ne peut pas mettre de condition comme if</li>
<li>On ne peut pas mettre des map, des forEach non plus dans l'interpolation</li>
<li>Il faut faire une méthode dans la class du component et faire son interpolation</li>
<li>En Angular on interpole un resultat.</li>

<li>Dans le fichier menu1.components.ts cré la fonction (methode) dans la class Menu1Component

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return 'Hello';
  }

}


````

</li>
<li>On fait l'interpolation dans menu1.component.html

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Menu1 Bonjour, {{sayHello()}}
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>
<p>menu1 works!</p>

````
Et on le resultat qui s'affiche sur la page.
</li>
<li>

Pour obtenir un objet en interpolation, on utilise un **pipe** (|) qui va passer en tuyau ce qu'il ya dans la fonction q'on veut interpolé et le transform en json.

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';

  constructor() {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }

}


````

````angularjs
<app-menu-logo></app-menu-logo>
<ul>
  Menu1 Bonjour, {{sayHello() | json}}
  <li>
    <a routerLink="home">home</a>
  </li>
  <li>
    <a routerLink="à-propos">about</a>
  </li>
</ul>
<p>menu1 works!</p>

````

```{sayHello() | json}``` le pipe | passe en tuyau l'objet et le transform en json
ainsi on a le resultat de l'objet qui s'affiche sur la page.
<ul><li>Si on veut seulement afficher le nom de notre objet:

````angularjs
{{sayHello().name}}
````
Oubien l'age:

````angularjs
{{sayHello().age}}
````

</li></ul>
</li>
</ul>