# Click
On cré un nouveau component enfant mood pour créer des bouttons permettant d'afficher des humeurs en cliquant. Comme c'est un nouveau component on va créer un module d'abord dans components

```ng g m components/mood``` <br>
m pour module

On cré ensuite le component mood

```ng g c components/mood``` <br>
c pour module
<ul>
<li>Dans mood.module.ts on export le component [ModComponent] pour l'utiliser dans le component preincipal app.component.ts

````angularjs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoodComponent } from './mood.component';



@NgModule({
  declarations: [MoodComponent],
  exports: [MoodComponent],
  imports: [
    CommonModule
  ]
})
export class MoodModule { }

````

</li>
<li>Dans mood.components.ts on definit des methodes qui permettent d'afficher l'humeur en cliquant sur le boutton créé dans mood.component.html

````angularjs
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  Iok() {
    console.log('ok');
  }
  NotOk() {
    console.log('not ok');
  }
  Neutre() {
    console.log('neutre');
  }

}

````

</li>
<li>Dans mood.component.html on cré les boutton.
<ul>
<li>On gére les cliks dans le bouttons</li>
<li>On met en parenthèse le verb Click pour envoyer une action.</li></ul>

````angularjs
<div>
  <ul>
    <li>
      <button (click)="Iok()">je suis ok</button>
    </li>
    <li>
      <button (click)="NotOk()">je ne suis pas ok</button>
    </li>
    <li>
      <button (click)="Neutre()">je suis neutre</button>
    </li>
  </ul>
</div>

````

</li>
<li>Dans app.component.html on appel le component app-mood

````angularjs
<h1>mon app</h1>
<app-menu1 [age]="age"></app-menu1>
<app-mood></app-mood>
<router-outlet></router-outlet>

````

</li>
</ul>
On aisni les bouttons qui s'affichent dans la page et à chaque fois qu'on clique sur un boutton on a le message correspondant qui s'affiche sur la console.
</ul>