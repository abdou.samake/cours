# Object Keys and Values

En Javascript on a des méthodes qui permettent de tranformer des objects en arrays 

````javascript
const hostels = {

hotel1: {id: 1...},
hotel2: {id: 2...},
hotel3: {id: 3...}

}

const keys = Object.keys((hostels))

console.log(keys) 
// affiche ['hotel1', 'hotel2', 'hotel3'] qui a transformé les éléments de l'objets sous forme de tableau.

````
````javascript
const rooms = Object.values(hostels.hotel.rooms);
console.log(rooms);
// affiche les éléments de rooms sous forme de tableau.
````

Ainsi on peut appliquer à rooms les fonctions habituelles des arrays.

Exemple: 

````javascript
rooms.forEach((room) => console.log(room));

// affiche une par une les room.
````