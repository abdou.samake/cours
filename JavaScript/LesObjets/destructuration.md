# La destructuration des Objets 
On l'oblet suivant :
````javascript
const user = {
firstName: 'Seb',
lastName: 'Le prof',
age: 34,
billionnaire: 'false',
eyes: 'blue'
}
````
Pour appeller les propriétés de user, au de faire ``user.firstnName`` oubien  
``user.lastName`` On fait la destructuration 
````javascript
const {age, lastName} = user 
console.log(age, lastName)
// affiche 34, Le prof````