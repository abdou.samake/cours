#  Objects Attribut access

On a l'objet suivant :

````javascript
const user = {
firstName: 'Seb',
lastName: 'Le prof',
age: 34,
billionnaire: 'false',
eyes: 'blue'
}
````
On crée un tableau contenant les attibuts de l'objet qu'on veut récupérer.

````javascript
const attr = ['firstName', 'age', 'eyes'];


attr.forEach((attr) => console.log(user[attr]));

// affiche Seb, 34, blue
````

Cette notation permet d'avoir l'acces à l'attribut d'un objet quand on ne sait pas le nom de l'attribut à l'avance.