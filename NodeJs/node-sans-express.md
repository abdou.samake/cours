## Comment créer un serveur de noeud sans express


Créer un foichier appelé index.js par exemple, dans le fichier exigez le HTTP modumle comme ceci
     
     
````javascript
     const http = require('http')
 ````
     
Appelez la ``createServer()`` méthode dessus et affectez-la à une constante comme celle-ci:


````javascript
const server = http.createServer();
````

Appelez la ``listen()`` méthode sur la constante du serveur comme ceci:


``server.listen``

Donnez-lui un port à écouter.Maintenant, cela pourrait çetre n'importe quel port libre, mais nous utiliserons le port **3000**
qui est le port conventionnel. Nous avons donc ceci:

````javascript
const http = require('http');
const server = http.createServer();
server.listen(3000);
````
    
