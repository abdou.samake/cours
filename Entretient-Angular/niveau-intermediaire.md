## 18. Quelles sont les différents types de directives Angular existants ?

<ul>
<li>1 Les directives structurelles (ngIf, ngFor)</li>
<li>Les composants</li>
<li>Les directives d'attributs(ngClass, ngStyle)</li>
</ul>

## 19. Pouvez vous expliquez ce qu'est  Angular Ivy ?

Ivy désigne le nom du compilateur interne d'Angular depuis la version4.

## 20. Quel est le but de la route ayant comme chemin "**"

La route ayant comme chemin "**" a pour but d'intercepter systématiquement toutes les routes de votre application.

## 21. Qu'est ce que la ViewEncapsulation dans un projet Angular ?

Angular propose 3 stratégies d'encapsulation différents: **Emulated**, **Native** et **None**.
La stratégie la plus courrante est Emulated.

## 22. Pouvez vous expliquer ce que sont les guards dans un projet Angular ?

les guards sont des interfaces fournies par Angular qui permettenet de controler le comportement du routeur.

## 23. Pouvez vous expliquer ce que c'est le Data Binding ? 
Le Data Binding permet de connecter les données de votre application Angular avec le DOM de 3 maniéres différentes: **property binding**, **event binding** et **two way binding**.

## 24. Pouvez vous expliquer le fonctionnement du décorateur @NgModule

Un module Angular permet de regrouper differents éléments de votre projet afin de répondre à une fonctionnalité précise. Pour cela on utilise le décorateur **@NgModule**: imports, exports, declaration et bootstrap.

## 25 Qu'est ce que la balise <router-oulet> pour Angular ?

Le RouterOutlet est une directive du routeur d'Angular qui permet de dynamiser une portion de vos templates en fonction d'une url donnée.

## 26 Est ce que l'utilisateur des modules de routes est obligatoire ?

Les modules de route ne sont pas obligatoires avec Angular, mais il s'agit tout de même de la manière la plus recommandée de déclarer vos routes.

## 27. Quel est le rôle de la balise `<base>`dans une application Angular ?

La balise `<base>` permet au router d'Angular de construire des urls relatives au dossier app de votre projet.

## 28. Qu'est ce une variable référencée dans le template

Angular permet de faire référence à un élément du DOM directement depuis un template, grâce aux variables référencées dans le template.

