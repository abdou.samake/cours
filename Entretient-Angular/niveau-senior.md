## 29. Qu'est ce que la compilation AOT et JIT ? (Présenter leurs avantages respectivements)

Avec le compilateur AOT votre application Angular est compilé durant la phase de build, ce qui rend ce type de compilation plus adaptée pour un environnement de production.
Sinon votre application est compilé directement dans le navigateur avec la compilation JIT, ce qui rend cette compilation plus adaptée pour un environnement de developpement.

## 30 Pourquoi utiliser le Lazy Loading dans votre application Angular ?

Le lazy Loading présente de nombreuses avantages pour améliorer les performances de votre application.
C'est dailleurs la recommandation numéro 1 de Google pour accélérer le chargement de votre application Angular.

## 31. Qu'est ce que la programmation reactive et en quoi est elle lié à Angular ?
La programmation reactive est un nouveau paradigme qui permet de programmer à partir de flux de données asynchrones appelés des Observables
La programmation reactive est omniprésente dans une application Angular, et c'est un choix délibéré des équipes de Google en charge d'Angular.

## 32. Expliquer la différence entre les observables et les Promesses.

#### Definition:
Un **Observable** représente un flux de données asynchrones. On l'importe depuis la librairie de programmation reactive **RXjS**.
<br>
Une promesse est un objet qui représente l'etat d'une opération asynchrone qui peut être soit en cours, résolue ou rejeté. Les promesses sont disponibles nativements en JavaScript depuis ECMAScript6, donc pas besoin de l'importé.

#### Différence à retenir:
-Les observables peuvent émettre plusieurs données dans le temps alors que les promesses ne peuvent émettre qu'une seule valeur.
<br>
-Les observables sont déclarés paresseusement alors que les promesses sont immédiatement exécutées.
<br>
-Les observables peuvent être annulé avec la methode **unsubscribe** mais pas les promesses.
<br>
-Les observables disposent d'opérateur spécifiques pour gérer des flux de données asynchrones, mais pas les promesses.

## 33. Comment fonctionne l'instruction subscribe() ?

L'instruction **subscribe()** est une methode des observables qui va permettre de s'abonner à un flux de données asynchrone et qui prend un observer en parametre qui posséde les propriété **next()**, **error**, et **complete** pour traiter les différentes notifications reçues par le flux.

## 34. Quelle est la différence entre les Pipes pures et impures?

Les pipes pures sont dits sans états(ou stateless), ce qui signifie qu'ils ne gérent pas un état entre deux appels, et qu'ils ne produisent aucun effet de bord.
Un pipe pure est donc appelé uniquement lorsque Angular detecte un changement dans la valeur ou le paramétre passé au pipe.

#### Différence à retenir

Un **Pipe pure** est appelé uniquement lorsque Angular détecte un changement dans la valeur ou le comportement passé au pipe.
<br>
Un **Pipe impure** est appelé pour chaque cycle de détection de changement, peu importe que la valeur ou les parametres changent. Généralement on évite d'utiliser les pipes impures, car ils n'effectuent pas des traitements très efficaces.

## 35. Comment définir des en-têtes pour chque requête émise par Angular?

Les intercepteurs HTTP sont un mécanisme directement inclus dans Angular depuis la version 4.3
Il vous permettent d'intercepter toutes les requêtes HTTP qui transitent au sein de votre application et de pouvoir appiquer un traitement spécifique dessus.
