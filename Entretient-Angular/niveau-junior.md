## 1. Pouvais vous expliquer l'apparition des framework coté client comme Angular

Les frameworks coté client permettent donc le developpement d'applcication web avancées, beaucoup plus rapidement et efficacement qu'avec javascript et jquery.

## 2. Qu'est ce qu'une Single Page Application (SPA)

Avec les sites web traditionnels, à chaque fois que le client demande une nouvelle page web, le serveur lui renvoie cette page. Le problème de cette architecture est qu'elle est très consommatrice de ressouce et que le temps de chargement est plus long pour l'utilisateur. Les SAP repondent efficacement à cette problématique.

## 3. Quels sont les avantages d'Angular par rapport aux autres Frameworks concurrents.
Angular dispose de 5 avantages principaux par rapport à ces concurrents.
<ul>
<li>Des fonctionnalités prêtes à l'emploi (modules, routage, des librairies...)</li>
<li>Une interface utilisateur declarative(pas de DOM virtuel)</li>
<li>Assistance Google à long terme</li>
<li>Lasy Loading (chargement parésseux) le routeur d'Angular permet de charger uniquement les composants dont l'utilisateur à besoin lors de sa navigation.</li>
<li>L'outil Angular CLI: l'eco-systeme d'Angular bénéficie d'une interface en ligne de commande nommé Angular CLI, qui permet d'accélérer le processus de developpement</li>
</ul>

## 4. Pouvez vous expliquer quelles sont les différences entre Angular et jQuery.
La plus grande différence entre Angular et jQuery est que: 
<ul>
<li>Angular est un framework frontend Javascript.</li>
<li>jQuery est une bibliothèque Javascript proposant une syntaxe plus efficace que le Javascript natif.</li>
<li>Angular dispose d'une liaison de données bidirectionnel contrairement à jQuery.</li>
<li>Angular dispose d'un client HTTP intégré mais pas jQuery</li>
<li>Angular possède un systéme de validation de formulaire.</li>
</ul>
Malgré cela, il existe des similitudes entre les deux, telles que la manipulation du DOM et la prise en charge de l'animation.

## 5. Pouvez vous expliquer les différences entre Angular et AngularJs.
Il existe 5 différences majeurs:
<ul>
<ol>1. l'architecture, AngularJs utilise l'architecture MVC(Model-View-Controller) alors que Angular utilise l'architecture MVVM(Model-View-ViewModel)</ol>
<ol>2. Dans Angular on utilise le langage Typescript alors que dans AngularsJs on utilise Javascrpt</ol>
<ol>3. Avec Angular on peut avoir le support pour mobile inclus alors que dans AngularJs il n'ya pas.</ol>
</ul>

## 6. Comment fonctionne une application Angular ?

Toute les applications angular sont compilés par un outil qui s'appellle **Webpack**. Pour demarrer une application, Angular excécute les fichiers dans cet ordre:
<ul>
<li>angular.json</li>
<li>app.module.ts</li>
<li>app.component.ts</li>
<li>index.html</li>
</ul>

## 8. Quels sont les principaux éléments d'une application Angular?

#### 1. les composants
Un composant est composé d'une classe TypeScript qui controle sa logique, et une vue qui est une partie de l'interface utilisateur. Chque application Angular possède au moins un composant, nommé le composant racine **AppComponent**.

#### 2. Templates
Chaque composant est associé avec un template qui respecte un certain format ({}, (), []), afin de permettre à Angular de savoir comment il est cencé afficher ce composant.

#### 3 La liaison de données(ou Data Binding)

C'est le mécanisme par lequel Angular coordonne les données d'un composant et le template HTML.
Pour cela, Angular s'appuie sur l'interpolation, la liaison de données et la liaison d'événements.

#### 4. Les décorateurs

On peut indiquer à Angular le rôle de chaque classe grâce à des décorateurs **@component**, **@Directive**, **@Pipe**, ...

#### 5. Les Modules
Un module est un élémént Angular qui rassemble des composants, des services, directives, etc ... autour d'une fonctionnalité spécifique dans votre application.

#### 6. Les Directives 

Une directive permet de factoriser le comportement commun entre des éléments différents du DOM. Il existe 3 types de directives.
<ul>
<li>Les directives d'attribut(ngStyle, ngClass)</li>
<li>Les directives structurelles(ngIf, ngFor, ...)</li>
<li>Les composants qui sont des directives spécifiques</li>
</ul>

#### 7. Le Routage
Le routeur d'Angular permet de construire un système de navigation dans votre application coté client.
Le routeur est chargé d'interpréter une URL du navigateur comme une instruction pour naviguer vers le composant correspondant.

#### 8. Les Services

Un service permet de réutiliser des valeurs et des fonctions à travers l'ensemble de votre application Angular.
Techniquement, il s'agit d'une classe TypeScript associé avec le système d'injection de dépendance d'Angular.

#### 9. L'injection de dépendance

Angular utilise l'injection de dépendance pour fournir les dépendances requises aux composants dans le besoin.
Pour indiquer à Angular les éléments à injecter dont un composant, on utilise les paramétres du constructeur de ce composant.

## 9. Qu'est ce qu'une architecture MVVM ?
Une architecture MVVM(Model-View-ViewModel) est composé de 3 points:

<ul>
<li>1. Le Model</li>
<li>2. La Vue</li>
<li>3. La couche d'abstraction ViewModel
(Le Model s'occupe de gérer la logique des traitements dans votre application)</li>
</ul>

## 10. Qu'est ce que Angular CLI ?
Angular CLI est une interface en ligne de commande qui permet d'accélérer les developpements d'une application Angular grâce à quelque commandes comme **ng new**, **ng serve**, **ng generate**, **ng test**, **ng build**.

## 11. Qu'est ce que les "Lifecycle hooks" ou cycle de vie pour Angular.

Angular fournit des méthodes spécifiques pour mettre en place des traitements lors de la création, la mise à jour et la destruction d'un composant.
Les méthodes que l'on utilise le plus souvent sont **ngOnInit** et **ngOnDestroy** qui permettent d'initialiser un composant et de le nettoyer proprement par la suite lorsqu'il est detruit.

## 12. Qu'est ce que l'interpolation

L'interpolation est une syntace alternative à la liaison de propriété, jugée plus efficace.

## 13. Pouvez vous expliquer les différences entre constructor et ngOnInit.
Le rôle du construteur est de configurer l'injection de dependance et le rôle de ngOnInit est de definir le comportement de votre composant lors de son initialisation.

## 14. Qu'est ce qu'un composant

Un composant est l'élément le plus basique d'une application Angular. Il est toujours composé d'une classe TypeScript, d'un template HTML et doit absolument appartenir à un module pour qu'il soit utilisable dans votre projet.

## 15. Qu'est ce qu'un Service ?

Un service Angular est une classe qui peut être réutilisé ailleurs dans votre application grâce au mécanisme d'injection de dépendances.

## 16 Quelle est la différence entre un composant et un directive Angular.

Un composant est l'association d'une vue et d'un comportement spécifique.
Une directive permet de definir un comportement récurrent entrre plusieurs éléments du DOM.

## 17. Pouvez vous expliquer ce que sont les Pipes dans Angular.

Angular dispose d'un mécanisme des Pipes afin de nous permettre d'effectuer plus simplement des transformations de données simples au sein de nos templates.

