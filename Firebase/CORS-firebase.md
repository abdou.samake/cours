## Comment résoudre le problème CORS dans Firebase ou Google Cloud Storage

Pour **CORS** dans Firebase afin de permettre les appels des fichiers depuis le storage de firebase on procède comme suit:

<ul>
<li>Accéder à Google Cloud Console</li>
<li>Ensuite selectionner le projet que je veux resoudre</li>
<li>Activer Cloud Shell en cliquant sur l'icone du terminbal dans le coin superieur droit.</li>
<li>Une fenetre de terminal appaitra ensuite on execute la commande

````
nano cors.json
````


</li>
<li>Ensuite un editeur à l'interieur du terminal apparaitra, on tape le code suivant de dans



``````javascript
[
    {
        "origin": ["*"],
        "method": ["GET"],
        "maxAgeSecond": 3600
    }   

]
``````


</li>
<li>Ensuite on appuie sur  

```CTRL + X``` ensuite sur   ``Y``

</li>
<li>Après cela on exécute la commande


````
gsutil cors set cors.json gs://YOUR-BUCKET-PROJECT-URL
````


``YOUR-BUCKET-PROJECT-URL``  c'est l'URL du projet de stockage dans firebase.

</li>


En cas de succès on aura l'affichage des informations dans le terminal.


</ul>
