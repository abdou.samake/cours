# Boostrap et modification

Pour modifier un fichier dans Bootstrap il existe deux manières:

<ol>
<li>Faire des thèmes : cette méthode demande des connaissance poussées en CSS, qui est un travail plutot réservé aux intégrateurs.</li>
<li>Modifier à partir d'une feuille de style:<br> On crée une feuille de style css où on fait ses modifications. Puis on fait le lien entre le fichier html et le fichier css en indiquant le bon chemin pour que ces modifications soient prises en compte.</li>
</ol>