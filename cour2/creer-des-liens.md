# Créer des liens

On crée un liens entre plusieurs pages d'un site par la balise 
``
<a></a>
``
En mettant cette ligne de code
````
<a href="UREL">nom de la page</a>
````
entre les balises header.
