# Les containers Bootstrap

Sur Bootstrap on a deux types de container:
les containers fluides et les containers classiques.
<ul><li>
Pour le container normal qu'on nomme container, il est toujours centré au milieu et il laisse des espaces vide à gauche et à droite de la page.
<li>Pour le container fluid qu'on nomme container-fluid, il prend toute la largeur du site.</li></ul>

Ces containers sont prédefinis par Bootstrap et leur largeur ne doivent pas être modifier.