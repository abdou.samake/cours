# Deploy Firebase 

On installe Firebase en allant sur webstorm, on effectue la commande

npm i -g firebase-tools<br>
ensuite<br>
firebase --version qui donne la version de firebase installée et la ligne de commande firebase est reconnu et accessible.<br>
 Pour deployer firebase on procéde avec les étapes suivantes sur terminal:
 <ul>
 <li>firebase init hosting</li>
 <li>firebase login</li>
 <li>./ pour la configuration à la racine</li>
 <li>No pour ne pas supprimer le contenu de la page</li>
 <li>On ouvre le fichier firebase.json qui est créé, on ajoute "package.json" , "package-lock.json"</li>
 <li>firebase deploy</li>
 </ul>
 Et on voit appaitre l'url de notre site qui est mis en ligne.<br>
 Vendors: on l'emploi pour désigner le nom des dossiers qui viennent de tiers personnes.<br>
 le CDN c'est l'outil qui permet d'accéder au site internet le plus rapidement possible.
