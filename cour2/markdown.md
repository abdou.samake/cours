# Markdown

Au cour de la formation, on est appelé à prendre  des notes sur les cours vidéos pour mieux les maitriser.
La prémière chose à faire est de créer un nouveau projet, ensuite on créer des dossiers pour organiser les cours.

Pour les prendre ces notes on utilise markdown en créant un fichier dans le repertoire de cour avec l'extension md.

Sur markdown on code à peu près nos notes.

``#`` permet de créer le titre de niveau 1 <br>
``##``permet de créer le titre de niveau 2 <br>
``###`` permet de créer le titre de niveau 3 etc.

Pour que ces titres marchent il faut mettre un espace entre le symbole et le titre.
Sur markdown on :
<ul>
<li>Altgr + 7 pour mettre une ligne de code</li>
<li>Pour ecrir un mot en gras on fait deux étoiles le mot ensuite deux autres étoiles pour femer</li>
</ul>
<li>Pour faire des citations on met le symbole '>' devant le mot</li>
<li>Pour faire des liens on fait: [nom du lien](l'url).</li>