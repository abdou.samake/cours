# Git travail à plusieurs

On est parfois amener à travailler avec plusieurs personnes sur un projet, de ce fait on doit s'organiser à travailler en équipe.
Chaque développeur doit faire un merge requeste.<br>
Faire un merge requeste c'est faire une demande d'autorisation pour que son travail soit mis dans le master.
<ul>
<li>D'abord on crée une branche sur le prjet avant de commencer</li>
<li>On nomme la branche par une fonctionnalité non pas par un nom de personne</li>
<li>On push son travail</li>
<li>On va sur son git sur le projet , on fait create merge requeste, on selectionne la branche, on choisit le nom du chef de projet puis on create le merge requeste. </li>
</ul>
Le chef de projet reçoit le merge requeste, il aura la tache de verifier le travail, si c'est bon il accepte le merge, s'il ya des conflits sur un même fichier entre développeur il peut le régler ou bien laisser les développeurs les réglés entre eux.<br>
Du codé du développeur si le merge est accepté par le chef de projet, ce dernier peut faire un update pour mettre à jour le nouveau master.
