#  gitignore

Pour ne pas prendre en compte certains dossiers créés sur git on crée un fichier dont le nom commence par un point on peut le nommer " .gitignore ", puis on l'ajoute à git.<br>
Sur le fichier on fait (ctrl + espace) pour avoir la suggestion des dossiers sur le repertoire et on peut choisir ceux à faire ignorer par git.
Ainsi on peut choisir ces dossiers à ignorer:
<ul>
<li> .idea</li>
<li> .node_modules</li>
<li> .firebaserc</li>
<li> .mesdatasecretes.js</li>
</ul>
Et en suite on push.<br>
 l'instruction  (rm -rf nom du dossier) permet d'enlever le dossier sur le push.
