# Flexbox
Sur Bootstrap on a des row et des col pour créer des contenues en ligne et en colonne.<br>
On utilise des Flexbox pour créer ces contenues quand on ne peut utiliser Bootastrap pour des raisons divers et variées.<br>
On a cet exemple de code qui permet de faire des flexbox.
````html
<div class="container">
    <div class="box"></div>
    <div class="box"></div>
    <div class="box"></div>
    <div class="box"></div>
</div>
````
Sur CSS on applique
````css
.box{
    height:150px;
    width:150px;
    border:black thin solid;
    background-color: red;
}
.container{
    display: flex;
    justify-content: center;/*pour centrer*/
    justify-content: space-between;/*pour mettre de l'espace entre les box*/
    flex-direction: row;/* pour disposé en ligne*/
    flex-direction: column;/* pour disposé en colonne*/
}
````
On obtient ainsi des box répartis en ligne ou en colonne et espacés