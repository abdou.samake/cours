#  Importer un Repo

Pour importer le repo du projet créé par le chef de projet on effectue ces étapes:
<ol>
<li>on va sur gitlab, on copie l'URL du repo</li>
<li>ensuite on retourne sur webstorm, on va sur le menu on clique sur VCS</li>
<li>Ensuite on selection  Get from version control</li>
<li>Et on clone pour finir</li>
</ol>
On a le projet ouvert avec tout les dossiers.
Un fichier modifier passe en bleu, on fait commit and push pour l'envoyer vers le projet.
 