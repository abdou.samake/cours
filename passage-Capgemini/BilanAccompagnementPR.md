## AngularJs

Pour un projet AngulaJs on repartit le code sur 2 fichiers principaux à savoir la **Factorie** et le **Controleur**.
Dans la **Factorie** on on effectue l'appel de la methode qu'on a definit dans le service, on definit ici aussi les variables globales qui qui vont parager dans tout le projet.<br>

Un exemple de de fichier factorie, **bianAccompagnementGlobalFcty.js**

````angularjs

angular.module('modaliteSuiviAccompagnement').factory('bilanAccompagnementGlobal.factories.bilanAccompagnementGlobalFcty', ['facade.services.restSvc', 'facade.services.restSvcEnreg', '$rootScope', 'facade.constants.serviceRestCst',
    'modaliteSuiviAccompagnement.factories.modaliteSuiviAccompagnementFcty',
    function(restSvc, restSvcEnreg, $rootScope, serviceRestCst, modaliteSuiviAccompagnementFcty) {

        var enregistrementBilanAccompagnementGloSuccess = false;
        var erreurAppelDa030 = false;
        var messageCourrier = false;
        var idNational = null;
        var nsmod = null;
        var idAccompagnementHistorique = " ";
        var dateDuBilan = null;
        var nomPrenomConseiller = " ";


        var statutMsaGloDEM = function(estMSAHistorisee) {
            if (estMSAHistorisee ) {
                return "TER";
            } else
                return (modaliteSuiviAccompagnementFcty.infoModaliteEnCours && modaliteSuiviAccompagnementFcty.infoModaliteEnCours.statutGlobal &&
                        modaliteSuiviAccompagnementFcty.infoModaliteEnCours.statutGlobal.code)
        }

        var getIdAccompagnementHistorique = function() {
            return idAccompagnementHistorique;
        }

        var setIdAccompagnementHistorique = function(idAccHistorique) {
            idAccompagnementHistorique = idAccHistorique;
        }


        var getBilanModalite = function(idAccompagnement , estMSAHistorisee) {
            return restSvc.appelServiceLireBilanModalite(
                $rootScope.contexte.idrci,
                idAccompagnement,
                $rootScope.contexte.rsin,
                $rootScope.contexte.codeterritoire,
                statutMsaGloDEM(estMSAHistorisee)
            ).then(function(data) {
                if (data.messages && data.messages.length !== 0 && !isMessageSet) {
                    for (var i = 0; i < data.data.messages.length; ++i) {
                        var message = { message: data.messages[i].libelle };
                        alertAlimenteurFcty.ajouterAlertPage("avertissement", message, "blocCodesOp");
                        isMessageSet = true;
                    }
                    isMessageSet = true;
                }
                return data;
            }).catch(function(error) {
                console.log("catch erreur data service lire bilan");
            });
        };

        var enregistrerDonneesBilanEnregister = function(saveData) {
            return restSvcEnreg.appelServiceResEnregistrerBilanModalite(serviceRestCst.dispositifAccompagnementbilanModalite, saveData)
                .then(function(response) {
                    this.idNational = response.idNational;
                    this.nsmod = response.NSMOD;
                    return response;
                })
                .catch(function(error) {
                    if (error.status === 400) {
                        console.log("Erreur 400 : la requête est mal formée")
                    } else {
                        console.log("Erreur inattendue : ", error.statusText);
                    }
                })
        }
        var updateDureeAccompagnementDonneesSortie = function(bilanFSE, idAccompagnement) {
            return restSvcEnreg.appelServiceResEnregistrerBilanAccompagnement(bilanFSE, idAccompagnement, serviceRestCst.modaliteSuiviAccompagnement);
        }
        return {
            getBilanModalite: getBilanModalite,
            enregistrerDonneesBilanEnregister: enregistrerDonneesBilanEnregister,
            updateDureeAccompagnementDonneesSortie: updateDureeAccompagnementDonneesSortie,
            getIdAccompagnementHistorique: getIdAccompagnementHistorique,
            setIdAccompagnementHistorique: setIdAccompagnementHistorique,
        };
    }
]);
````

Le Fichier **Controleur** on definit les variables et les methodes qu'on a besoin pour l'afficher dans la vue. <br>

Exemple de fichier Controleur, **BilanAccompagnementPECtrl.js**

````angularjs

angular.module('modaliteSuiviAccompagnement').controller('bilanAccompagnementGlobal.controller.bilanAccompagnementGlobalCtrl', [
    'donneesSpecifique.factories.donneesSpecifiquesEnregFcty',
    'modaliteSuiviAccompagnement.factories.modaliteSuiviAccompagnementFcty',
    'bilanAccompagnementGlobal.factories.bilanAccompagnementGlobalFcty',
    'modaliteEnCours.factories.droitsConseillerFcty',
    '$scope',
    '$rootScope',

    function(donneesSpecifiquesEnregFcty, modaliteSuiviAccompagnementFcty, bilanAccompagnementGlobalFcty, droitsConseillerFcty, $scope, $rootScope) {
        var _this = this;
        _this.listSituationSortie = modaliteSuiviAccompagnementFcty.listSituationSortie;
        _this.listNatureContrat = modaliteSuiviAccompagnementFcty.listNatureContrat;
        _this.listIntensiteContrat = modaliteSuiviAccompagnementFcty.listIntensiteContrat;
        var displayedPresentationDonneesSpecifique = true;
        _this.nombreAteliers = null;
        _this.isDisabled = false;
        _this.nombreEvaluations = null;
        _this.nombreImmersionsPro = null;
        _this.nombreMisesEnContact = null;
        _this.nombreJoursTravailles = null;
        _this.nombreJoursFormations = null;
        _this.nombreActionsRealises = null;
        _this.nombreEntretiens = null;
        _this.nombreJoursIndisponibilites = null;
        _this.actionsDurantAccompagnement = null;
        _this.nombreJoursIndisponibilites = null;
        _this.entretiensDurantAccompagnement = null;
        _this.dateFinTheorique = null;
        _this.dateFinEffective = null;
        _this.accompagnementProlonge = false;
        _this.presenceBilanModaliteP300b = false;
        _this.dateDuBilan = null;
        _this.messageError = ""
        _this.dernierEntretien = "";
        _this.dernierEntretienInit = "";
        _this.idGlobal = null;
        _this.idConseillerReferent = "";
        _this.nomPrenomConseiller = "";
        _this.idAccompagnementHistorique = null;
        _this.dateDebut = null;
        _this.isAcquisitionChecked = null;
        _this.donneesSpecifiquesInit = null;
        _this.nbCaracteresParLigne = 79;
        _this.limiteLignesAtteinte = false;
        _this.isFormatageDernierEntretienValide = false;

        modaliteSuiviAccompagnementFcty.historiqueModalites.forEach(function(historiqueModalite) {
            if (historiqueModalite.lienDonneesSpecifique) {
                _this.idAccompagnementHistorique = historiqueModalite.idSRE + historiqueModalite.idModalite
            }
        })
        $scope.$on('variableUpdated', function(event, newVariable) {
            _this.changeValueIdAccompagnement2 = donneesSpecifiquesEnregFcty.getChangeValueIdAccompagnement();
            if (_this.changeValueIdAccompagnement2) {
                // Appel lireBilanModalite avec un statut "TER", ( historisé ) 
                _this.lireBilanModalite(bilanAccompagnementGlobalFcty.getIdAccompagnementHistorique(),true);
            } else {
                _this.lireBilanModalite(_this.idAccompagnement,false);
            }
        })

        _this.idAccompagnement = modaliteSuiviAccompagnementFcty.infoModaliteEnCours.identifiantSRE + modaliteSuiviAccompagnementFcty.infoModaliteEnCours.identifiantModalite;
        _this.nsmod = null;

        _this.statutMsaGloDEM = function() {
            return (modaliteSuiviAccompagnementFcty.infoModaliteEnCours && modaliteSuiviAccompagnementFcty.infoModaliteEnCours.statutGlobal && modaliteSuiviAccompagnementFcty.infoModaliteEnCours.statutGlobal.code)
        }
        _this.dateBilan = function() {
            if (modaliteSuiviAccompagnementFcty.isEmptyBilanAccompagnement) {
                return new Date()
            }
            return bilanAccompagnementGlobalFcty.dateBilan
        }
        _this.$onInit = function() {
            if (_this.infoModaliteEnCours() && _this.infoModaliteEnCours().typeModalite === "GLO" &&  ( _this.infoModaliteEnCours().statutGlobal.code === "DEM" || _this.infoModaliteEnCours().statutGlobal.code === "TER")){
                _this.lireBilanModalite(_this.idAccompagnement,false);
            }  
            // On assigne les données spécifiques initiales avant modification de saisie avec un nouveau pointeur
            _this.donneesSpecifiquesInit = angular.copy(_this.donneesSpecifique());

            if (_this.donneesSpecifiquesInit &&
                _this.donneesSpecifiquesInit.donneesSortie &&
                angular.isDefined(_this.donneesSpecifiquesInit.donneesSortie.acquisitionQualification)) {
                _this.isAcquisitionChecked = _this.donneesSpecifiquesInit.donneesSortie.acquisitionQualification;
            }
        };

        _this.lireBilanModalite = function(identifiantAccomp, estMSAHistorisee) { 
            bilanAccompagnementGlobalFcty.getBilanModalite(identifiantAccomp,estMSAHistorisee).then(function(data) {
                if (data) {
                    _this.nombreAteliers = (data.nombreAteliers) ? data.nombreAteliers : " ";
                    _this.nombreEvaluations = (data.nombreEvaluations) ? data.nombreEvaluations : " ";
                    _this.nombreImmersionsPro = (data.nombreImmersionsPro) ? data.nombreImmersionsPro : " ";
                    _this.nombreMisesEnContact = (data.nombreMisesEnContact) ? data.nombreMisesEnContact : " ";
                    _this.nombreJoursTravailles = (data.nombreJoursTravailles) ? data.nombreJoursTravailles : " ";
                    _this.nombreEntretiens = (data.nombreEntretiens) ? data.nombreEntretiens : " ";
                    _this.nombreJoursFormations = (data.nombreJoursFormations) ? data.nombreJoursFormations : " ";
                    //CG-COR0027557 correction alimentation
                    _this.nombreActionsRealises = (data.nombreActionsRealises) ? data.nombreActionsRealises : " ";
                    _this.nombreJoursIndisponibilites = (data.nombreJoursIndisponibilites) ? data.nombreJoursIndisponibilites : " ";
                    //CG-COR0027557 correction alimentation
                    _this.actionsDurantAccompagnement = _this.nombreActionsRealises;
                    _this.misesContactAccompagnement = (data.nombreMisesEnContact) ? data.nombreMisesEnContact : " ";
                    _this.entretiensDurantAccompagnement = (data.nombreEntretiens) ? data.nombreEntretiens : " ";
                    _this.dernierEntretien = (data.dernierEntretien) ? data.dernierEntretien : " ";
                    _this.dernierEntretienInit = (data.dernierEntretien) ? data.dernierEntretien : " ";
                    _this.dateFinTheorique = (data.dateFinTheorique) ? data.dateFinTheorique : " ";
                    _this.accompagnementProlonge = (data.accompagnementProlonge) ? data.accompagnementProlonge : false;
                    _this.idConseillerReferent = (data.idConseillerReferent) ? data.idConseillerReferent : " ";
                    _this.nomPrenomConseiller = (data.nomPrenomConseiller) ? data.nomPrenomConseiller : " ";
                    _this.presenceBilanModaliteP300b = (data.presenceBilanModaliteP300B) ? data.presenceBilanModaliteP300B : false;
                    _this.dateDuBilan = (data.tsp) ? data.tsp : new Date;
                    _this.dateFinEffective = (data.dateFinEffective) ? data.dateFinEffective : " "
                    _this.dateDebut = (data.dateDebut) ? data.dateDebut : "";
                    // On reformate les données du dernier entretien
                    _this.formaterDernierEntretien();
                };
            });
        }

        _this.formaterDernierEntretien = function() {
            if (_this.dernierEntretien && _this.dernierEntretien.split('\n')) {
                if (_this.dernierEntretien.split('\n').length > 6) {
                    let lignes = _this.dernierEntretien.split('\n');
                    _this.dernierEntretien = lignes.slice(0, 6).join('\n');
                }
                _this.isFormatageDernierEntretienValide = true;
            }
        }

        _this.calculerNbCaracteres = function(zoneTexte) {
            _this.limiteLignesAtteinte = false;

            let lines = zoneTexte.target.value.split('\n');
            if (lines.length <= 6) {
                for (let i = 0; i < lines.length; i++) {
                    if (lines[i].length <= _this.nbCaracteresParLigne) continue;
                    if (i != 5 && zoneTexte.target.value < 474) continue;
                    let j = 0;
                    let space = _this.nbCaracteresParLigne;
                    while (j++ <= _this.nbCaracteresParLigne) {
                        if (lines[i].charAt(j) == ' ') {
                            space = j;
                        }
                    }
                    lines[i + 1] = lines[i].substring(space + 1) + (lines[i + 1] || "");
                    lines[i] = lines[i].substring(0, space);
                }
            } else {
                _this.limiteLignesAtteinte = true;
            }

            zoneTexte.target.value = lines.slice(0, 6).join('\n');
        }

        _this.calculerNbCaracteresCopierColler = function(zoneTexte) {
            document.querySelector("#textareaEntretien").focus();
            _this.limiteLignesAtteinte = false;
            var chaineCopierColler = null;
            let lines;
            if (typeof zoneTexte.clipboardData !== 'undefined') {
                // On recupere la chaine dans le presse-papier en fonction de la compatibilité du navigateur utilisé
                chaineCopierColler = (window.clipboardData || zoneTexte.clipboardData).getData('text');
            }
            if (chaineCopierColler) {
                lines = chaineCopierColler.split('\n');
            }

            if (lines.length <= 6) {
                for (let i = 0; i < lines.length; i++) {
                    if (lines[i].length <= _this.nbCaracteresParLigne) continue;
                    if (i != 5 && chaineCopierColler < 474) continue;
                    let j = 0;
                    let space = _this.nbCaracteresParLigne;
                    while (j++ <= _this.nbCaracteresParLigne) {
                        if (lines[i].charAt(j) == ' ') {
                            space = j;
                        }
                    }
                    lines[i + 1] = lines[i].substring(space + 1) + (lines[i + 1] || "");
                    lines[i] = lines[i].substring(0, space);
                }
            } else {
                _this.limiteLignesAtteinte = true;
            }

            if (lines.length - 6 > 0) {
                do {
                    lines.pop();
                } while (lines.length - 6 > 0);
            }
            zoneTexte.target.value = lines.slice(0, 6).join('\n');
        }

        _this.isAccompagnementProlonge = function() {
            if (angular.isDefined(_this.donneesSpecifique())) {
                if (_this.donneesSpecifique().dureeAccompagnement.accompagnementProlonge) {
                    return "Oui";
                } else {
                    return "Non";
                }
            }

            return "";
        };
        _this.listStatutMDFSE = modaliteSuiviAccompagnementFcty.listStatutMDFSE;

        _this.listSituationSortie = modaliteSuiviAccompagnementFcty.listSituationSortie;
       _this.generationJustificatifFSEPossible = true;

        _this.motifDeFinModeVisu = function() {
            return donneesSpecifiquesEnregFcty.MotifFinVisu;
        };

        _this.infoModaliteEnCours = function() {
            return modaliteSuiviAccompagnementFcty.infoModaliteEnCours;
        };

        _this.afficherMessageSuccessEnregistrement = function() {
            return bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess;
        };

        _this.afficherMessageErreurDuDA030 = function() {
            return bilanAccompagnementGlobalFcty.erreurAppelDa030;
        }

        _this.reinitialiserAffichageErreurDuDA030 = function() {
            _this.messageError = "";
            bilanAccompagnementGlobalFcty.erreurAppelDa030 = false;
        }

        _this.affichageDuMessageCourrier = function() {
            return bilanAccompagnementGlobalFcty.messageCourrier;
        }

        _this.isAcquisitionQualification = function() {
            return donneesSpecifiquesEnregFcty.acquisitionQualification;
        }

        _this.affichageBilanAccompagnementGlobalVisuMsa = function() {
            return donneesSpecifiquesEnregFcty.afficherBilanAccompagnementGlobalVisu;
        };

        _this.enregistrementBilanAccompagnementGlo = function() {
            var donneesBilanAEnregistrer = {
                dateFinTheorique: _this.dateFinTheorique,
                idAccompagnement: _this.idAccompagnement,
                nombreAteliers: _this.nombreAteliers,
                nombreEvaluations: _this.nombreEvaluations,
                nombreImmersionsPro: _this.nombreImmersionsPro,
                nombreMisesEnContact: _this.nombreMisesEnContact,
                nombreJoursTravailles: _this.nombreJoursTravailles,
                nombreEntretiens: _this.nombreEntretiens,
                nombreJoursFormations: _this.nombreJoursFormations,
                nombreActionsRealises: _this.nombreActionsRealises,
                nombreJoursIndisponibilites: _this.nombreJoursIndisponibilites,
                actionsDurantAccompagnement: _this.actionsDurantAccompagnement,
                misesContactAccompagnement: _this.misesContactAccompagnement,
                entretiensDurantAccompagnement: _this.entretiensDurantAccompagnement,
                statutDE: _this.statutMsaGloDEM(),
                dernierEntretien: _this.dernierEntretien,
                idConseillerReferent: _this.idConseillerReferent
            }

            return bilanAccompagnementGlobalFcty.enregistrerDonneesBilanEnregister(donneesBilanAEnregistrer).then(function(data) {
                if (data) {
                    bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess = true;
                    _this.nsmod = data.NSMOD;
                    if (null != data && null != data.NSMOD && data.NSMOD === "ACAT") {
                        bilanAccompagnementGlobalFcty.messageCourrier = true;
                    }
                    bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess = true;

                    _this.isDisabled = true;
                    _this.presenceBilanModaliteP300B = true;
                    bilanAccompagnementGlobalFcty.erreurAppelDa030 = false;
                    bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess = true;
                    _this.dateDuBilan = new Date();
                    $rootScope.$broadcast('majDateDuBilan', _this.dateDuBilan);
                } else {
                    bilanAccompagnementGlobalFcty.erreurAppelDa030 = true;
                    bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess = false;
                    bilanAccompagnementGlobalFcty.messageCourrier = false;
                    _this.messageError = "Impossible de modifier le bilan.";
                }
            })
        }

        _this.motifDeFin = function() {
            if (_this.donneesSpecifique().donneesSortie.situationSortie) {
                return _this.donneesSpecifique().donneesSortie.situationSortie.libelle;
            }
        }

        _this.enregistreDernienBilanEtDonneesAlaSortie = function(donneesSpecifique) {
            var bilanFSE = {
                accompagnementProlonge: _this.isAccompagnementProlonge(),
                acquisitionQualification: _this.isAcquisitionChecked,
                bilanAccompagnement: _this.dernierEntretien,
                dateDebutAccompagnement: donneesSpecifique.dureeAccompagnement.dateDebut,
                dateFinEffective: donneesSpecifique.dureeAccompagnement.dateFinEffective,
                dateFinTheorique: donneesSpecifique.dureeAccompagnement.dateFinTheorique,
                intensiteContrat: null,
                natureContrat: null,
                propositionFerme: null,
                typeAppelant: "GLO",
            };
            if (angular.isDefined(donneesSpecifique.referent) &&
                null != donneesSpecifique.referent) {
                bilanFSE.identifiantConseiller = donneesSpecifique.referent.identifiant;
                bilanFSE.nomPrenomConseiller = donneesSpecifique.referent.prenomNom;
            }
            if (donneesSpecifique.dureeAccompagnement.dateFinTheorique != null) {
                if (donneesSpecifique.dureeAccompagnement.dateDebut >= donneesSpecifique.dureeAccompagnement.dateFinTheorique) {
                    modaliteSuiviAccompagnementFcty.setMessageError(errorsFacade.erreursFacade.E_ERR_PN072_N_DATE_DEBUT_DATE_FIN_THEORIQUE.log);
                    return;
                }
            }
            if (angular.isDefined(donneesSpecifique.donneesSortie)) {
                if (null != donneesSpecifique.donneesSortie.situationSortie) {

                    bilanFSE.situationSortie = donneesSpecifique.donneesSortie.situationSortie.code;
                }
                if (null != donneesSpecifique.donneesSortie.intensiteContrat) {
                    bilanFSE.intensiteContrat = donneesSpecifique.donneesSortie.intensiteContrat.code;
                }
                if (null != donneesSpecifique.donneesSortie.natureContrat) {
                    bilanFSE.natureContrat = donneesSpecifique.donneesSortie.natureContrat.code;
                }
            }

            let nbLignesEntretien = _this.dernierEntretien.split('\n');
            if (nbLignesEntretien.length > 6) {
                _this.limiteLignesAtteinte = true;
                return;
            }
            try {
                bilanAccompagnementGlobalFcty.updateDureeAccompagnementDonneesSortie(bilanFSE, _this.idAccompagnement).then(
                    function(data) {
                        if (null != data && ("OK" === data.message || data.message == null)) {
                            // affichage du mode visu
                            displayedPresentationDonneesSpecifique = true;
                            modaliteSuiviAccompagnementFcty.isMiseAJourPossible = true;
                            modaliteSuiviAccompagnementFcty.initError(null);
                            modaliteSuiviAccompagnementFcty.getLireModalite();
                            _this.enregistrementBilanAccompagnementGlo();
                        } else {
                            if (null != data) {
                                bilanAccompagnementGlobalFcty.erreurAppelDa030 = true;
                                bilanAccompagnementGlobalFcty.enregistrementBilanAccompagnementGloSuccess = false;
                                bilanAccompagnementGlobalFcty.messageCourrier = false;
                                _this.messageError = "La saisie du bilan et des données de sortie est obligatoire.";
                            }
                        }
                    },
                    function() {
                        modaliteSuiviAccompagnementFcty.setMessageError(errorsFacade.erreursDA030.E_ERR_EX042_DA030_APPEL_ECHEC.log);
                    }
                );
            } catch (error) {
                modaliteSuiviAccompagnementFcty.setMessageError(errorsFacade.erreursDA030.E_ERR_EX042_DA030_APPEL_ECHEC.log);
            }
        };

        _this.isAccompagnementProlonge = function() {
            if (angular.isDefined(_this.donneesSpecifique())) {
                if (_this.donneesSpecifique().dureeAccompagnement.accompagnementProlonge) {
                    return "Oui";
                } else {
                    return "Non";
                }
            }
            return "";
        };

        _this.displayBilanDateOrDateDay = function() {
            if (modaliteSuiviAccompagnementFcty.isEmptyBilanAccompagnement) {
                return new Date();
            } else if (bilanAccompagnementGlobalFcty.dateBilan) {
                return bilanAccompagnementGlobalFcty.dateBilan;
            }
        }

        _this.listOperationCofinancee = function() {
            if (typeof modaliteSuiviAccompagnementFcty.infoDonneesSpecifique != 'undefined' &&
                null != modaliteSuiviAccompagnementFcty.infoDonneesSpecifique) {
                return modaliteSuiviAccompagnementFcty.infoDonneesSpecifique.listOperationCofinancee;
            }
            return null;
        };

        _this.donneesSpecifique = function() {
            if (_this.changeValueIdAccompagnement2) {
                return modaliteSuiviAccompagnementFcty.infoHistoDonneesSpecifique;
            } else {
                return modaliteSuiviAccompagnementFcty.infoDonneesSpecifique;
            }
        }

        _this.displayBilanDateOrDateDay = function() {
            if (modaliteSuiviAccompagnementFcty.isEmptyBilanAccompagnement) {
                return new Date();
            } else if (modaliteSuiviAccompagnementFcty.dateBilan !== null) {
                return modaliteSuiviAccompagnementFcty.dateBilan;
            }
        }


        _this.restitutionPrecision = function() {
            if (!angular.isDefined(_this.donneesSpecifique()) ||
                null === _this.donneesSpecifique() ||
                !angular.isDefined(_this.donneesSpecifique().donneesSortie) ||
                null === _this.donneesSpecifique().donneesSortie ||
                !angular.isDefined(_this.donneesSpecifique().donneesSortie.situationSortie) ||
                null === _this.donneesSpecifique().donneesSortie.situationSortie) {
                return false;
            } else {

                if (!_this.donneesSpecifique().donneesSortie.situationSortie.precision) {
                    _this.donneesSpecifique().donneesSortie.natureContrat = null;
                    _this.donneesSpecifique().donneesSortie.intensiteContrat = null;
                }
                return _this.donneesSpecifique().donneesSortie.situationSortie.precision;
            }

        };

        _this.reinitialiserBilanAccompagnementGlo = function() {
            if (_this.donneesSpecifique() && !_this.donneesSpecifique().bilanAccompagnement &&
                _this.donneesSpecifique().donneesSortie &&
                !_this.donneesSpecifique().donneesSortie.situationSortie &&
                !_this.donneesSpecifique().donneesSortie.acquisitionQualification) {
                _this.donneesSpecifique().bilanAccompagnement = "";
                if (_this.dernierEntretienInit && _this.dernierEntretienInit !== "") {
                    _this.dernierEntretien = _this.dernierEntretienInit;
                } else {
                    _this.dernierEntretien = "";
                }

                _this.donneesSpecifique().donneesSortie.situationSortie = "";
                _this.donneesSpecifique().donneesSortie.acquisitionQualification = null;
            } else {
                _this.donneesSpecifique().bilanAccompagnement = angular.copy(_this.donneesSpecifiquesInit.bilanAccompagnement);
                _this.donneesSpecifique().donneesSortie = angular.copy(_this.donneesSpecifiquesInit.donneesSortie);
                _this.isAcquisitionChecked = _this.donneesSpecifiquesInit.donneesSortie.acquisitionQualification;
                _this.dernierEntretien = _this.dernierEntretienInit;
            }
        }

        _this.isProfilDSI = function() {
            return droitsConseillerFcty.getProfilAgent() === "DSI";
        };

        _this.isActifGenererJustificatif = function() {
            return _this.isProfilDSI() && !droitsConseillerFcty.getProfilSocle();
        };

        _this.genererJustificatif = function(idAdhesion) {
            //rendre le bouton inaccessible
            _this.generationJustificatifFSEPossible = false;
            modaliteSuiviAccompagnementFcty.initError(null);
    
            var idAccompagnement = bilanAccompagnementGlobalFcty.getIdAccompagnementHistorique();

            //appel méthode modaliteSuiviAccompagmenetFcty.genererJustificatifFSE
            try {
                donneesSpecifiquesEnregFcty.genererJustificatifFSE(idAdhesion, idAccompagnement).then(
                    function(data) {
                        if (null != data && 'info' == data.type) {
                            modaliteSuiviAccompagnementFcty.setMessageInfo(data.libelle);
                        } else if (null != data && 'error' == data.type) {
                            modaliteSuiviAccompagnementFcty.setMessageError(data.libelle);
                        }
                        _this.generationJustificatifFSEPossible = true;
                    },
                    function(dataKO) {
                        modaliteSuiviAccompagnementFcty.setMessageError("Erreur lors de l appel a la facade EX042");
                        _this.generationJustificatifFSEPossible = true;
                    }
                );
            } catch (error) {
                modaliteSuiviAccompagnementFcty.setMessageError("Erreur lors de l appel a la facade EX042");
                _this.generationJustificatifFSEPossible = true;
            }
        };
    }
]);

````
