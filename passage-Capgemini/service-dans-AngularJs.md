## Exemple de service dans AngularJs

````angularjs

angular.module('facade').provider('facade.services.restSvcEnreg', function() {
/**
* retourne un singleton du service
*/
this.$get = ['pn072.constants.appCst', 'logger.factory.loggerFcty', 'pn072.constants.appEnvCst', '$http', 'facade.factories.echecAppelRestFcty', 'pn072.constants.errorsFacade', '$rootScope',
    function(appCst, loggerFcty, appEnvCst, $http, echecAppelRestFcty, errorCst, $rootScope) {
        return new restSvcEnreg(appCst, loggerFcty, appEnvCst, $http, echecAppelRestFcty, errorCst, $rootScope);
    }
];

function restSvcEnreg(appCst, loggerFcty, appEnvCst, $http, echecAppelRestFcty, errorCst, $rootScope) {
    this.appelServiceRest = function(saveAxes, service) {
    loggerFcty.init("restSvcEnreg");

    var serviceCall = service;
    if (this.VerifierParam(saveAxes)) {
    var datajson = {
        idRci: saveAxes.idRci,
        idDiag: saveAxes.idDiag,
        idAgent: saveAxes.idAgent,
        codeTerritoire: saveAxes.codeTerritoire,
        idAxeTravailPrincipal: saveAxes.idAxeTravailPrincipal,
        idAxeTravailSecondaire: saveAxes.idAxeTravailSecondaire,
        dateSaisieAxeTravail: saveAxes.dateSaisieAxeTravail,
        nomAgentSaisieAxe: saveAxes.nomAgentSaisieAxe,
        trajectoire: saveAxes.trajectoire
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }));
} else {
    var error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_IDRCI_LIRE_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
};

this.appelServiceRestFragiliteProfessionnelle = function(saveFragilite, service) {
    var datajson = {
        idRci: saveFragilite.idRci,
        codePoleEmploi: saveFragilite.codeTerritoire,
        idSigma: saveFragilite.idSigma,
        idAgent: saveFragilite.idAgent,
        profilAgent: saveFragilite.profilAgent,
        suiviEtAcceptation: saveFragilite.suiviEtAcceptation,
        mode: saveFragilite.mode
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.appelServiceRestModalite = function(saveModalite, service) {
    var datajson = {
        idRci: saveModalite.idRci,
        codePoleEmploi: saveModalite.codeTerritoire,
        idSigma: saveModalite.idSigma,
        idAgent: saveModalite.idAgent,
        profilAgent: saveModalite.profilAgent,
        identifiantModalite: saveModalite.identifiantModalite,
        identifiantSRE: saveModalite.identifiantSRE,
        typeModalite: saveModalite.typeModalite,
        statutDemat: saveModalite.statutDemat,
        statutGlobal: saveModalite.statutGlobal,
        statutExh: saveModalite.statutExh,
        verificationQuestionnaire: saveModalite.verificationQuestionnaire,
        verificationStatutTermine: saveModalite.verificationStatutTermine,
        mode: saveModalite.mode,
        codeCommune: saveModalite.codeCommune,
        dateFinTheoriqueInitiale: saveModalite.dateFinTheoriqueInitiale
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.appelServiceResDonneesSpecifiques = function(saveDonneesSpecifique, service) {
    var datajson = {
        idRci: saveDonneesSpecifique.idRci,
        codePoleEmploi: saveDonneesSpecifique.codeTerritoire,
        idSigma: saveDonneesSpecifique.idSigma
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.appelServiceResMettreAJourStatutMDFSE = function(statutMdfse, idAccompagnement, service) {
    var triplette = $rootScope.contexte.idrci + "&" + $rootScope.contexte.rsin + "&" + $rootScope.contexte.codeterritoire;
    var path = service + triplette + "/" + idAccompagnement + "/statut_mdfse";
    var datajson = {
        codeStatutMDFSE: statutMdfse,
        libelleStatutMDFSE: ""
    };
    return this.gererErreurAppelRest($http.put(path, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.appelServiceResEnregistrerOperationsCofinancees = function(listOperationCofinancee, service) {
    var datajson = {
        listOperationCofinancee: listOperationCofinancee
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.appelServiceResEnregistrerDonneesSortieHistorique = function(saveDonneesSortieHistorique, service) {
    var datajson = {
        idRci: saveDonneesSortieHistorique.idRci,
        idRegional: saveDonneesSortieHistorique.idRegional,
        codeTerritoire: saveDonneesSortieHistorique.codeTerritoire,
        idAccompagnement: saveDonneesSortieHistorique.idAccompagnement,
        typeAppelant: saveDonneesSortieHistorique.typeAppelant,
        bilanFSE: saveDonneesSortieHistorique.bilanFSE,
        listOperationCofinancee: saveDonneesSortieHistorique.listOperationCofinancee,
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};


this.appelServiceResEnregistrerBilanAccompagnement = function(bilanFSE, idAccompagnement, service) {
    var triplette = $rootScope.contexte.idrci + $rootScope.contexte.rsin + $rootScope.contexte.codeterritoire;
    var path = service + triplette + "/" + idAccompagnement + "/bilan" + "/" + bilanFSE.typeAppelant;
    var datajson = {
        accompagnementProlonge: bilanFSE.accompagnementProlonge,
        acquisitionQualification: bilanFSE.acquisitionQualification,
        bilanGLODonneesSortieSaisissable: bilanFSE.bilanGLODonneesSortieSaisissable,
        bilanGLODureeAccompagnementSaisissable: bilanFSE.bilanGLODureeAccompagnementSaisissable,
        bilanSDSaisissable: bilanFSE.bilanSDSaisissable,
        dateDebutAccompagnement: bilanFSE.dateDebutAccompagnement,
        dateFinEffective: bilanFSE.dateFinEffective,
        dateFinTheorique: bilanFSE.dateFinTheorique,
        identifiantConseiller: bilanFSE.identifiantConseiller,
        intensiteContrat: bilanFSE.intensiteContrat,
        natureContrat: bilanFSE.natureContrat,
        nomPrenomConseiller: bilanFSE.nomPrenomConseiller,
        propositionFerme: bilanFSE.propositionFerme,
        situationSortie: bilanFSE.situationSortie,
        bilanAccompagnement: bilanFSE.bilanAccompagnement
    };
    return this.gererErreurAppelRest($http.post(path, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

/*Implementation Appel Service Enregistrer Bilan Modalite*/

this.appelServiceResEnregistrerBilanModalite = function(serviceRestCst, bilanSave) {
    var serviceName = serviceRestCst;
    var datajson = {
        "idNational" : $rootScope.contexte.idrci,
        "idAccompagnement" : bilanSave.idAccompagnement,
        "dateFinTheorique" : bilanSave.dateFinTheorique,
        "nombreAteliers" : bilanSave.nombreAteliers,
        "nombreEvaluations" : bilanSave.nombreEvaluations,
        "nombreImmersionsPro" : bilanSave.nombreImmersionsPro,
        "nombreMisesEnContact" : bilanSave.nombreMisesEnContact,
        "nombreEntretiens" : bilanSave.nombreEntretiens,
        "nombreJoursTravailles" : bilanSave.nombreJoursTravailles,
        "nombreJoursFormations" : bilanSave.nombreJoursFormations,
        "nombreActionsRealises" : bilanSave.nombreActionsRealises,
        "nombreJoursIndisponibilites" : bilanSave.nombreJoursIndisponibilites,
        "statutDE": bilanSave.statutDE,
        "dernierEntretien": bilanSave.dernierEntretien,
        "idConseillerReferent": bilanSave.idConseillerReferent,
        "codeTerritoire": $rootScope.contexte.codeterritoire,
        "idRegional": $rootScope.contexte.rsin
    };
    return this.gererErreurAppelRest($http.post(serviceName, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), serviceName);
};


this.appelServiceGenerationJustificatifFSE = function(idAdhesion, idAccompagnement, service) {
    var path = service +
        "?idAdhesion=" + idAdhesion +
        "&idNational=" + $rootScope.contexte.idrci +
        "&idRegional=" + $rootScope.contexte.rsin +
        "&codeTerritoire=" + $rootScope.contexte.codeterritoire +
        "&idAccompagnement=" + idAccompagnement;

    return this.gererErreurAppelRest($http.get(path, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), path);
};

/*Implementation Appel Service Enregistrer La date de fin de l'accompagnement 
sur la base de la copie de appelServiceResEnregistrerBilanAccompagnement*/

this.appelServiceMajFinTheoriqueMsaGlo = function(nouvelleDateFin, idAccompagnement, service) {
    var datajson = {
        "idAccompagnement" : idAccompagnement,
        "idNational" : $rootScope.contexte.idrci,
        "nouvelleDateFin" : nouvelleDateFin
    };
    return this.gererErreurAppelRest($http.post(service, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), service);
};

this.gererErreurAppelRest = function(promise, serviceCall) {
    return promise.then(function(successResponse) {
    if (successResponse) {
        return successResponse.data;
    } else {
        var error = errorCst.erreursFacade.E_ERR_PN072_ECHEC_REPONSE_SERVICE_REST;
        loggerFcty.log(appCst.LOG_LEVEL.ERROR, " restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
        throw new Error(error.codeAffiche);
}
},
function(errorResponse) {
    var error = echecAppelRestFcty.interpreterReponseKO(errorResponse);
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
});
};

this.VerifierParam = function(saveAxes) {
    var error;
    if (saveAxes.idRci == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_IDRCI_LIRE_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
if (saveAxes.idAgent == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_IDAGENT_ENREG_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
if (saveAxes.codeTerritoire == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_CODE_TERRI_ENREG_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
if (saveAxes.idAxeTravailPrincipal == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_ID_AXE_PRIN_ENREG_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
if (saveAxes.dateSaisieAxeTravail == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_DATE_SAISIE_AXE_ENREG_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
if (saveAxes.nomAgentSaisieAxe == null) {
    error = errorCst.erreursFacade.E_ERR_PN072_N_PARAM_NOM_SAISIE_AXE_ENREG_MSA;
    loggerFcty.log(appCst.LOG_LEVEL.ERROR, "restSvcEnreg : appel du service " + serviceCall + " - " + error.log);
    throw new Error(error.codeAffiche);
}
return true;
};

this.appelServiceEnregistrerCodesOP = function(idRci, idSigma, codeTerritoire, codeOP1, codeOP2, serviceRestCst) {

    var serviceName = serviceRestCst.individus + 'idNational/' + idRci + '/idRegional/' + idSigma + '/codeTerritoire/' + codeTerritoire + serviceRestCst.codesOP;
    var datajson = { "codeOP1": codeOP1, "codeOP2": codeOP2 };

    return this.gererErreurAppelRest($http.post(serviceName, datajson, {
        headers: {
            'pe-id-correlation': Math.floor(Math.random() * 900000) + 100000,
            'pe-nom-application': appEnvCst.NOM_APPLICATION,
            'pe-nom-domaine': 'EX042',
            'pe-id-utilisateur': $rootScope.contexte.userAgent
        }
    }), serviceName);
};
};
});
````
