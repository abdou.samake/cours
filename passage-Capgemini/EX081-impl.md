## dans le fichier Impl pour faire l'implémentation de l'interface **DonneesOFIIREST**

````javascript
package fr.pe.rind.donneessocialesindividu.services.rest;


import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.pe.rind.donneessocialesindividu.config.annotations.ContexteHabilitations;
import fr.pe.rind.donneessocialesindividu.exceptions.ErreurDonneesOFII;
import fr.pe.rind.donneessocialesindividu.services.DonneesOFIIService;
import fr.pe.rind.donneessocialesindividu.services.controleur.DonneesOFIIValidateur;
import fr.pe.rind.donneessocialesindividu.services.rest.api.DonneesOFIIREST;
import fr.pe.rind.donneessocialesindividu.services.rest.ressource.CriteresEnregistrerNumeroCIR;
import fr.pe.rind.donneessocialesindividu.services.rest.ressource.RetourEnregistrerNumeroCIR;
import fr.pe.rind.donneessocialesindividu.services.rest.ressource.RetourLireDonneesOFII;
import fr.pe.sldng.api.integration.rest.OperationRestSLD;
import fr.pe.sldng.api.securite.integration.PermissionsEntiteAppelante;
import fr.pe.sldng.api.securite.integration.ScopesSecurite;

public class DonneesOFIIRESTImpl implements DonneesOFIIREST {
	
	@Inject
	private DonneesOFIIService donneesOFIIService;
	
	@Inject
	private DonneesOFIIValidateur donneesOFIIValidateur;
	
	/**
     * Permet de récupérer les données ofii.
	 * @throws Exception 
     */
	@Override
    @OperationRestSLD
    @ContexteHabilitations
    @PermissionsEntiteAppelante({"MAP|LECT"})
    @TransactionAttribute(TransactionAttributeType.NEVER)
	public Response lireDonneeOFII(String idNational) throws Exception {
	   	
    	RetourLireDonneesOFII retour = new RetourLireDonneesOFII();
	    Response reponse = null;
	    
	    try {
	    	this.donneesOFIIValidateur.controlerIdNational(idNational);
	    	retour = donneesOFIIService.lireDonneesOFII(idNational);
	    	reponse = Response.status(Status.OK.getStatusCode()).entity(retour).build();
	        
	    } catch (ErreurDonneesOFII e) {
	    	return Response.status(Status.BAD_REQUEST).entity(e.getErreur().getLibelle()).build();
	    }
	    return reponse;
    }

	@Override
    @OperationRestSLD
    @ContexteHabilitations
    @ScopesSecurite("enregistrerNumeroCIR")
    @TransactionAttribute(TransactionAttributeType.NEVER)
	public Response enregistrerNumeroCIR(CriteresEnregistrerNumeroCIR criteresEnregistrerNumeroCIR) {
		
		RetourEnregistrerNumeroCIR retour = new RetourEnregistrerNumeroCIR();
		Response reponse = null;
		
		try {
			this.donneesOFIIValidateur.controlerCriteresEnregistrerNumeroCir(criteresEnregistrerNumeroCIR);
			retour = donneesOFIIService.enregistrerNumeroCIR(criteresEnregistrerNumeroCIR);
	    	reponse = Response.status(Status.OK.getStatusCode()).entity(retour).build();
	    	
		} catch (ErreurDonneesOFII e) {
	    	return Response.status(Status.BAD_REQUEST).entity(e.getErreur().getLibelle()).build();
		} catch (Exception e) {
	    	return Response.status(Status.BAD_REQUEST).entity(e.getCause().getCause().getMessage()).build();
	    }
		return reponse;
	}
	
}

````
