## Systeme de recupération de données par appel servicxe

Ici on récupére les données puis effectué une transformation selon l'existence de la données pour l'afficher. On a fait des ternaires à la place 
des if else.
````angularjs

recuperationDonneesPaveInfoComplementaire(data: DonneesOFII): void {
    if (data && data.infoComplementaireDTO) {
    this.permisConduire = (data.infoComplementaireDTO.permisConduire) ? data.infoComplementaireDTO.permisConduire :  '-';
    this.permisConduire = (this.permisConduire === '1') ? 'Oui' : (this.permisConduire === '0') ? 'Non' : '-';
    this.typePermis = (data.infoComplementaireDTO.typePermis) ? data.infoComplementaireDTO.typePermis : [];
    this.equipementInformatique = (data.infoComplementaireDTO.equipementInformatique) ?
        data.infoComplementaireDTO.equipementInformatique : '-';
    this.connexionInternet = (data.infoComplementaireDTO.connexionInternet) ? data.infoComplementaireDTO.connexionInternet : '-';
    this.connexionInternet = (this.connexionInternet === '1') ? 'Oui' : (this.connexionInternet === '0') ? 'Non' : '-';
    this.autonomieInformatique = (data.infoComplementaireDTO.autonomieInformatique) ?
        data.infoComplementaireDTO.autonomieInformatique : '-';
    this.autonomieInformatique = (this.autonomieInformatique === '1') ? 'Oui' : (this.autonomieInformatique === '0') ? 'Non' : '-';
    if ((this.permisConduire && this.permisConduire !== '-') ||
(this.equipementInformatique && this.equipementInformatique !== '-') ||
(this.connexionInternet && this.connexionInternet !== '-') ||
(this.autonomieInformatique && this.autonomieInformatique !== '-')) {
    this.isDonneesPaveInformationComplementaireExiste = true;
}
}
}


````
On définit une fonction pour appeler la fonction definie depuis le service faison l'appel rest du service

````angularjs
  obtenirDonneeOffi(): Observable<DonneesOFII> {
    return this.donneesOFIIService.getDataOfii();
}
````

On definit ensuite une fonction pour la souscription afin de récupérer les données

````angularjs
  lireDonneesPaveInfoComplementaire(): void {
    this.obtenirDonneeOffi().pipe(
        takeUntil(this.unsubscribe$)
    ).subscribe((data: DonneesOFII) => {
    this.recuperationDonneesPaveInfoComplementaire(data);
},

    (error) => {
    console.log(error);
}
);
}
````

## Appel de Service

On a fait un double appel de service dans cette methode.

-Le premier appel ,c'est pour récupérer une donnée qu'on a besoin de passer en paramétre dans l'appel du deuxiéme service. Ceci a été effectué grace à switchmap.

-Ensuite on a ajouté un catchError dans l'appel de ce service pour lever les errors envoyé par le back, puis on retourne un objet d'erreur constitué.


````angularjs

getDonneesOFII(): Subscription{
    return this.individusService.getIndividu().pipe(
        switchMap(individu => {
            const idRCI = individu.idRCI;
            return this.http.get<DonneesOFII>(lireDonneesOfii + `${idRCI}`).pipe(
    tap(
        (data) => {
        this.donneesOfiiSubject$.next(data);
    }
),
    catchError((error: any) => {
    if (error.status === 404) {
    const clientError = new Error('Not Found');
return throwError(clientError);
} else {
    const errorObj = {
        status: error?.status || 500,
        statusText: error?.statusText || 'Unknown Error',
    };
    return throwError(errorObj);
}
})
);
})
).subscribe();
}


````


## Methode de service pour envoyer des données

````angularjs

public postNumeroCIR(objetEnregistrement: ContratIntegrationRepublicaine ): Observable<ContratIntegrationRepublicaine>{
    return this.http.post<ContratIntegrationRepublicaine>(enregistrementCIR, objetEnregistrement)
        .pipe(
            catchError((error: any) => {
    if (error.status === 404) {
    const clientError = new Error('Les données OFII ne sont pas disponibles pour cet ID national.');
return throwError(clientError);
} else {
    const errorObj = {
        status: error?.status || 500,
        statusText: error?.statusText || 'Unknown Error',
        message: error?.message,
        messageerror: error?.error
    };
    return throwError(errorObj);
}
})
);
}
````

On a effectué l'appel de ce service d'enregistrement puis enregistré le formulaire.

````angularjs
enregistrerNumeroCIR(): void {
    this.tagDeClic();
    this.submittedForm = true;
    if (this.enregistrementForm.valid) {
    this.individusService.getIndividu().subscribe(
        individu => {
        this.identifiantNational = individu.idRCI;
        this.enregistrementCIR = {
            identifiantCIRIndividu: this.numeroCIR.value,
            idRci: individu.idRCI,
            codeTerritoire: individu.codeTerritoire
        };
        this.donneesOFIIService.postNumeroCIR(this.enregistrementCIR)
            .pipe(
                takeUntil(this.unsubscribe$)
            )
            .subscribe((reponse: any) => {
    if (reponse.messageSortie) {
    this.afficheMsgEnregistrementOK = true;
    this.afficheMsgCIRExisteDeja = false;
    this.messageOut = reponse.messageSortie;
} else if (this.enregistrementForm.get('numeroCIR')?.hasError('maxlength')) {
    this.afficheMsgEnregistrementOK = false;
}
},
(error) => {
    if (error.status === 400 && error.statusText === 'Bad Request') {
    this.afficheMsgCIRExisteDeja = true;
    this.afficheMsgEnregistrementOK = false;
    this.messageError = error.messageerror;
    this.messageErrorModifie = this.messageError.replace(/&apos;/g, `'`);
}
});
});
}
}
````

Construction du formulaire

````angularjs

ngOnInit(): void {
    this.lireDonneesOFII();
    this.enregistrementForm = this.formBuilder.group({
        numeroCIR: ['' , Validators.compose([Validators.required, Validators.maxLength(14)])]
    });


    this.getDataFromLocalStorage();
}

get numeroCIR(): FormControl {
    return this.enregistrementForm.get('numeroCIR') as FormControl;
}
````
