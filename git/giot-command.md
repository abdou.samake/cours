## Les commandes de Git

**git --version**: pour afficher la version
<br>
**git init**: pour installer le projet à git
<br>
**git config --global user.name "Bertrand"**: configure le projet au git de speudo Bertrand.
<br>
**git config --global user.email "toto@gmail.com"**: Et ces informations seront enregistrées au niveau du git lui même.
<br>
**git add nom du_fichier_ajouté**: permet d'ajouter le fichier à git.
<br>
**git status**: permet de voir les modifications qui seront en cours et qui seront sauvegardées.
<br>
**git commit -m "on met ici le message du commit"**: permet de faire un commit.
<br>
**git add**: permet d'ajouter en même temps tous les fichiers.
<br>
**git log**: afficher tous les commits qui ont été effectué sur le projet.
<br>
**git checkout nom_de_la_branche**: permet de basculer sur une branche.
<br>
**git checkout master -f**: permet de forcer à revenir sur le dernier commit qui a été effectué.
<br>
**git branch**: permet de voir toutes les branches du projet.
<br>
**git branch nom_de_la_branch**: permet de créer une branche.
<br>
**git checkout nom_de_la_branch**: permet de se positionner sur la branch.
<br>
**git merge nom_de_la_branch**: permet d'ajouter les modifications faites sur la nouvelle branche dans la branche principale master.
<br>
**git branch -d nom_de_la_branche**
<br>
**git branch -d nom_de_la_branch**: permet de supprimer la branch une fois qu'on a fini avec.
<br>
**git reset --hard HEAD**: permet de se retourner à l'etat où on était précedemment.
