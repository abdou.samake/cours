## Déploiement App mobile Android avec Capacitor

On peut utiliser Capacitor avec Ionic pour deployer notre application mobile sur Android.

Les etapes sont:

<ul>
<li>

``ionic integrations enable capacitor``

pour intégrer capacitor s'il n'existe pas dans le projet ionic

</li>

<li>

`ionic integrations disable capacitor`

pour l'enlever du projet

</li>

<li>

`ionic capacitor add android`

on ajoute le platform android
</li>

<li>

`ionic capacitor build android`

pour construire en android

</li>

<li>

``ionic capacitor open android``

on va ouvrir le projet dans android studio. Sur Android Studio on crée directement notre App Bundle release
(aab) et la signé avec une clé qu'on crée aussi sur android studio.
<br> Pour se faire on suit le chemin sur android studio

`` Build > Generate Signed Bundle/APK...``

On remplit toutes les informations

</li>


<li> Une fois le aab généré, on peut le uploader sur Google Play.
On a plus besoin d'entré des commandes pour générer notre clé de signature ou de signer notre aab. Grace a capacitor on peut le faire directement sur Android Studio.</li>

<li>Pour modifier le nom du package de notre application et/ou la version, on ouvre le fichier BuildConfig et on modifie

``app > java* > BuildConfig``

</li>

</ul>
