## Les Variables

Ils sont utilisé pour stocker des informations, c'est un segment de la mémoire où les données sont stockées.

Pour déclarer une variable il faut tapé:

``
typeDonnées nomVariable;
``
Exemple

``
int nombreDeFlowers;
``
Définir une variable:
 C'est le declaré et lui affecter une valeur en même temps

``
int nombreDeFlowers = 300;
``
On peut aussi declarer d'abord et ensuite lui affecter une valeur.

````
int nombreDeFlowers;

nombreDeFlowers = 300;
````
### Types de Dopnnées

Il ya deux groupes les **Primitif** et les **Références d'objets**

### Variables Primitifs

Pour les Nombres entiers on a **byte**, **short**, **int**, **long**

Pour les nombres à virgule Flottante on **float**, **double**

On a aussi les groupes de type **Boolean** qui peuvent avoir que deux états **VRAI** ou **FAUX**

On a le type **Caratères Uniques** appelé **char** qui est utlisé pour typer les variables à un seul caractére.

### Les Références d'objtes

Pour les nombres entiers on a **Byte**, **Short**, **Integer**, **Long**

Pour les nombres à virgule Flottante on **Float**, **Double**

On a aussi les **Boolean**

Pour les types à Caractéres Unique on a **Character**, **String**

Pour les Intégrés et Personnalités qui peuvent utiliser pour modifier un fichier ou un systéme on a  **FileWriter**, **BlueWhale**, **ATMMachine**

### Variables-Convention d'appellation

<ul>
<li>Sensible à la casse

``Int nombreDeFollowers != int NOMBREDeFollowers``

</li>
<li>Ne peut pas être déclaré deux fois avec le même nom</li>
<li>doit commencer par une lettre, ou $ ou _(non recommandé)</li>
<li>peut également contenir un chiffre</li>
<li>ne peut pas être égal aux mots clés réservés</li>
</ul>

### Conventions de Nommage des Entiers.
<ul>
<li>camelCase</li>
<li>décrire complétement ce que contient la variable</li>
<li>n'utilisez pas d'abréviations</li>
<li>ne pas réutiliser la variable</li>
<li>ne pas utiliser de $ ou de _ ou de chiffre</li>
</ul>
