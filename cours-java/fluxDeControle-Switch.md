## Flux de controle Switch

````angularjs

switch(valeur) {
    case valeur1:
        ...
        break;
    case valeur2:
        ...
        default:
        
        ...
}
````
Les valeurs valeur1, valeur2 sont comparés à valeur puis l'excécution est faite si chacun des cas est résalisés.

La partie **default** est excécuté lorsque aucune correspondance des cas n'est trouvé.

Exemple:

````angularjs

Switch(couleurVoiture) {
    
    case "bleu":
            System.out.println("cette voiture est bleu");
            break;
    case "jaune":
            System.out.println("cette voiture est jaune");
            break;
    default:
        System.out.println("Je n'ai aucune idée de la couleur de cette voiture");

}
````
