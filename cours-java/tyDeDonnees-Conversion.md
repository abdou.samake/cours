## Primitifs - Conversion

Nous avoins besoin de la conversion parceque parfois le valeur de la variable change.
Il ya deux types de conversions: **Elargissement** ou **rétrécissement**

### Conversdion explicite

Le syntax est le suivant:

```
typeDopnnéesSouhaité nomNouvelleVariable = (typeDonnéesSouhaité) valeur Ou autreNomVariable
```


````javascript
import java.sql.SQLOutput;

public class MaPremierApplication {

    public static void main(String[] args) {
        System.out.println("hello word !");

        int nombreDeFllowers = 300;
        float comsonnationCombinee = 15.5F;

        byte nombrePlaces = 5;
        int kilometrage = 22850;
        short puissance = 362;

        short nouveauNombrePlaces = nombrePlaces;
        System.out.println("Nouvelle valeur du nombre de places: " + nouveauNombrePlaces);

        int nouveauKilometrage = kilometrage;
        System.out.println("Nouvelle valeur du Kilométrage: " + nouveauKilometrage);

        // on a effectué ici une rétrissement et on a perdu des données en mémoire.
        byte nouvellePuissance = (byte) puissance;
        System.out.println("Nouvelle valeur du nombre de la puissance: " + nouvellePuissance); // resultat  Nouvelle valeur du nombre de la puissance 106 au lieu de 362.

    }
}
````
