## Références d'Objets

Pour créer un objet

```
NomClasse nomVariable = valeur OU new nomClasse ();
```
Parmis les références d'objets intégré en JAVA on a:

<ul>
<li>

**Byte**
</li>
<li>

**Short**

</li>
<li>

**Integer**

</li>
<li>

**Long**

</li>
<li>

**Boolean**

</li>
<li>

**Character**

</li>
<li>

**String**

</li>
<li>

**Float**

</li>
<li>

**Double**

</li>
</ul>
Les objets ont des fonctions qui peuvent leurs etre appliqué:

<ul>
<li>toUpperCase() pour de la miniscule à majuscule</li>
<li>toLowerCase() l'inverse de toUpperCase()</li>
<li>equals() pour comparer deux chaines de caratères</li>
<li>isEmpty() pour verifier si c'est vide</li>
<li>isBlank() permet de verifier si une chaine de caractère est vide ou ne contient que des espaces blancs</li>
</ul>


### Exemple de declaration des references d'objet

````javascript
public class ReferenecObjetsApp {

    public static void main(String[] args) {

        Byte nombrePlaces = 5;
        Short puissance = 362;
        Integer prix = 360000;
        Long immatriculation = 54698563214568978L;

        Float consommationCombinee = 15.5F;
        Double consommationMoyenne = 12.3654789652314325;

        Boolean estEndommagee = true;
        Character classeEnergetique = 'F';

        String typeVoiture = "Dodge challenger SRT";
        String typeVoitureAvecNew = new String ("Dodge challenger SRT");

        System.out.println("nombre de place: " + nombrePlaces);
        System.out.println("puissance: " + puissance);
        System.out.println("prix £ : " + prix.floatValue());
        System.out.println("immatriculation : " + immatriculation);
        System.out.println("consommation du carburant combinée : " + consommationCombinee + "l/100km");
        System.out.println("consommation du carburant Moyenne : " + consommationMoyenne.intValue() + "l/100km");
        System.out.println("La voiture est endommagée : " +  estEndommagee);
        System.out.println("La classe de l'efficacité énergitique : " + classeEnergetique);
        System.out.println("Le type de voiture  : " + typeVoiture);
        System.out.println("Le type de voiture tout en Majuscule: " + typeVoiture.toUpperCase());
        System.out.println("Le type de voiture tout en Miniscule: " + typeVoiture.toLowerCase());
        System.out.println("Le type de voiture avec le mot clé new : " + typeVoitureAvecNew);
        System.out.println("Le type de voiture est egale type de voiture avec le mot clé new : " + typeVoitureAvecNew.equals(typeVoiture));
    }
}
````
