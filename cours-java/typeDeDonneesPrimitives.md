## Primitives - Nombres Entiers

<ul>
<li>byte: stocke des valeurs allant de -128 jusqu'à 127</li>
<li>short: peut stocker des valeurs allant de -32765 jusqu'à 32767</li>
<li> int peut stocker de -2 147 483 648 jusqu'à -2 147 483 648</li>
<li>long peut socker un nombre vraiment enorme</li>
</ul>

Le plus courrament utilisé de ces types est **int**.
Pour stocker un grand nomvre on peut utiliser **long**

## Primitives - Nombre à virgule flottante
<ul>
<li>float stock un nombre de 6 à 7 chiffres décimaux</li>
<li>double: stock un nombre de 15 chiffres décimaux</li>
</ul>

Le **double** est le plus courramant utilisé.
 ## Primitifs - Boolean et Character

<ul>
<li>boolean: true ou false</li>
<li>char: caractère unique ou valeur ASCII</li>
</ul>
