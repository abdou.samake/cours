## Continue

Le mot clé **continue** permet de faire sauter les étapes dans les boucles

````angularjs

for(int i = 1; i <= 100; i++) {
    if(i == 10) {
        continue
}
System.out.println("Je ne parle pas français");
}
````

Dans cet exemple l'excution sautera quand i sera égale à 10 donc on aura 99 phrases imprimées aulieu de 100.

## Break

Ce mot clé sert à arréter la boucle

````angularjs

for(int i = 1; i <= 100; i++) {
    if(i == 10) {
        break
}
System.out.println("Je ne parle pas français");
}
````
Dans cet exemple l'excécution s'arréte quand i sera égale à 10. Donc on va imprimé 10 lignes.


*NB*: continue et break ne sont pas courramment utilisé mais il ya des cas où il est pratique de les utiliser.
