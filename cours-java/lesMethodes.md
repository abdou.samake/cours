## Comment créer une methode


````angularjs

typeDeRetour nomFonction() {
    
    ...
    return typeDrETOUR.
}
````

OU

````angularjs

modificateurAccès static typeDeRetour nomFonction(paramétres) {
    
    ...
}
````

Exemple:

````angularjs

public static void main(String[] args) {
    
    ...
}
````

**public** est le modificateur d'accés.

**static** montre que la methode appartient à la classe

**main** est le nom de la fonction .
