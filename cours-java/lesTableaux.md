## Comment créer un tableau


````angularjs

Int[] numerosLoterie = new int[5];
````

Oubien

````angularjs

Int[] numerosLoterie;
numerosLoterie = new int[5];
````

numerosLoterie est le nom du tableau.

Int est le type de données que contiennent le tableau.

Et ici 5 est la taille du tableau.

### Utilisation de ForEach pour parcourir un tableau

````angularjs

For(int numeroLoterie: numerosLoterie){
    ...
    System.out.println(numeroLoterie)
}
````

## Tableaux multidimensionnels

Création:

```int[][] numerosLoterie hebdomadaire = int[54][5];```
