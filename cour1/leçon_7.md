## Leçon n°7 - Les variables

Un programme informatique mémorise des données en utilisant des variables. Une variable est une zone de stockage d'information. On peut l'imaginer comme une boîte dans laquelle on range des choses.

var est une manière de définir une variable en Javascript comme le montre l'exemple suivant

```
var name=prompt('donne moi ton nom');
console.log(name)
````
 Ce code va demander à l'utilsateur de lui donner son nom, une fois l'utilisateur lui donne son, la machine le stocke dans la variable name puis l'affiche à l'aide de la commande console.log.
 