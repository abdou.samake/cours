## Leçon n°15 - La structure conditionnelle if else

L'instruction if exécute une instruction si une condition donnée est vraie ou équivalente à vrai. Si la condition n'est pas vérifiée, il est possible d'utiliser une autre instruction (else).
Par exemple

 ```` const me = {
       firstname:'Abdou',
       lastname:'Samake',
       gender: 'H'
       age:'25',
       adresse:'cite des fleurs'
}

if(me.gender==='H'){
    console.log('Abdou est un homme');
}
else{
console.log('Abdou n/'est pas un homme');
}


