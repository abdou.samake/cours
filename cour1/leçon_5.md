## Leçon n°5 - Intégrer du code Javascript à une page HTML

Pour commencer on crée un fichier javascript avec l'extension .js. Puis on fait le lien entre le fichier html et le le fichier javascript grace à cette balise
`<script src="index.js"></scrit>`qu'on met entre les balises `<header></headre>`. Cependant pour accéder à toute les balises du html on le met a la fin du balise body avant la balise fermante `</body>`.
<br>La console est un outil qui va nous permettre d'écrire des informations sur ce qui se passe dans votre application.

Pour accéder à la console de Javascript on fait clique droit sur la page HTML que nous affiche le navigateur, ensuite on clique sur inspecter puis console.
Ainsi pour affihicher quelque chose sur la console on exécute sur le fichier javascript le code suivant:

`console.log('texte')
Ensuite on fait ctrl + R sur la console pour voir le resultat.`
