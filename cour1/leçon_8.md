## Leçon n°8 - Les variables let et const
 
 Généralement la déclaration d'une variable en javascript par le mot clé var ne s'utilise plus. On utilise maintenent les mots clés _const_ ou _let_.
 Déclarer une variable par const c'est lui assigner une valeur et une seule fois qui ne va pas changer.
 
 ' const number1=42; '
 
 Déclarer une variable par let c'est lui assigner une valeur qu'on peut au fur du temps si on veut la changer.

 ````javascript
const number1=42;
 let numbe2=30;
 number2=20;
 console.log(number1 + number2); // affiche 62, on remarque on a changé la valeur de number2 de 30 à 20
  
 