## Leçon n°13 - La composition de prédicats

La composition des prédicats permet de vérifier si plusieurs attributs sont vrais ou faux.
<ul>
<li> Si le prédicat est défini par la composition de ou(||) entre deux ou plusieurs attributs, si l'un des attributs est vrais, ça renvoit la valeur vrais.</li>

 <li>Si le prédicat est defini par la composition de ET(&&) entre deux ou plusieurs attributs, le predicat est vrais si toutes les valeurs sont vrais, si l'un des valeurs est fausse alors le prédicat est faux.</li>

``````
const me = {
    firstname:'Abdou',
    lastname:'Samake',
    gender: 'H'
    age:'25',
    adresse:'cite des fleurs,
}  
``````
Pour verifier on fait:
``````
  const imajor = me.age===28;
const iname = me.firstname==='Abdou';
const result1 = imagor && iname;
const result2 = imagor || iname;

console.log(result1) // affiche false car imajor est faux, avec ET dés qu'un resultat est faux la composition sera fausse.
console.log(result2) // affiche true car iname est vrais,  avec ou dés qu'un resultat est vrais alors la composition est vrais

 ``````