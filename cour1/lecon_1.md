# Introduction à webstorn, HTML, CSS, JavaScript

## Leçon n°1 Mettre en place l’environnement de travail pour coder en Javascript

Un environnement de développement est Un outil qui permet aux développeurs de faire son travail c'est à dire codé. Ces logiciels on les appelle des IDE (integrated development environment).
Parmi ces IDE on a Webstorm, VSC, Atome, Sublime Texte etc.
On utilise wesbtorn parceque c'est un IDE pour débutant pas compliqué, parcontre il a un défaut c'est un IDE payant et il est un peu lent pour les machines lentes.
Un autre avantage de webstorm est que son instalation n'est pas compliqué, il est  facile à mettre en place.
Une fois webstorm installé sur le pc, on a quelques raccourcits clavier qui permet de gagner un temps considérable quand on code.
<ul>

<li>ctrl + c: pour copier</li>

<li>clt + v : pour coler</li>

<li>cltr + x: pour couper</li>

<li>ctlr + d: pour dupliquer une ligne</li>

<li>cltr + z: pour revenir en arriére et shift + cltr + z: pour annuler le retour en arrière</li>

<li>Alt + j: permet  de sélectionner tous les mêmes mots du texte</li>

<li>ctlr + y: permet de supprimer une ligne.</li>