 ## Leçon n°9 - Les fonctions
 
 Une définition de fonction (aussi appelée déclaration de fonction ou instruction de fonction) est construite avec le mot-clé **function**, suivi par :
 le nom de la fonction.
 <ul>

 <li>Une liste d'arguments à passer à la fonction, entre parenthèses et séparés par des virgules.</li>
 
 <li>Les instructions JavaScript définissant la fonction, entre accolades, { }.</li>
</ul>
On a l'exemple suivant:

  ````function addition(param1, param2){
    console.log(param1 + param2)
    }
    
addition(5, 6) // qui va appeler la fonction  