
## Leçon n°4 - Créer des classes CSS

Les sélecteurs de classe CSS permettent de cibler des éléments d'un document en fonction du contenu de l'attribut class de chaque élément.
En CSS on met un point (.) devant le nom de chaque classe.<br>


````
.nom de class{
        instruction
}
````