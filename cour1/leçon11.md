
## Leçon n°11 - Les objets

JavaScript est conçu autour d'un paradigme simple, basé sur les objets. Un objet est un ensemble de propriétés et une propriété est une association entre un nom et une valeur.

On déclare un objet par la façon suivante:

 ````const me = {
    firstname:'Abdou',
    lastname:'Samake',
    age:'25',
    adresse:'cite des fleurs'
}  
Pour afficher quelque propriété comme firstname on fait:

  console.log(me.firstname); // qui va afficher Abdou 