 ## Leçon n°6 - Les valeurs primitives présentation

Un langage de programmation quant il traite de l'information il a besoin de savoir de quels types d'informations il traite. Il ne traite pas de la même facon les nombres, les textes et d'autres valeurs qu'on appelle les valeurs primitives.
Ainsi on a les valeurs primitives en Javascript:
<ul>
<li>le type nombre c'est number</li>

<li>le type texte c'est string</li>

<li>le type vrais ou faux c'est booleans</li>

<li>La valeur primitive Undefined est une valeur qui n'existe pas de type undefined</li>

<li>la valeur null c'est nul </li>

<li>la valeur primitive NAN qui signifie c'est pas un nombre lorsqu'on fait une opération mathématique.</li>

