# Deploiement Projet Angular avec Firebase

Pour effectuer le déploiement de son projet angular sur Firebase, on procéde comme suit:

<ul>
<li>On lance l'application

``ng serve``

</li>
<li>On va lancer la production

``ng build --prod --aot``
qui va créer un dossier **dist**

</li>
<li>

``firebase init``
</li>
<li>On choisit Hosting</li>
<li>On choisit un projet existant</li>
<li>Que souhaiter vous utilisez comme repertoire public ?

``On tape dist``
le dossier qui a été déja créé par la prod
</li>

<li>Configurez en tant qu'application d'une seule page ?

``Non``
Car notre application ne constitue pas une seule page, si c'était le cas on peut répondre par Oui

</li>

<li>Voulez vous ecraser le fichier index.html qui existe déja 

``Non``
</li>

<li>Et enfin

``firebase deploy``

</li>

On obtient l'url de notre site
</ul>

## Deploiement à nouveau

Pour déployer à nouveau l'application on exécute juste la commande build:


``ng build --prod --aot``

Ensuitre on déploie sur firebase


``firebase deploy``
