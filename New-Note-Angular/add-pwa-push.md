## Ajouter PWA sur son projet Angular pour lesNotifications

Il faut exécuter la commande suivante:

````ng add @angular/pwa --project angular project name````

## Pour générer les clés publiques et privé de son application 


````npm install web-push -g````

Ensuite

````web-push generate-vapid-keys --json````
