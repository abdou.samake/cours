## Mise en ligne App Store

**Pré-requis** :

<ul>
<li>MacOS</li>
<li>Apple Developper Program</li>
<li>Xcode</li>
</ul>

### Taches à effectuer

<li>Ouvrir le projet ionic avec Xcode

<ul>
<li>Dans Xcode aller à Xcode => Preferences => account => on clique sur + 
=> selectionner Apple ID => on renseigne l'identifiant de Apple Id et le mot de passe.

</li></ul>
Apres ça on a bien lié notre identifiant Apple ID avec le projet XCOde

</li>

### Enrégistrement au prés d'Apple de notre Identifiant d'application

<ul>
<li>Renseigner un Bundle Identifier unique dans Xcode => AppToDeploy
</li>

<li>On se rend sur le site  <a href="developper.apple.com">developper.apple.com</a>

</li>
<li> 

Clique en haut à droite  **Account**  </li>
<li> 

On va sur **Certificates, IDs and Profiles**  

<ul>
<li>Identifier


<ul><li>On selectionne App IDs => on clique sur le + en haut à droite</li>
<li>On renseigne un nom de l'application</li>
<li>On selection Explicit App ID on renseigne l'identifiant unique de l'application</li>
<li>On selectionne les services qu'on souhaite intégrer dans l'applications</li>
<li>On enrégistre tout après verification</li>
<li>Done</li>
</ul>

</li>
</ul>

</li>

<li> 

On crée les **Provisioning** Profile sur Xcode

<ul>
<li>On retour dans Xcode => Préferences => on selectionne notre
Apple ID ensuite l'équipe avec laquelle veut générer nos cerficats => on clique sur 

**Manage certificats**
</li>
<li>On crée deux certificats Developpement et distribution
=> on Clique + => IOS Development et IOS Distribution

Sur le site Portail Developer dans l'onglet All on doit voir nos deux certificats
</li>

</ul>
</li>
</ul>

### Creations des Provisioning Profiles

<ul>
<li>Provisioning Profiles

<ul>
<li>All
<ul>
<li>On clique sur + => Dans  

**Distribution** on selectionne **AppStore** </li>
<li>On selecionne le certificat ensuite on clique 

**continue**</li>
<li>On renseigne le nom du Profil on met le nom de l'application suivi der AppStore => On finit par Done!</li>
</ul>

</li>
</ul>

</li>
<li>On retourne dans Xcode => Preference => AppleID => faire 

**Download Manual Profiles**
</li>
<li>Dans les propriétes du projet  => Signing => Team => on selectionne notre equipe créée</li>

<li>Retour apple.developer.com => 

**Account** (haut à droite) => **Overview** => **iTunes coonnect**

=> Go to iTunes connect => Mes Apps => + => Nouvelle App

=> On coche **ios** et on remplit les champs dans UGS on remplit un identifiant unique crée exemple com.dimitridessus.appStore.appdeploy
=> ensuite on clique sur **creer**
<ul>
<li>Information de l'app</li>
<li>Tarifs et disponibilité</li>
<li>App ios</li>
</ul>

Finaliser avant soumission ( on renseigne les descriptions, les mots clès, les captures d'écran, renseigne l'icone de taille 1024x1024)


</li>
<li>On côche validation automatique de la version</li>
</ul>


### Transférer l'application sur Itunes Connect

Dans Xcode => En haut à gauche on clique sur **AppToDeploy** => Generic iOS Device => Product => Archive (l'apllication va se compiler) => on selectionne sur l'archive qu'on vient de faire, puis appuyer

**Upload to App Store** => Next => on coche **Manually manage**
 => **signing** => Next => Default(iOS Distribution), choisir notre Profil (AppDeploy AppStore)
 => Next => Upload => Done!
 
 ### Retour Sur iTunes Connect
 
 => Build => Apparition + => **selectionner une build avant de soumettre votre app** => on selectionne
 la version envoyer depuis Xcode => Terminé => Enrégistré => Soumettre pour verification
 
 
 

