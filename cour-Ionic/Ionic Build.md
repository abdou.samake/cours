## Ionic Capacitor

## Ionic Build Android and Ionic Build iOS

### Build Android

<ul>
<li>

``ionic start``

</li>
<li>

``ionic build``

</li>
<li>

``npx cap add android``

</li>
<li>

``npx cap open android``

</li>
</ul>

### Build iOS
<ul>
<li>

``ionic start``

</li>
<li>

``open . `` (pour ouvrir le projet dans l'editeur)

</li>
<li>

``npm install @capacitor/cli @capacitor/core``

</li>
<li>

``npx cap init`` (si c'est un nouveau projet)

</li>

<li>

``npx cap add ios``

</li>
<li>

``npx cap open ios``

</li>

</ul>
