## Installation et Notes

Ionic a commencé avec AngularJs comme framework sous-jacent, mais maintenant il s'est etendu à Angular et même à React.
Ionic s'appuyant avec cordova permet de créer des applications hybrides (Android, ios, Windows phone)
<br></br>
En ce qui concerne l'environnement de construction/d'exécution, 
Ionic prend désormais en charge **Cordova** et **Capacitor**. 
Alors que Cordova est l'ainé avec une communauté plus grande 
et des constructions stables, Capacitor gravit rapidement 
les échelons et est également soutenu par une communauté en 
pleine croissance.

<ul>
<li>Pour installer ionic on exécute la commande suivante:

``npm install -g @ionic/cli``
</li>

<li>Pour commencer un nouveau projet ionic on exécute les commandes suivantes selon les options qu'on veut exécuter.

``ionic start`` // commande de démarrage simple.

``ionic start --list`` // liste les modèles de demarrage disponible.

``ionic start myApp`` // commande de demarrage avec le nom de l'application.

``ionic start myApp blank`` // commande de demarrage interactif avec le nom et type vierge.

``ionic start myApp tabs --cordova`` // commence par angular cordova

``ionic start myApp tabs --capacitor`` // commence par angular capacitor

``ionic start myApp super --type=ionic-angular`` // commance par angular.


</li>

<li>Pour verifier les informations de votre environnement

``ionic info``

</li>

</ul>

### Capacitor
C'est un environnement d'exécution d'application multiplateforme qui facilite 
la création d'application web qui qui s'éxécute de manière navive
sur ios et Android, Electron et le web.<br>
Capacitor essaie de surmonter certaines limitations de cordova avec un nouveau 
flux de travail App. Capacitor represente la prochaine évolution au dela des applications hybrides.

###Cordova(ou Apache Cordova):
Cordova permet aux programmeurs de logiciel de créer des applications
pour appareils mobiles de CSS3, HTML5 et JavaScript au lieu de s'appuyer sur des API
spécifiques à la plateforme comme Android, ios ou Windows phone.

###Différence entre Cordova et Capacitor:
**Capacitor** ne s'exécute pas sur l'appareil ou n'émule pas 
via la ligne de commande. Au de cela, ces operations se produisent
via l'IDE spécifique à la plateforme.<br>
On ne peut exécuter des applications de **Ionic Capacitpor** à l'aide d'une commande telle que 

``ionic run ios``

Avec **Capacitor** vous devez exécuter des applications ios à l'aide de **Xcode** et des applications Android à l'aide
d'**Android Studio**.<br>
An substance Capacitor est comme une nouvelle version plus flexible de cordova conçue spécialement pour le type de cadre ionic.


