## Migration de Ionic 5 à Ionic 6

<ul>
<li>
Ionic 6 prend en charge Angular 12+, mettez à jour vers la dernière version
d'Angular
</li>
<li>
Mise à jour vers la dernière version d'Ionic 6

``npm install @ionic/angular@6``

Si vous utilisez Ionic Angular Server assurez vous également de le mettre à jour.


``npm install @ionic/angular@6 @ionic/angular-server@6``

</li>

<li>Supprimez toute utilisation de   

**config.set()** 

Au lieu de cela, définissez votre configuration dans **IonicModule.forRoot()**.
Voir la documentation Angular config pour plus d'exemple.
</li>

<li>Supprimez toute utilisation de la setupConfig fonction précédement exportée de 

**@ionic/angular**. Définissez votre configuration à la **IonicModule.forRoot()** place.
</li>
</ul> 
