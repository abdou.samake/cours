## Différence entre JDK et JRE

### JDK: Java Developpement Kit (kit de developpement)

### JRE: Java Runtime Environment(environnement d'excécution Java)

Dans JDK on a le compilateur qui s'appelle **Javac**.

Dans JRE on a les outils pour excécuter les codes java.
Parmi ces outils on a la **JVM** Machine Virtuelle Java qui est l'élément logiciel qui nous permet d'excécuter du code Java.

Dans le **JDK** on a le **JRE**, donc le JRE est un sous ensemble de **JDK**.

## Difference entre JavaSE et JavaEE
(JavaEE est remplacé par Jakarta)

**JSE**: Standart Edition.<br>
**JEE**: Entreprise Edition. **JEE** s'intéresse à tous ce qui fait du web, de s'adresser à la base de donnée etc...<br>
**JavaEE** depend du **JavaSE**

### Note
Java 1.8 ou Java8 est la version 8 de Java où il ya les expressions Lambda.

### Compilation et excécution Java

On compile le code d'un fichier pour le traduire en langage Machine sous un autre fichier en class. Et c'est ce nouveau fichier qui sera excécuter par  **JVM** de Java et avoir le resultat en sortie.