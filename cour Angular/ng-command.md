## ng command

  add Adds support for an external library to your project.
  **analytics** Configures the gathering of Angular CLI usage metrics. See https://angular.io/cli/usage-analytics-gathering.
  <br>
  **build** (b) Compiles an Angular app into an output directory named dist/ at the given output path. Must be executed from within a workspace directory.
  <br>
  **deploy** Invokes the deploy builder for a specified project or for the default project in the workspace.
  <br>
  **config** Retrieves or sets Angular configuration values in the angular.json file for the workspace.
  <br>
  **doc** (d) Opens the official Angular documentation (angular.io) in a browser, and searches for a given keyword.
  <br>
  **e2e** (e) Builds and serves an Angular app, then runs end-to-end tests.
  <br>
  **extract-i18n (i18n-extract, xi18n)** Extracts i18n messages from source code.
  <br>
  **generate** (g) Generates and/or modifies files based on a schematic.
  <br>
  **help** Lists available commands and their short descriptions.
  <br>
  **lint** (l) Runs linting tools on Angular app code in a given project folder.
  <br>
  **new** (n) Creates a new workspace and an initial Angular application.
  <br>
  **run** Runs an Architect target with an optional custom builder configuration defined in your project.
  <br>
  **serve** (s) Builds and serves your app, rebuilding on file changes.
  <br>
  **test** (t) Runs unit tests in a project.
  <br>
  **update** Updates your application and its dependencies. See https://update.angular.io/
  <br>
  **version** (v) Outputs Angular CLI version.
