## Guide de mise à jour angular 11.0 vers 12.0 pour les applications de base.

Avant la mise à jour aucune modification n'est actuellement nécessaire avant de passer d'une version à l'autre.

Exécuter:

``npx @angular/cli@12 update @angular/core@12 @angular/cli@12``

qui devrait vous amener à la version 12 d'Angular.

<ul>
<li>Angular nécessite désormais TypeScript 4.2, pour mettre à jour automatiquement
exécutez

``ng update``

</li>
<li>Vous ne pouvez plus utiliser Angular avec Nodejs version 10 ou anterieure</li>
</ul>
