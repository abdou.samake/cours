## SetTimeOut

Pour afficher des données au bout d'un temps (24heur par exemple)


````
volume: number;
sum: number;

setTimeout(
() => {
this.volume = this.sum;
},
86400;

)
````

Cette fonction calcule la somme des volumes et affiche le resultat au bout de 24 heure.
